import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getApiDataAxios } from '../../helpers';
import { Link } from 'react-router-dom';

function Activate(user, setUser, setAlert) {
  const [usuarioCreado, setUsuarioCreado] = useState(false);

  const { activationCode } = useParams();

  async function userValidate(event) {
    const url = `${global.config.backend}/user/validate/${activationCode}`;
    const metodo = 'get';
    const datos = {};

    const data = await getApiDataAxios(url, metodo, datos);
    if (data.status === 200) {
      setUsuarioCreado(false);
    } else {
      setUsuarioCreado(data.data.message);
    }
  }
  useEffect(() => {
    userValidate();
  });

  return (
    <div>
      <br></br>
      <br></br>
      <br></br>

      {usuarioCreado ? (
        <>
          <p className="mensajeError">{usuarioCreado}</p>
          <Link to="/login">Prueba a hacer Login</Link>
        </>
      ) : (
        <>
          <p className="mensajeError">
            Usuario Validado, ya puedes iniciar sesión.
          </p>
          <Link to="/login">Login</Link>
        </>
      )}
    </div>
  );
}

export default Activate;
