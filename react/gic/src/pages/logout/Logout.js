import { useHistory } from 'react-router';
import { useEffect } from 'react';

function Logout({ setUser }) {
  let history = useHistory();

  useEffect(() => {
    setUser({
      idUser: 0,
      emailUser: null,
      isUserLogged: false,
    });
    window.localStorage.removeItem('accesstoken');
    window.localStorage.removeItem('iduser');
    window.localStorage.removeItem('emailUser');
    window.localStorage.removeItem('vista');
    history.push('/login');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setUser]);

  return (
    <div>
      <br></br>
      <br></br>
      <br></br>
      <h1>Route para Logout</h1>
      <br></br>

      <div className="login-container">
        <h1>Hasta la proxima....</h1>
      </div>
    </div>
  );
}

export default Logout;
