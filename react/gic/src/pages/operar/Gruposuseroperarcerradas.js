import { useState, useEffect } from 'react';
import { getApiDataAxios, parseSybaseDate, classVar } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';
import InversionOperacionescerradas from './InversionOperacionescerradas';
let arrayGroups = [];

function Gruposuseroperarcerradas({ user, setAlert, padre }) {
  const [groups, setGroups] = useState([]);
  const [tableState, setTableState] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [selectGroup, setSelectGroup] = useState([]);

  // Obtenemos los datos de grupos del usuario del Back
  async function getGroupsUser() {
    const url = `${global.config.backend}/inversion/groups/closed/${user.idUser}`;

    const metodo = 'get';
    const datos = { userId: user.idUser };
    const cabeceras = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setGroups(data.data.data);
      data.data.data.length === 0 ? setTableState(0) : setTableState(-1);
    } else {
      setTableState(data.statusText);
    }
  }

  // Expansion de grupos
  function verDetallesGrupo(event) {
    let index = arrayGroups.indexOf(event.target.dataset.grupo * 1);
    if (index > -1) {
      arrayGroups.splice(index, 1);
    } else {
      arrayGroups.push(event.target.dataset.grupo * 1);
    }
    setSelectGroup([...arrayGroups]);
  }

  // Carga inicial y temporizacion 1 minuto
  useEffect(() => {
    getGroupsUser();
    const intervalID = setInterval(() => {
      getGroupsUser();
    }, 60000);
    return () => {
      clearInterval(intervalID);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return user.isUserLogged && tableState === -1 ? (
    <div className="tablasOperarCerradas">
      <p className="tituloTabla">
        Grupos donde participas con Operaciones Cerradas
      </p>

      {groups.map((group) => (
        <div key={group.id} className="contenedortablasGrupos">
          <div className="tablasGrupos">
            <table className="tablaGrupos">
              <thead></thead>
              <tbody>
                <tr className="tablaHeader">
                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    Nombre
                  </td>

                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    Fecha Creacion
                  </td>
                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    Role
                  </td>
                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    Tipo
                  </td>
                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    Rend.
                  </td>
                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    %Rend.
                  </td>
                </tr>
                <tr>
                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    {group.name}
                  </td>
                  <td
                    align="center"
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                  >
                    {parseSybaseDate(group.createdAt)}
                  </td>

                  {group.role === 'admin' ? (
                    <td
                      align="center"
                      onClick={verDetallesGrupo}
                      data-grupo={group.id}
                    >
                      Admin
                    </td>
                  ) : (
                    <td
                      align="center"
                      onClick={verDetallesGrupo}
                      data-grupo={group.id}
                    >
                      Usuario
                    </td>
                  )}

                  {group.personal ? (
                    <td
                      align="center"
                      onClick={verDetallesGrupo}
                      data-grupo={group.id}
                    >
                      Personal
                    </td>
                  ) : (
                    <td
                      align="center"
                      onClick={verDetallesGrupo}
                      data-grupo={group.id}
                    >
                      Compart.
                    </td>
                  )}

                  <td
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                    className={classVar(0.001, group.var_rendimiento)}
                    align="center"
                  >
                    {group.rendimiento.toFixed(1) * 1}
                  </td>
                  <td
                    onClick={verDetallesGrupo}
                    data-grupo={group.id}
                    className={classVar(0.001, group.var_rendimiento)}
                    align="center"
                  >
                    {group.var_rendimiento.toFixed(2) * 1}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          {arrayGroups.indexOf(group.id * 1) !== -1 && (
            <InversionOperacionescerradas
              user={user}
              idGroup={group.id}
              roleUser={group.role}
              alert={alert}
              setAlert={setAlert}
            />
          )}
        </div>
      ))}
    </div>
  ) : (
    <>
      <br></br>
      {tableState === 0 ? (
        <Cargandodatos texto="1-No tienes grupos con operaciones cerradas" />
      ) : (
        <Cargandodatos texto="Recuperando Datos de Grupos.." />
      )}
    </>
  );
}

export default Gruposuseroperarcerradas;
