import { useState, useEffect } from 'react';
import { getApiDataAxios, parseDate, formatDate } from '../../helpers';
import { useHistory } from 'react-router';

function Operacioncerrada({
  user,
  idgrupo,
  setAlert,
  operationId,
  numero_compra,
  precio_compra,
  fecha_compra,
  comision_compra,
  numero_venta,
  precio_venta,
  fecha_venta,
  comision_venta,
  moneda,
  seteditarOperacion,
}) {
  const [selMoneda, setselMoneda] = useState('');
  const rutaImagenes = `${global.config.backend}/static/images/`;

  const [editNumeroVenta, seteditNumeroVenta] = useState('');
  const [editPrecioVenta, seteditPrecioVenta] = useState('');
  const [editFechaVenta, seteditFechaVenta] = useState(parseDate());
  const [editComisionVenta, seteditComisionVenta] = useState('');

  let history = useHistory();

  // Funcion Llamada a Backend editar operacion
  async function editOperacion(event) {
    const url = `${global.config.backend}/operations/editclose/` + operationId;
    const metodo = 'put';
    const datos = {
      price: editPrecioVenta,
      comision: editComisionVenta,
      quantity: editNumeroVenta,
      date: formatDate(editFechaVenta),
    };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Operacion "${editNumeroVenta} ${moneda}" modificada`);
      history.push('/');
      history.push('/operar');
    } else {
      setAlert('1-Error modificando la operacion, comprueba los valores.');
    }
  }

  useEffect(() => {
    seteditNumeroVenta(numero_venta + '');
    seteditPrecioVenta(precio_venta + '');
    seteditFechaVenta(fecha_venta);
    seteditComisionVenta(comision_venta + '');
    setselMoneda(moneda);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <h3>Modo Editar Operacion</h3>
      <div className="divOperacionEditClose">
        <h1 align="left">{selMoneda}</h1>
        <img
          src={
            rutaImagenes + selMoneda.slice(0, selMoneda.indexOf('-')) + '.png'
          }
          alt={selMoneda}
          width="20"
          height="20"
          className="noborder "
        />
        <img
          onClick={() => {
            seteditarOperacion(false);
          }}
          src={rutaImagenes + 'atras.png'}
          title="Volver Atras"
          alt="Atras"
          width="25"
          height="25"
          className="noborder operacionEditImg"
        />
        <p className="divOperacion-hidden">Moneda</p>

        <p> Fecha Compra</p>
        <label htmlFor={idgrupo}>
          <input
            disabled
            value={fecha_compra}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        <img
          onClick={editOperacion}
          src={rutaImagenes + 'aceptar.png'}
          title="Confirmar"
          alt="Confirmar"
          width="25"
          height="25"
          className="noborder operacionEditImg"
        />
        <p> Cantidad Compra</p>
        <label htmlFor={idgrupo}>
          <input
            disabled
            value={numero_compra}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <img
          onClick={() => {
            seteditarOperacion(false);
          }}
          src={rutaImagenes + 'cancelar.png'}
          title="Cancelar"
          alt="Cancelar"
          width="25"
          height="25"
          className="noborder operacionEditImg"
        />
        <p> Precio Compra</p>
        <label htmlFor={idgrupo}>
          <input
            disabled
            align="left"
            value={precio_compra}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <div></div>
        <p> Comision Compra</p>
        <label htmlFor={idgrupo}>
          <input
            disabled
            value={comision_compra}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <div></div>
        <p> Fecha Venta</p>
        <label htmlFor={idgrupo}>
          <input
            onChange={(event) => {
              event.preventDefault();
              seteditFechaVenta(event.target.value);
            }}
            value={editFechaVenta}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <div></div>
        <p> Cantidad Venta</p>
        <label htmlFor={idgrupo}>
          <input
            onChange={(event) => {
              event.preventDefault();
              seteditNumeroVenta(event.target.value);
            }}
            value={editNumeroVenta}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <div></div>
        <p> Precio Venta</p>
        <label htmlFor={idgrupo}>
          <input
            align="left"
            onChange={(event) => {
              event.preventDefault();
              seteditPrecioVenta(event.target.value);
            }}
            value={editPrecioVenta}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <div></div>
        <p> Comision Venta</p>
        <label htmlFor={idgrupo}>
          <input
            onChange={(event) => {
              event.preventDefault();
              seteditComisionVenta(event.target.value);
            }}
            value={editComisionVenta}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <div></div>
      </div>
      <br></br>
    </>
  );
}

export default Operacioncerrada;
