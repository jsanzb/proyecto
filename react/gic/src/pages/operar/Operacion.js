import { useState, useEffect } from 'react';
import { getApiDataAxios, parseDate, formatDate } from '../../helpers';
import { useHistory } from 'react-router';

function Operacion({
  user,
  idgrupo,
  setAlert,
  setOperations,
  operations,
  operationId,
  cantidad,
  precio_compra,
  fecha_compra,
  comision_compra,
  moneda,
  precio_actual,
  seteditarOperacion,
  roleUser,
  confirm,
  setConfirm,
}) {
  // const [monedas, setMonedas] = useState([]);
  const [selMoneda, setselMoneda] = useState(moneda);
  const rutaImagenes = `${global.config.backend}/static/images/`;

  //   const [editarOperacion, seteditarOperacion] = useState(false);
  const [modoEditar, setmodoEditar] = useState(false);
  const [modoCerrar, setmodoCerrar] = useState(false);
  const [modoAbrir, setmodoAbrir] = useState(false);
  const [editCantidad, seteditCantidad] = useState('');
  const [editPrecioCompra, seteditPrecioCompra] = useState('');
  const [editFecha, seteditFecha] = useState(parseDate());
  const [editComision, seteditComision] = useState('');
  const [disableInput, setdisableInput] = useState(true);
  const [classDiv, setclassDiv] = useState('divOperacion');
  const [classImg, setclassImg] = useState('noborder operacionEditImg');

  let history = useHistory();

  // Funcion Llamada a Backend editar operacion
  async function editOperacion(event) {
    const url = `${global.config.backend}/operations/edit/` + operationId;
    const metodo = 'put';
    const datos = {
      price: editPrecioCompra,
      comision: editComision,
      quantity: editCantidad,
      date: formatDate(editFecha),
    };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Operacion "${editCantidad} ${moneda}" modificada`);
      history.push('/');
      history.push('/operar');
    } else {
      setAlert('1-Error modificando la operacion, comprueba los valores.');
    }
    // }
  }

  // Funcion Llamada a Backend añadir operacion
  async function addOperacion(event) {
    const url = `${global.config.backend}/operations/open`;
    const metodo = 'post';
    const datos = {
      idUser: user.idUser,
      idGroup: idgrupo,
      coin: selMoneda,
      price: editPrecioCompra,
      comision: editComision,
      quantity: editCantidad,
      date: formatDate(editFecha),
    };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Operacion de ${editCantidad} ${selMoneda} Añadida`);
      history.push('/');
      history.push('/operar');
    } else {
      setAlert(
        '1-Error Añadiendo la operacion, comprueba los valores: ' +
          data.data.message
      );
    }
  }

  // Funcion Llamada a Backend cerrar operacion
  async function cierreOperacion(event) {
    const url = `${global.config.backend}/operations/close`;
    const metodo = 'post';
    const datos = {
      idOperacion: operationId,
      precio_venta: editPrecioCompra,
      comision_venta: editComision,
      numero_venta: editCantidad,
      fecha_venta: formatDate(editFecha),
    };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };

    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Operacion "${editCantidad} ${moneda}" Cerrada`);
      history.push('/');
      history.push('/operar');
    } else {
      setAlert('1-Error cerrando la operacion, comprueba los valores.');
    }
    // }
  }

  // Preborrado operacion
  function predeleteOperacion(event) {
    if (!confirm) {
      setConfirm({
        aviso: 1,
        mensaje: `Vas a borrar la operacion "${cantidad} ${moneda}". Esta accion es irreversible.
        Estas seguro?`,
        proceso: 'Operacion',
      });
    }
  }

  // Funcion Llamada a Backend borrar operacion
  async function deleteOperacion(event) {
    const url = `${global.config.backend}/operations/delete/` + operationId;
    const metodo = 'delete';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };

    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Operacion "${cantidad} ${moneda}" Borrada`);
      history.push('/');
      history.push('/operar');
    } else {
      setAlert('1-Error borrando la operacion: ' + data.data.message);
    }
    setConfirm(false);
  }

  function cancelarEdicion() {
    setmodoEditar(false);
    setmodoCerrar(false);
    setmodoAbrir(false);
    setdisableInput(true);
    seteditPrecioCompra(precio_compra + '');
    seteditFecha(fecha_compra);
    setclassDiv('divOperacion');
    setselMoneda(moneda);
    seteditCantidad(cantidad + '');
  }

  useEffect(() => {
    seteditCantidad(cantidad + '');
    seteditPrecioCompra(precio_compra + '');
    seteditFecha(fecha_compra);
    seteditComision(comision_compra + '');
    setselMoneda(moneda);
    roleUser !== 'admin' && setclassImg('noborder operacionEditImgHidden');

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    confirm === 'Operacion' && deleteOperacion();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [confirm]);

  return (
    <>
      {!modoEditar && !modoCerrar && !modoAbrir && (
        <h3>Detalle de la Operacion</h3>
      )}
      {modoEditar && <h3>Modo Editar Operacion</h3>}
      {modoCerrar && <h3>Modo Cerrar Operacion (Venta)</h3>}
      {modoAbrir && <h3>Modo Abrir Operacion (Compra)</h3>}
      <div className={classDiv}>
        <h1 align="left">{selMoneda}</h1>

        <img
          src={
            rutaImagenes + selMoneda.slice(0, selMoneda.indexOf('-')) + '.png'
          }
          alt={selMoneda}
          width="20"
          height="20"
          className="noborder "
        />
        {/* )} */}

        <img
          onClick={() => {
            seteditarOperacion(false);
          }}
          src={rutaImagenes + 'atras.png'}
          title="Volver Atras"
          alt="Atras"
          width="25"
          height="25"
          className="noborder operacionEditImg"
        />

        {modoCerrar ? <p> Fecha Venta</p> : <p> Fecha Compra</p>}
        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            onChange={(event) => {
              event.preventDefault();
              seteditFecha(event.target.value);
            }}
            value={editFecha}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        {modoCerrar || modoEditar || modoAbrir ? (
          <img
            onClick={
              modoEditar
                ? editOperacion
                : modoAbrir
                ? addOperacion
                : cierreOperacion
            }
            src={rutaImagenes + 'aceptar.png'}
            title="Confirmar"
            alt="Confirmar"
            width="25"
            height="25"
            className="noborder operacionEditImg"
          />
        ) : (
          <img
            onClick={() => {
              setmodoEditar(true);
              setmodoCerrar(false);
              setmodoAbrir(false);
              setdisableInput(false);
              setclassDiv('divOperacion divOperacionEdit');
            }}
            src={rutaImagenes + 'editar.png'}
            title="Editar Operacion"
            alt="Editar Operacion"
            width="25"
            height="25"
            className={classImg}
          />
        )}

        {modoCerrar ? <p> Cantidad Venta</p> : <p> Cantidad Compra</p>}
        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            onChange={(event) => {
              event.preventDefault();
              seteditCantidad(event.target.value);
            }}
            value={editCantidad}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        {modoCerrar || modoEditar || modoAbrir ? (
          <img
            onClick={cancelarEdicion}
            src={rutaImagenes + 'cancelar.png'}
            title="Cancelar"
            alt="Cancelar"
            width="25"
            height="25"
            className="noborder operacionEditImg"
          />
        ) : (
          <img
            onClick={() => {
              setmodoEditar(false);
              setmodoCerrar(true);
              setmodoAbrir(false);
              setdisableInput(false);
              seteditFecha(parseDate());
              seteditPrecioCompra(precio_actual + '');
              setclassDiv('divOperacion divOperacionEdit');
            }}
            src={rutaImagenes + 'vender.png'}
            title="Cerrar Operacion (Vender)"
            alt="Cerrar Operacion"
            width="25"
            height="25"
            className={classImg}
          />
        )}

        {modoCerrar ? <p> Precio Venta</p> : <p> Precio Compra</p>}

        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            align="left"
            onChange={(event) => {
              event.preventDefault();
              seteditPrecioCompra(event.target.value);
            }}
            value={editPrecioCompra}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        {!modoCerrar && !modoEditar && !modoAbrir ? (
          <img
            onClick={() => {
              setmodoEditar(false);
              setmodoCerrar(false);
              setmodoAbrir(true);
              seteditFecha(parseDate());
              seteditCantidad('0');
              seteditPrecioCompra(precio_actual + '');
              setdisableInput(false);
              setclassDiv('divOperacion divOperacionEdit');
            }}
            src={rutaImagenes + 'agregar.png'}
            title="Abrir Operacion (Comprar)"
            alt="Abrir Operacion"
            width="25"
            height="25"
            className={classImg}
          />
        ) : (
          <div></div>
        )}

        {modoCerrar ? <p> Comision Venta</p> : <p> Comision Compra</p>}
        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            onChange={(event) => {
              event.preventDefault();
              seteditComision(event.target.value);
            }}
            value={editComision}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        {!modoCerrar && !modoEditar && !modoAbrir ? (
          <img
            onClick={predeleteOperacion}
            src={rutaImagenes + 'borrar.png'}
            title="Borrar Operacion"
            alt="Borrar Operacion"
            width="25"
            height="25"
            className={classImg}
          />
        ) : (
          <div></div>
        )}
      </div>
      <br></br>
    </>
  );
}

export default Operacion;
