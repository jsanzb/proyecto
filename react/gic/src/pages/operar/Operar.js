import { useEffect } from 'react';
import { useHistory } from 'react-router';
import Gruposuseroperar from './Gruposuseroperar';
import Gruposuseroperarcerradas from './Gruposuseroperarcerradas';

import './operar.css';

function Operar({ user, alert, setAlert, setSelMenu, confirm, setConfirm }) {
  document.title = 'giC-Operar';
  let history = useHistory();

  useEffect(() => {
    setSelMenu('Operar');
    !user.isUserLogged &&
      setAlert(
        '1-No has iniciado Sesión en la plataforma. Solo podrás ver los datos de cotizaciones del Mercado. Inicia Sesión o créate una cuenta.'
      );
    !user.isUserLogged && history.push('/login');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="tablasOperacionesTodas">
      {user.isUserLogged && (
        <>
          <Gruposuseroperar
            user={user}
            setAlert={setAlert}
            confirm={confirm}
            setConfirm={setConfirm}
          />
          <br></br>
          <hr></hr>
          <br></br>
          <Gruposuseroperarcerradas user={user} setAlert={setAlert} />
        </>
      )}
    </div>
  );
}

export default Operar;
