import { useState, useEffect } from 'react';
import { getApiDataAxios, parseDate, formatDate } from '../../helpers';
import { useHistory } from 'react-router';

function Operacionadd({
  user,
  idgrupo,
  setAlert,
  seteditarOperacion,
  roleUser,
}) {
  const [monedas, setMonedas] = useState([]);
  const [selMoneda, setselMoneda] = useState('');
  const rutaImagenes = `${global.config.backend}/static/images/`;

  const [editCantidad, seteditCantidad] = useState('0');
  const [editPrecioCompra, seteditPrecioCompra] = useState('0');
  const [editFecha, seteditFecha] = useState(parseDate());
  const [editComision, seteditComision] = useState('0');

  let disableInput = false;
  let history = useHistory();

  // Llamada a Backend para Select de monedas
  async function getTablamonedas(url, metodo, datos) {
    const data = await getApiDataAxios(url, metodo, datos);
    setMonedas(data.data.data);
  }

  // Llamada a Backend para precio Moneda Seleccionada
  async function getMoneda(moneda) {
    const url = `${global.config.backend}/monedas/${moneda}`;
    const metodo = 'get';
    const data = await getApiDataAxios(url, metodo);
    seteditPrecioCompra(data.data.data[0].ultimo_precio);
  }

  // Funcion Llamada a Backend añadir operacion
  async function addOperacion(event) {
    if (!selMoneda) {
      setAlert('1-Tienes que elegir una Moneda');
    } else if (editCantidad === '0') {
      setAlert('1-La cantidad de monedas no pueden ser 0.');
    } else {
      const url = `${global.config.backend}/operations/open`;
      const metodo = 'post';
      const datos = {
        idUser: user.idUser,
        idGroup: idgrupo,
        coin: selMoneda,
        price: editPrecioCompra.toString(),
        comision: editComision,
        quantity: editCantidad,
        date: formatDate(editFecha),
      };

      const cabecera = {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      };
      const data = await getApiDataAxios(url, metodo, datos, cabecera);
      if (data.status === 200) {
        setAlert(`0-Operacion de ${editCantidad} ${selMoneda} Añadida`);
        history.push('/');
        history.push('/operar');
      } else {
        setAlert(
          '1-Error Añadiendo la operacion, comprueba los valores: ' +
            data.data.message
        );
      }
    }
  }

  useEffect(() => {
    getTablamonedas(`${global.config.backend}/monedas/all`, 'get');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return roleUser === 'admin' ? (
    <>
      <h3>Modo Abrir Operacion (Compra)</h3>
      <div className="divOperacionAdd">
        <p>Moneda</p>

        <select
          className="inputEditOperationSelect"
          onChange={(event) => {
            setselMoneda(event.target.value);
          }}
          onBlur={(event) => {
            getMoneda(event.target.value);
          }}
        >
          <option key="0" value={null}></option>
          {monedas.map((moneda) => (
            <option key={moneda.moneda} value={moneda.moneda}>
              {moneda.moneda}
            </option>
          ))}
        </select>

        <img
          onClick={() => {
            seteditarOperacion(false);
          }}
          src={rutaImagenes + 'atras.png'}
          title="Volver Atras"
          alt="Atras"
          width="25"
          height="25"
          className="noborder operacionEditImg"
        />

        <p> Fecha Compra</p>
        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            onChange={(event) => {
              event.preventDefault();
              seteditFecha(event.target.value);
            }}
            value={editFecha}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        <img
          onClick={addOperacion}
          src={rutaImagenes + 'aceptar.png'}
          title="Confirmar"
          alt="Confirmar"
          width="25"
          height="25"
          className="noborder operacionEditImg"
        />

        <p> Cantidad Compra</p>
        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            onChange={(event) => {
              event.preventDefault();
              seteditCantidad(event.target.value);
            }}
            value={editCantidad}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        <img
          onClick={() => {
            seteditarOperacion(false);
          }}
          src={rutaImagenes + 'cancelar.png'}
          title="Cancelar"
          alt="Cancelar"
          width="25"
          height="25"
          className="noborder operacionEditImg"
        />

        <p> Precio Compra</p>

        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            align="left"
            onChange={(event) => {
              event.preventDefault();
              seteditPrecioCompra(event.target.value);
            }}
            value={editPrecioCompra}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>
        <div></div>
        <p> Comision Compra</p>
        <label htmlFor={idgrupo}>
          <input
            disabled={disableInput}
            onChange={(event) => {
              event.preventDefault();
              seteditComision(event.target.value);
            }}
            value={editComision}
            className="inputEditOperation"
            autoComplete="off"
            type="text"
          />
        </label>

        <div></div>
      </div>
      <br></br>
    </>
  ) : (
    <h3>No eres Admin del grupo, no puedes añadir operaciones</h3>
  );
}

export default Operacionadd;
