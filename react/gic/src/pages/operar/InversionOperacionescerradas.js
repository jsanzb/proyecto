import { useState, useEffect, Fragment } from 'react';
import { getApiDataAxios, classVar, parseSybaseDate } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';
import Operacioncerrada from './Operacioncerrada';

function InversionOperacionesCerradas({ user, idGroup, roleUser, setAlert }) {
  const [operations, setOperations] = useState([]);
  const [tableState, setTableState] = useState(false);
  const [editarOperacion, seteditarOperacion] = useState(false);

  const rutaImagenes = `${global.config.backend}/static/images/`;

  // Obtenemos los datos de operaciones cerradas del usuario del Back
  async function getOperationsGroups() {
    const url = `${global.config.backend}/operations/close/${idGroup}`;
    const metodo = 'get';
    const datos = {};
    const cabeceras = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setOperations(data.data.data);
      setTableState(true);
    } else {
      setTableState(data.statusText);
    }
  }

  // Plegar y desplegar operaciones
  function desplegarEdit(event) {
    if (editarOperacion === event.target.dataset.operacion) {
      seteditarOperacion(false);
    } else {
      seteditarOperacion(event.target.dataset.operacion);
    }
  }

  useEffect(() => {
    getOperationsGroups();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return user.isUserLogged && tableState ? (
    <>
      <div className="tablasOperaciones">
        <table className="tablaOperaciones">
          <thead></thead>
          <tbody>
            <tr key="titulo">
              <td align="center">Mon.</td>
              <td align="center">Fecha Venta.</td>
              <td align="center">Cant. Compra</td>
              <td align="center">Total Compra</td>
              <td align="center">Cant. Venta</td>
              <td align="center">Total Venta</td>
              <td align="center">Total Comis.</td>
              <td align="center">Rend. Bruto</td>
              <td align="center">%Rend. Bruto</td>
            </tr>

            {operations.map((operation) => (
              <Fragment key={operation.id}>
                <tr key={operation.id} className="operacionesLineas">
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    className="moneda"
                    title={operation.moneda}
                  >
                    <img
                      onClick={desplegarEdit}
                      data-operacion={operation.id}
                      valign="middle"
                      src={
                        rutaImagenes +
                        operation.moneda.slice(
                          0,
                          operation.moneda.indexOf('-')
                        ) +
                        '.png'
                      }
                      alt={operation.moneda}
                      width="13"
                      height="13"
                      className="noborder"
                    />{' '}
                    <span onClick={desplegarEdit} data-operacion={operation.id}>
                      {operation.moneda.slice(0, operation.moneda.indexOf('-'))}
                    </span>
                  </td>
                  <td
                    onClick={desplegarEdit}
                    align="center"
                    data-operacion={operation.id}
                  >
                    {parseSybaseDate(operation.fecha_venta)}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    align="center"
                    data-operacion={operation.id}
                  >
                    {operation.numero_compra.toFixed(3) * 1}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    align="center"
                    data-operacion={operation.id}
                  >
                    {(
                      operation.numero_compra * operation.precio_compra
                    ).toFixed(2) * 1}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    align="center"
                    data-operacion={operation.id}
                  >
                    {operation.numero_venta.toFixed(3) * 1}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    align="center"
                    data-operacion={operation.id}
                  >
                    {(operation.numero_venta * operation.precio_venta).toFixed(
                      3
                    ) * 1}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    align="center"
                    data-operacion={operation.id}
                  >
                    {(
                      operation.comision_compra + operation.comision_venta
                    ).toFixed(2) * 1}
                  </td>

                  <td
                    data-operacion={operation.id}
                    onClick={desplegarEdit}
                    className={classVar(
                      operation.precio_compra,
                      operation.precio_venta
                    )}
                    align="center"
                  >
                    {operation.rendimiento_bruto.toFixed(2) * 1}
                  </td>
                  <td
                    data-operacion={operation.id}
                    onClick={desplegarEdit}
                    className={classVar(
                      operation.precio_compra,
                      operation.precio_venta
                    )}
                    align="center"
                  >
                    {operation.porcentaje_bruto.toFixed(2) * 1}
                  </td>
                </tr>
                {/* Desplegar Operacion */}
                {editarOperacion * 1 === operation.id * 1 && (
                  <tr key="-1">
                    <td colSpan="9" align="center">
                      <Operacioncerrada
                        idgrupo={idGroup}
                        user={user}
                        setAlert={setAlert}
                        setOperations={setOperations}
                        operations={operations}
                        operationId={operation.id}
                        numero_compra={operation.numero_compra}
                        precio_compra={operation.precio_compra}
                        fecha_compra={parseSybaseDate(operation.fecha_compra)}
                        comision_compra={operation.comision_compra}
                        numero_venta={operation.numero_venta}
                        precio_venta={operation.precio_venta}
                        fecha_venta={parseSybaseDate(operation.fecha_venta)}
                        comision_venta={operation.comision_venta}
                        moneda={operation.moneda}
                        seteditarOperacion={seteditarOperacion}
                      />
                    </td>
                  </tr>
                )}
              </Fragment>
            ))}
          </tbody>
        </table>
      </div>
    </>
  ) : (
    <>
      <div>
        <Cargandodatos texto="Recuperando Datos de Operaciones.." />
      </div>
    </>
  );
}

export default InversionOperacionesCerradas;
