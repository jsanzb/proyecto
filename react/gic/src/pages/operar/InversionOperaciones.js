import { useState, useEffect, Fragment } from 'react';
import { getApiDataAxios, classVar, parseSybaseDate } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';
import Operacion from './Operacion';
import Operacionadd from './Operacionadd';

function InversionOperaciones({
  user,
  idGroup,
  roleUser,
  setAlert,
  confirm,
  setConfirm,
}) {
  const [operations, setOperations] = useState([]);
  const [tableState, setTableState] = useState(false);
  const [editarOperacion, seteditarOperacion] = useState(false);
  const [addOperacion, setaddOperacion] = useState(false);

  const rutaImagenes = `${global.config.backend}/static/images/`;

  // Obtenemos los datos de operaciones del usuario del Back
  async function getOperationsGroups() {
    const url = `${global.config.backend}/inversion/operations/${idGroup}`;
    const metodo = 'get';
    const datos = {};
    const cabeceras = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setOperations(data.data.data);
      setTableState(true);
    } else {
      setTableState(data.statusText);
    }
  }

  // Plegar y desplegar operaciones
  function desplegarEdit(event) {
    if (editarOperacion === event.target.dataset.operacion) {
      seteditarOperacion(false);
    } else {
      seteditarOperacion(event.target.dataset.operacion);
    }
  }

  useEffect(() => {
    getOperationsGroups();

    const intervalID = setInterval(() => {
      getOperationsGroups();
    }, 60000);
    return () => {
      clearInterval(intervalID);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [idGroup]);

  return user.isUserLogged && tableState ? (
    <>
      <div className="tablasOperaciones">
        <table className="tablaOperaciones">
          <thead></thead>
          <tbody>
            <tr key="titulo">
              <td align="center">Mon.</td>
              <td align="center">Fecha Comp.</td>
              <td align="center">Cant.</td>
              <td align="center">Pre. Comp.</td>
              <td align="center">Com. Comp.</td>
              <td align="center">Pre. Actual</td>
              <td align="center">Rend.</td>
              <td align="center">%Rend.</td>
            </tr>

            {operations.map((operation) => (
              <Fragment key={operation.id}>
                <tr key={operation.id} className="operacionesLineas">
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    className="moneda"
                    title={operation.moneda}
                  >
                    <img
                      onClick={desplegarEdit}
                      data-operacion={operation.id}
                      data-cantidad={operation.numero_compra}
                      data-precio_compra={operation.precio_compra}
                      data-fecha_compra={parseSybaseDate(
                        operation.fecha_compra
                      )}
                      data-comision_compra={operation.comision_compra}
                      valign="middle"
                      src={
                        rutaImagenes +
                        operation.moneda.slice(
                          0,
                          operation.moneda.indexOf('-')
                        ) +
                        '.png'
                      }
                      alt={operation.moneda}
                      width="13"
                      height="13"
                      className="noborder"
                    />{' '}
                    <span
                      onClick={desplegarEdit}
                      data-operacion={operation.id}
                      data-cantidad={operation.numero_compra}
                      data-precio_compra={operation.precio_compra}
                      data-fecha_compra={parseSybaseDate(
                        operation.fecha_compra
                      )}
                      data-comision_compra={operation.comision_compra}
                    >
                      {operation.moneda.slice(0, operation.moneda.indexOf('-'))}
                    </span>
                  </td>
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    align="center"
                  >
                    {parseSybaseDate(operation.fecha_compra)}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    align="center"
                  >
                    {operation.numero_compra.toFixed(3) * 1}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    align="center"
                  >
                    {operation.precio_compra.toFixed(3) * 1}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    align="center"
                  >
                    {operation.comision_compra.toFixed(2)}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    className="precio_actual"
                    align="center"
                  >
                    {operation.precio_actual.toFixed(3) * 1}
                  </td>

                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    className={classVar(
                      operation.precio_total_compra,
                      operation.precio_total_actual
                    )}
                    align="center"
                  >
                    {operation.rendimiento.toFixed(1) * 1}
                  </td>
                  <td
                    onClick={desplegarEdit}
                    data-operacion={operation.id}
                    data-cantidad={operation.numero_compra}
                    data-precio_compra={operation.precio_compra}
                    data-fecha_compra={parseSybaseDate(operation.fecha_compra)}
                    data-comision_compra={operation.comision_compra}
                    className={classVar(
                      operation.precio_compra,
                      operation.precio_actual
                    )}
                    align="center"
                  >
                    {operation.var_rendimiento
                      ? operation.var_rendimiento.toFixed(2) * 1
                      : '---'}
                  </td>
                </tr>

                {/* Desplegar Operacion */}
                {editarOperacion * 1 === operation.id * 1 && (
                  <tr key="-1">
                    <td colSpan="8" align="center">
                      <Operacion
                        idgrupo={idGroup}
                        user={user}
                        setAlert={setAlert}
                        setOperations={setOperations}
                        operations={operations}
                        operationId={operation.id}
                        cantidad={operation.numero_compra}
                        precio_compra={operation.precio_compra}
                        fecha_compra={parseSybaseDate(operation.fecha_compra)}
                        comision_compra={operation.comision_compra}
                        moneda={operation.moneda}
                        precio_actual={operation.precio_actual}
                        seteditarOperacion={seteditarOperacion}
                        roleUser={roleUser}
                        confirm={confirm}
                        setConfirm={setConfirm}
                      />
                    </td>
                  </tr>
                )}
              </Fragment>
            ))}
            <tr key="0" className="operacionesLineasAdd">
              <td
                colSpan="8"
                align="center"
                valign="middle"
                onClick={() => {
                  setaddOperacion(!addOperacion);
                }}
              >
                <strong>
                  <h3
                    onClick={() => {
                      setaddOperacion(!addOperacion);
                    }}
                  >
                    Abrir Nueva Operacion...
                  </h3>
                </strong>
                <img
                  onClick={() => {
                    setaddOperacion(!addOperacion);
                  }}
                  src={rutaImagenes + 'agregar.png'}
                  title="Abrir Operacion (Comprar)"
                  alt="Abrir Operacion"
                  width="12"
                  height="12"
                />
              </td>
            </tr>
            {addOperacion && (
              <tr key="-2">
                <td colSpan="8" align="center">
                  <Operacionadd
                    idgrupo={idGroup}
                    user={user}
                    setAlert={setAlert}
                    seteditarOperacion={setaddOperacion}
                    roleUser={roleUser}
                  />
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </>
  ) : (
    <>
      <div>
        <Cargandodatos texto="Recuperando Datos de Operaciones.." />
      </div>
    </>
  );
}

export default InversionOperaciones;
