import { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { getApiDataAxios } from '../../helpers';
import './login.css';

function Login({ user, setUser, setAlert, setSelMenu }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  let history = useHistory();
  document.title = 'giC-Login';

  // Funcion Llamada a Backend para Login
  async function onSubmitLogin(event) {
    event.preventDefault();
    const url = `${global.config.backend}/user/login`;
    const metodo = 'post';
    const datos = { email, password };
    const data = await getApiDataAxios(url, metodo, datos);

    if (data.status === 200) {
      setUser({
        idUser: data.data.data.idUser,
        emailUser: data.data.data.email,
        roleUser: data.data.data.role,
        avatarUser: data.data.data.avatar,
        isUserLogged: true,
      });
      window.localStorage.setItem('accesstoken', data.data.data.token);
      window.localStorage.setItem('iduser', data.data.data.idUser);
      window.localStorage.setItem('emailUser', data.data.data.email);
      data.data.data.role === 'admin' &&
        window.localStorage.setItem('vista', '1');
      window.localStorage.setItem('avatarUser', data.data.data.avatar);
      history.push('/consultar');
    } else {
      // setLoginError(data.message);
      window.localStorage.removeItem('accesstoken');
      window.localStorage.removeItem('iduser');
      window.localStorage.removeItem('emailUser');
      window.localStorage.removeItem('avatarUser');
      setAlert('1-Usuario o Contraseña Incorrectos');
      history.push('/login');
    }
  }

  useEffect(() => {
    setSelMenu('Login');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="loginDetails">
      <h3>Introduce tu Correo y Contraseña</h3>
      <div className="login-container">
        <form onSubmit={onSubmitLogin}>
          <label className="field-container">
            <br></br>
            Correo Electronico (*)
            <br></br>
            <input
              className="inputLogin"
              value={email}
              autoComplete="on"
              onChange={(event) => {
                setEmail(event.target.value);
              }}
              type="email"
            />
          </label>
          <br></br>
          <label className="field-container">
            Contraseña (*)
            <br></br>
            <input
              className="inputLogin"
              type="password"
              autoComplete="on"
              value={password}
              onChange={(event) => {
                setPassword(event.target.value);
              }}
            />
          </label>
          <br></br>
          <br></br>
          <input className="button" type="submit" value="Iniciar Sesion" />
        </form>
        <br></br>
        <br></br>
        <Link to="/resetpassword">
          <h3 align="center">Contraseña Olvidada?</h3>
        </Link>
        <Link to="/nuevousuario">
          <h2 align="center">Nuevo Usuario?</h2>
        </Link>
      </div>
    </div>
  );
}

export default Login;
