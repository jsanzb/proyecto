import { useState } from 'react';
import { getApiDataAxios } from '../../helpers';
import { Link } from 'react-router-dom';
import './nuevousuario.css';

function Nuevousuario({ setAlert }) {
  const [email, setEmail] = useState('');
  const [alias, setAlias] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [usuarioCreado, setUsuarioCreado] = useState(false);
  let error = false;
  document.title = 'giC-Nuevo Usuario';

  // Funcion Llamada a Backend para Login
  async function onSubmitLogin(event) {
    event.preventDefault();

    if (!email && !error) {
      setAlert('1-No has introducido email');
      error = true;
    }

    if (password !== password2 && !error) {
      setAlert('1-No coinciden los passwords');
      error = true;
    }
    if (!password && !error) {
      setAlert('1-No has introducido password ');
      error = true;
    }
    if (password.length < 6 && !error) {
      setAlert('1-La contraseña debe tener 6 caracteres o mas');
      error = true;
    }

    if (!error) {
      const url = `${global.config.backend}/user`;
      const metodo = 'post';
      const datos = { email, password, alias };

      const data = await getApiDataAxios(url, metodo, datos);
      if (data.status === 200) {
        setAlert('0-Usuario creado, comprueba el correo para activarlo');
        setUsuarioCreado(true);
      } else {
        setAlert('1-' + data.data.message);
      }
      console.log(data);
    }
    error = false;
  }

  return (
    <div className="newUserDetails">
      <h3>Registro de Nuevo Usuario</h3>
      <div className="newUser-container">
        {usuarioCreado ? (
          <div id="userCreado">
            <h2>Usuario creado, revisa tu correo para activarlo.</h2>
          </div>
        ) : (
          <>
            <form onSubmit={onSubmitLogin}>
              <label className="field-container">
                <br></br>
                Correo Electronico (*)
                <br></br>
                <input
                  className="inputNewUser"
                  value={email}
                  autoComplete="on"
                  onChange={(event) => {
                    setEmail(event.target.value);
                  }}
                  type="email"
                />
              </label>
              <label className="field-container">
                Alias
                <br></br>
                <input
                  className="inputNewUser"
                  value={alias}
                  type="text"
                  autoComplete="alias"
                  onChange={(event) => {
                    setAlias(event.target.value);
                  }}
                />
              </label>
              <br></br>
              <label className="field-container">
                <br></br>
                Password (*)
                <br></br>
                <input
                  className="inputNewUser"
                  type="password"
                  autoComplete="new-password"
                  value={password}
                  onChange={(event) => {
                    setPassword(event.target.value);
                  }}
                />
              </label>
              <label className="field-container">
                Repite Password (*)
                <br></br>
                <input
                  className="inputNewUser"
                  type="password"
                  autoComplete="new-password"
                  value={password2}
                  onChange={(event) => {
                    setPassword2(event.target.value);
                  }}
                />
              </label>
              <br></br>
              <br></br>
              <input className="button" type="submit" value="Crear Usuario" />
              <br></br>
            </form>
            <Link to="/login">
              <h3 align="center"> Ya Tienes Usuario?</h3>
            </Link>
          </>
        )}
      </div>
    </div>
  );
}

export default Nuevousuario;
