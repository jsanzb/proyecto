import { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import Cargandodatos from '../../components/cargando/Cargando';
import { getApiDataAxios, parseSybaseDate } from '../../helpers';
import './administrar.css';

function Administrar({ user, setUser, setSelMenu, setAlert }) {
  const [varlogs, setvarLogs] = useState([]);
  const [tableState, settableState] = useState(false);
  const [users, setUsers] = useState([]);
  const [selUser, setselUser] = useState('');
  const [responseSimulation, setresponseSimulation] = useState(false);
  document.title = 'giC-Aprender';

  let history = useHistory();
  // No permitimos la entrada si no tenemos token y de admin (falta por hacer)
  if (!user.isUserLogged || user.roleUser !== 'admin') {
    setAlert(
      '1-No has iniciado Sesión en la plataforma. Solo podrás ver los datos de cotizaciones del Mercado. Inicia Sesión o créate una cuenta.'
    );
    history.push('/login');
  }

  // Llamada a Backend para Ver las tablas de Log
  async function getTablalogs(url, metodo, datos) {
    const data = await getApiDataAxios(url, metodo, datos);
    setvarLogs(data.data);
    settableState(true);
  }

  // Llamada a Backend para Select de usuarios
  async function getTablausers(url, metodo, datos) {
    const data = await getApiDataAxios(url, metodo, datos);
    setUsers(data.data);
  }

  // Funcion Llamada a Backend lanzar Simulacion
  async function getSimulationUser(url, metodo, datos, cabecera) {
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    data.status === 200
      ? setresponseSimulation('Simulacion Creada')
      : setresponseSimulation(data.data.message);
  }

  // Pasar datos de Simulacion
  function simulacionUser() {
    selUser
      ? getSimulationUser(
          `${global.config.backend}/operations/simulation/` + selUser,
          'post',
          { userId: user.idUser },
          {
            authorization: window.localStorage.getItem('accesstoken'),
            'Content-Type': 'application/json',
          }
        )
      : setresponseSimulation(false);
  }

  // Funcion Llamada a Backend borrar Usuario
  async function getDeleteUser(url, metodo, datos, cabecera) {
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setresponseSimulation('Usuario Borrado');

      // Si el usuario a borrar el es mismo que el logueado, hacemos logut
      if (1 * selUser === 1 * user.idUser) {
        setUser({
          idUser: 0,
          emailUser: null,
          isUserLogged: false,
        });
        window.localStorage.removeItem('accesstoken');
        window.localStorage.removeItem('iduser');
        window.localStorage.removeItem('emailUser');
        // window.localStorage.removeItem('roleUser');
        history.push('/login');
      }
    } else {
      setresponseSimulation(data.data.message);
    }
  }

  // Pasar Datos Eliminar Usuario
  function deleteUser() {
    selUser
      ? getDeleteUser(
          `${global.config.backend}/user/` + selUser,
          'delete',
          { userId: user.idUser },
          {
            authorization: window.localStorage.getItem('accesstoken'),
            'Content-Type': 'application/json',
          }
        )
      : setresponseSimulation(false);
  }

  //Carga Previa
  useEffect(() => {
    getTablalogs(`${global.config.backend}/api/Log_resumen`, 'get');
  }, []);
  useEffect(() => {
    setSelMenu('Administrar');
    getTablausers(`${global.config.backend}/api/Tusers`, 'get');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return tableState ? (
    <>
      <div className="tablasAdministrarAcciones">
        <p>Administracion Usuarios</p>
        <label>
          User
          <select
            onChange={(event) => {
              setselUser(event.target.value);
              setresponseSimulation(false);
            }}
          >
            <option key="0" value={null}></option>
            {users.map((user) => (
              <option key={user.id} value={user.id}>
                {user.email}
              </option>
            ))}
          </select>
        </label>
        <button onClick={simulacionUser}>Lanzar Sim.</button>
        <button onClick={deleteUser}>Borrar User</button>
        <br></br>
        <p>{responseSimulation}</p>
      </div>
      <br></br>
      <div className="tablasAdministrar">
        <p>Historico de Operaciones</p>
        <table className="tablaAdministrar">
          <tbody>
            <tr className="tablaHeader">
              <td align="center">Fecha</td>
              <td align="center">Acciones</td>
            </tr>
            {varlogs.map((varlog) => (
              <tr key={Date.parse(varlog.Fecha) + varlog.id}>
                <td>{parseSybaseDate(varlog.Fecha)}</td>
                <td>{varlog.Accion}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  ) : (
    <>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <Cargandodatos texto="Recuperando Datos de Administracion.." />
    </>
  );
}

export default Administrar;
