import Editpassworduser from '../profile/Editpassworduser';
import { useParams } from 'react-router-dom';

function Confirmresetpassword({ user, setUser, setAlert }) {
  const { recoverCode } = useParams();
  return (
    <div className="profileDetails">
      <h2>Recuperacion de Contraseña</h2>
      <Editpassworduser
        setUser={setUser}
        user={user}
        recover={recoverCode}
        setAlert={setAlert}
      />
    </div>
  );
}

export default Confirmresetpassword;
