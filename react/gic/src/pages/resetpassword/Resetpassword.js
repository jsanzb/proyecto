import { useState } from 'react';
import { Link } from 'react-router-dom';
import { getApiDataAxios } from '../../helpers';
import './resetpassword.css';

function Resetpassword({ setAlert }) {
  const [email, setEmail] = useState('');
  document.title = 'giC-Reset Contraseña';

  // Funcion Llamada a Backend para Login
  async function onSubmitEmail(event) {
    event.preventDefault();
    const url = `${global.config.backend}/user/password/recover`;
    const metodo = 'put';
    const datos = { email };
    const data = await getApiDataAxios(url, metodo, datos);

    if (data.status === 200) {
      setAlert('0-Comprueba tu correo electronico para resetear tu contraseña');
    } else {
      setAlert('1-Error reseteando la contraseña:' + data.data.message);
    }
  }

  return (
    <div className="resetpassDetails">
      <h3>Para Resetear tu contraseña</h3>
      <h3>Introduce tu Correo</h3>
      <div className="login-container">
        <form onSubmit={onSubmitEmail}>
          <label className="field-container">
            <br></br>
            Correo Electronico
            <br></br>
            <input
              value={email}
              autoComplete="on"
              onChange={(event) => setEmail(event.target.value)}
              type="email"
            />
          </label>
          <input className="button" type="submit" value="Enviar" />
        </form>

        <Link to="/login">
          <h3 align="center">Ya Tienes Usuario?</h3>
        </Link>

        <Link to="/nuevousuario">
          <h2 align="center">Nuevo Usuario?</h2>
        </Link>
      </div>
    </div>
  );
}

export default Resetpassword;
