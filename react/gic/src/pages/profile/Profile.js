import { useEffect } from 'react';
import Editdatauser from './Editdatauser';
import Editpassworduser from './Editpassworduser';
import { useHistory } from 'react-router';
import './profile.css';
import { getApiDataAxios } from '../../helpers';

function Profile({
  user,
  setUser,
  alert,
  setAlert,
  setSelMenu,
  input,
  setInput,
  valorinput,
  setValorinput,
}) {
  document.title = 'giC-Perfil Usuario';

  let history = useHistory();

  if (!user.isUserLogged) {
    setAlert(
      '1-No has iniciado Sesión en la plataforma. Solo podrás ver los datos de cotizaciones del Mercado. Inicia Sesión o créate una cuenta.'
    );
    history.push('/login');
  }

  // Funcion Llamada a Backend para Login
  async function onSubmitLogin(email, password) {
    const url = `${global.config.backend}/user/login`;
    const metodo = 'post';
    const datos = { email, password };
    const data = await getApiDataAxios(url, metodo, datos);
    if (data.status === 200) {
      return true;
    } else {
      return false;
    }
  }

  // Preborrado Usuario
  function pregetDeleteUser(event) {
    if (!input) {
      setInput({
        aviso: 1,
        mensaje: `Vas a borrar tu usuario y todos los datos asociados. Introduce tu contraseña para confirmar el borrado.`,
        proceso: 'getDeleteUser',
        longitud: 30,
        password: true,
      });
    }
  }

  // Funcion Llamada a Backend borrar Usuario
  async function getDeleteUser() {
    if (await onSubmitLogin(user.emailUser, valorinput)) {
      const url = `${global.config.backend}/user/` + user.idUser;
      const metodo = 'delete';
      const datos = {};
      const cabecera = {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      };
      const data = await getApiDataAxios(url, metodo, datos, cabecera);
      if (data.status === 200) {
        setAlert('0-Usuario Borrado Completamente');
        setUser({
          idUser: 0,
          emailUser: null,
          isUserLogged: false,
        });
        window.localStorage.removeItem('accesstoken');
        window.localStorage.removeItem('iduser');
        window.localStorage.removeItem('emailUser');
        history.push('/login');
      } else {
        setAlert('1-No se ha podido borrar el usuario' + data.data.message);
      }
    } else {
      setAlert('1-Ha fallado el borrado, comprueba la contraseña');
    }
    setInput(false);
    setValorinput(false);
  }

  useEffect(() => {
    setSelMenu(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    input === 'getDeleteUser' && getDeleteUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [input]);

  return (
    <div className="profileDetails">
      <h3>{user.emailUser}</h3>
      <Editdatauser
        setUser={setUser}
        user={user}
        alert={alert}
        setAlert={setAlert}
      />
      <Editpassworduser
        setUser={setUser}
        user={user}
        recover={false}
        alert={alert}
        setAlert={setAlert}
      />
      <div>
        <input
          id="deleteUser"
          onClick={pregetDeleteUser}
          className="button"
          type="submit"
          value="Eliminar Usuario"
        />
      </div>
    </div>
  );
}

export default Profile;
