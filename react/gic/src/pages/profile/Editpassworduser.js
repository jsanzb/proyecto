import { useState } from 'react';
import { getApiDataAxios } from '../../helpers';
import { useHistory } from 'react-router';

function Editpassworduser({ user, setUser, recover, setAlert }) {
  const [passwordOld, setPasswordOld] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');

  let history = useHistory();
  let url;
  let metodo;
  let datos;
  let cabecera;
  let error = false;

  // Funcion Llamada a Backend para cambiar Password
  async function onSubmitChangePassword(event) {
    event.preventDefault();

    if (!passwordOld && !recover && !error) {
      setAlert('1-No has introducido la contraseña antigua');
      error = true;
    }

    if (password.length < 6 && !error) {
      setAlert('1-La contraseña debe tener 6 caracteres o mas');
      error = true;
    }

    if (password !== password2 && !error) {
      setAlert('1-No coinciden las contraseñas');
      error = true;
    }

    if (!password && !error) {
      setAlert('1-No has introducido la nueva contraseña');
      error = true;
    }
    if (!error) {
      console.log('entra');
      if (recover) {
        url = `${global.config.backend}/user/password/reset`;
        metodo = 'put';
        datos = {
          recoverCode: recover,
          newPassword: password,
        };
      } else {
        url = `${global.config.backend}/user/${user.idUser}/password`;
        metodo = 'put';
        datos = {
          oldPassword: passwordOld,
          newPassword: password,
        };
        cabecera = {
          authorization: window.localStorage.getItem('accesstoken'),
          'Content-Type': 'application/json',
        };
      }
      const data = await getApiDataAxios(url, metodo, datos, cabecera);
      if (data.status === 200) {
        setPasswordOld('');
        setPassword('');
        setPassword2('');
        if (recover) {
          setAlert(
            '0-Contraseña actualizada, ya puedes hacer login en la plataforma.'
          );
          history.push('/login');
        } else {
          setAlert('0-Contraseña Actualizada');
          history.push('/profile');
        }
      } else {
        setAlert('1-' + data.data.message);
      }
    }
    error = false;
  }

  return (
    <div className="profile_password">
      <form onSubmit={onSubmitChangePassword}>
        {!recover && (
          <label className="field-container">
            Contraseña Actual
            <br></br>
            <input
              type="password"
              autoComplete="on"
              value={passwordOld}
              onChange={(event) => {
                setPasswordOld(event.target.value);
              }}
            />
          </label>
        )}
        <label className="field-container">
          Nueva Contraseña
          <br></br>
          <input
            type="password"
            autoComplete="on"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />
        </label>
        <label className="field-container">
          Repite Contraseña
          <input
            type="password"
            autoComplete="on"
            value={password2}
            onChange={(event) => {
              setPassword2(event.target.value);
            }}
          />
        </label>
        <input className="button" type="submit" value="Cambiar Contraseña" />
      </form>
    </div>
  );
}

export default Editpassworduser;
