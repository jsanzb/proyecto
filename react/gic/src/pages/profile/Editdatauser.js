import { useState, useEffect } from 'react';
// import Modal from '../../components/modal/Modal';
import { getApiDataAxios } from '../../helpers';

function Editdatauser({ user, setUser, setAlert }) {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [alias, setAlias] = useState('');
  const [changeDataError, setchangeDataError] = useState(false);
  const [file, setFile] = useState();
  const [fileUrl, setFileUrl] = useState(null);

  // Llamada a Backend para datos y avatar
  async function onSubmitChangeData(event) {
    event.preventDefault();
    setchangeDataError(false);
    let dataForm = new FormData();
    dataForm.append('name', name);
    dataForm.append('alias', alias);
    dataForm.append('avatar', file);

    const url = `${global.config.backend}/user/${user.idUser}`;
    const metodo = 'put';
    const datos = dataForm;
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      data.data.avatar &&
        window.localStorage.setItem('avatarUser', data.data.avatar);
      data.data.avatar &&
        setUser({
          idUser: user.idUser,
          emailUser: user.emailUser,
          roleUser: user.roleUser,
          avatarUser: data.data.avatar,
          isUserLogged: user.isUserLogged,
        });
      setAlert('0-Datos Actualizados');
      setchangeDataError(false);
      setFileUrl(false);
    } else {
      setAlert('1-Error modificando datos del Usuario: ' + data.data.message);
    }
  }

  const onFileChange = (event) => {
    if (event.target.files[0]) {
      const selectFile = event.target.files[0];
      if (selectFile.type.includes('image')) {
        setFile(selectFile);
        const imageUrl = URL.createObjectURL(selectFile);
        setFileUrl(imageUrl);
      } else {
        setAlert('1-No has elegido un fichero de tipo imagen');
      }
    }
  };

  // Obtenemos los datos del usuario del Back
  async function getUser(url, metodo, datos, cabeceras) {
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setEmail(data.data.data.email);
      setName(data.data.data.name);
      setAlias(data.data.data.alias);
    } else {
      setchangeDataError(data.data.message);
    }
  }

  useEffect(() => {
    getUser(
      `${global.config.backend}/user/${user.idUser}`,
      'get',
      {},
      {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      }
    );
  }, [user]);

  return (
    <div className="profile_data">
      <form onSubmit={onSubmitChangeData}>
        <label className="campoEmail">
          Correo Electrónico
          <br></br>
          <input
            disabled={true}
            value={email}
            onChange={(event) => {
              setchangeDataError(false);
              setEmail(event.target.value);
            }}
            type="email"
          />
        </label>
        <label className="field-container">
          Nombre
          <br></br>
          <input
            type="text"
            value={name}
            onChange={(event) => {
              setchangeDataError(false);
              setName(event.target.value);
            }}
          />
        </label>
        <label className="field-container">
          Alias
          <input
            type="text"
            value={alias}
            onChange={(event) => {
              setchangeDataError(false);
              setAlias(event.target.value);
            }}
          />
        </label>
        Avatar (Click para Editar)
        <div>
          <label className="cambiarAvatar">
            <img
              className="cambiarAvatarimg"
              width="80px"
              height="80px"
              title="Cambiar Avatar"
              src={`${global.config.backend}/static/uploads/${user.avatarUser}`}
              // src={file}
              alt="Imagen Avatar"
              onClick={() => {
                setchangeDataError(false);
              }}
            />
            <input
              id="elegirFicherohidden"
              type="file"
              accept=".jpg, .jpeg, .png"
              onChange={onFileChange}
            />
          </label>
          {fileUrl && (
            <img
              className="cambiarAvatarimg"
              width="80px"
              height="80px"
              title="Imagen Seleccionada"
              src={fileUrl}
              alt="Imagen Avatar Seleccionada"
            />
          )}
        </div>
        <input className="button" type="submit" value="Modificar Datos" />
      </form>

      {changeDataError && (
        <div className="mensajeErrorcenter">{changeDataError} </div>
      )}
    </div>
  );
}

export default Editdatauser;
