import { useState, useEffect } from 'react';
import { getApiDataAxios, classVar, parseSybaseDate } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';
import './inversionTotalGrupos.css';
import InversionTotalOperaciones from './InversionTotalOperaciones';
let arrayGroups = [];

function InversionTotalGrupos({ user, setAlert }) {
  const [groups, setGroups] = useState([]);
  const [tableState, setTableState] = useState(false);
  const [simpleViewGroups, setSimpleViewGroups] = useState(
    window.localStorage.getItem('vistaSimpleGrupos')
  );
  // eslint-disable-next-line no-unused-vars
  const [selectGroup, setSelectGroup] = useState([]);

  // Obtenemos los datos de grupos del usuario del Back
  async function getGroupsUser(url, metodo, datos, cabeceras) {
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setGroups(data.data.data);

      // || !data.data.data[0].precio_actual

      data.data.data.length === 0 ||
      (data.data.data.length === 1 && data.data.data[0].precio_compra === null)
        ? setTableState(0)
        : setTableState(-1);
    } else {
      setTableState(data.statusText);
    }
  }

  // Toggle para vista simple grupos
  function onChangeSimpleViewGroups(event) {
    setSimpleViewGroups(event.target.value);
    window.localStorage.setItem('vistaSimpleGrupos', event.target.value);
    if (event.target.value === '1') {
      arrayGroups = [];
      setSelectGroup([arrayGroups]);
    } else {
      arrayGroups = [];
      for (const grupo of groups) {
        arrayGroups.push(grupo.id * 1);
      }
      setSelectGroup([arrayGroups]);
    }
  }

  // Expansion de grupos
  function verDetallesGrupo(event) {
    let index = arrayGroups.indexOf(event.target.dataset.grupo * 1);
    if (index > -1) {
      arrayGroups.splice(index, 1);
    } else {
      arrayGroups.push(event.target.dataset.grupo * 1);
    }
    setSelectGroup([...arrayGroups]);
  }

  useEffect(() => {
    getGroupsUser(
      `${global.config.backend}/inversion/groups/${user.idUser}`,
      'get',
      { userId: user.idUser },
      {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      }
    );
    const intervalID = setInterval(() => {
      getGroupsUser(
        `${global.config.backend}/inversion/groups/${user.idUser}`,
        'get',
        { userId: user.idUser },
        {
          authorization: window.localStorage.getItem('accesstoken'),
          'Content-Type': 'application/json',
        }
      );
    }, 60000);
    return () => {
      clearInterval(intervalID);
    };
  }, [user.idUser]);

  // user.isUserLogged &&

  return tableState === -1 ? (
    <div className="tablasInversion">
      <p className="tituloTabla">
        Grupos de Inversion {' (' + new Date().toLocaleString() + ')'}
      </p>
      <div className="contenedorOpciones">
        <label
          className="labelvistaTotalesGrupos opciones-label"
          htmlFor="vistaTotalesGrupos"
        >
          Ver Solo Totales
        </label>
        <input
          onChange={onChangeSimpleViewGroups}
          value={simpleViewGroups}
          className="vistaTotalesGrupos opciones"
          type="range"
          name="vistaTotalesGrupos"
          min="0"
          max="1"
        />
      </div>

      {groups.map(
        (group) =>
          group.precio_compra && (
            <div key={group.id} className="tablaInversion">
              <table className="tablaInversiones">
                <thead></thead>
                <tbody>
                  {simpleViewGroups === '1' &&
                    arrayGroups.indexOf(group.id * 1) === -1 && (
                      <tr>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Nombre
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Fecha Creacion
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Val. Compra
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Val. Actual
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Rend.
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          %Rend.
                        </td>
                      </tr>
                    )}

                  {simpleViewGroups === '0' ||
                  arrayGroups.indexOf(group.id * 1) !== -1 ? (
                    <>
                      <tr>
                        <td
                          align="center"
                          className="noborder"
                          colSpan="3"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Nombre
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        ></td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Val. Compra
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Val. Actual
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Rend.
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          %Rend.
                        </td>
                      </tr>
                      <tr className="filaTotales">
                        <td
                          align="center"
                          className="noborder"
                          colSpan="3"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {`Grupo ${group.name}`}
                        </td>

                        <td
                          align="right"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          Totales
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {group.precio_compra
                            ? group.precio_compra.toFixed(1) * 1
                            : '---'}
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                          className="precio_actual"
                        >
                          {group.precio_actual
                            ? group.precio_actual.toFixed(1) * 1
                            : '---'}
                        </td>
                        <td
                          className={classVar(
                            group.precio_compra,
                            group.precio_actual
                          )}
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {group.rendimiento
                            ? group.rendimiento.toFixed(1) * 1
                            : '---'}
                        </td>
                        <td
                          className={classVar(
                            group.precio_compra,
                            group.precio_actual
                          )}
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {group.var_rendimiento
                            ? group.var_rendimiento.toFixed(2) * 1
                            : '---'}
                        </td>
                      </tr>

                      {arrayGroups.indexOf(group.id * 1) !== -1 && (
                        <>
                          <tr>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              Moneda
                            </td>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              Cant.
                            </td>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              Pre. Compra
                            </td>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              Pre. Actual
                            </td>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              Val. Compra
                            </td>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              Val. Actual
                            </td>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              Rend.
                            </td>
                            <td
                              align="center"
                              onClick={verDetallesGrupo}
                              data-grupo={group.id}
                            >
                              %Rend.
                            </td>
                          </tr>
                          <InversionTotalOperaciones
                            user={user}
                            idGroup={group.id}
                            setAlert={setAlert}
                          />
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <tr className="filaTotales">
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {group.name}
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {parseSybaseDate(group.createdAt)}
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {group.precio_compra
                            ? group.precio_compra.toFixed(3) * 1
                            : '---'}
                        </td>
                        <td
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                          className="precio_actual"
                        >
                          {group.precio_actual
                            ? group.precio_actual.toFixed(3) * 1
                            : '---'}
                        </td>
                        <td
                          className={classVar(
                            group.precio_compra,
                            group.precio_actual
                          )}
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {group.rendimiento
                            ? group.rendimiento.toFixed(3) * 1
                            : '---'}
                        </td>
                        <td
                          className={classVar(
                            group.precio_compra,
                            group.precio_actual
                          )}
                          align="center"
                          onClick={verDetallesGrupo}
                          data-grupo={group.id}
                        >
                          {group.var_rendimiento
                            ? group.var_rendimiento.toFixed(3) * 1
                            : '---'}
                        </td>
                      </tr>
                    </>
                  )}
                </tbody>
              </table>
            </div>
          )
      )}
    </div>
  ) : (
    <>
      {tableState === 0 ? (
        <Cargandodatos texto="1-No tienes grupos con operaciones abiertas" />
      ) : (
        <Cargandodatos texto="Recuperando Datos de Grupos.." />
      )}
    </>
  );
}

export default InversionTotalGrupos;
