import { useState, useEffect } from 'react';
import { getApiDataAxios, classVar } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';

import Monedasdetails from '../../components/monedas/Monedasdetails';

import './inversionTotalOperaciones.css';

function InversionTotalOperaciones({ user, idGroup, setAlert }) {
  const [operations, setOperations] = useState([]);
  const [tableState, setTableState] = useState(false);
  const [monedaSelect, setMonedaSelect] = useState('');
  const [onlyFav, setOnlyFav] = useState(
    window.localStorage.getItem('monedasFavoritas')
  );

  const rutaImagenes = `${global.config.backend}/static/images/`;
  // Obtenemos los datos de operaciones del usuario del Back
  async function getOperationsGroups(url, metodo, datos, cabeceras) {
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setOperations(data.data.data);
      setTableState(true);
    } else {
      setTableState(data.statusText);
    }
  }

  // OnClick para detalle monedas
  function onClickMoneda(event) {
    event.preventDefault();
    setMonedaSelect(event.target.dataset.moneda);
  }

  useEffect(() => {
    getOperationsGroups(
      `${global.config.backend}/inversion/operations/${idGroup}`,
      'get',
      {},
      {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      }
    );

    const intervalID = setInterval(() => {
      getOperationsGroups(
        `${global.config.backend}/inversion/operations/${idGroup}`,
        'get',
        {},
        {
          authorization: window.localStorage.getItem('accesstoken'),
          'Content-Type': 'application/json',
        }
      );
    }, 60000);
    return () => {
      clearInterval(intervalID);
    };
  }, [idGroup]);

  return user.isUserLogged && tableState ? (
    <>
      {operations.map((operation) => (
        <tr key={operation.id}>
          <td
            className="moneda"
            title={operation.moneda}
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
          >
            <img
              data-moneda={operation.moneda}
              onClick={onClickMoneda}
              valign="middle"
              src={
                rutaImagenes +
                operation.moneda.slice(0, operation.moneda.indexOf('-')) +
                '.png'
              }
              alt={operation.moneda}
              width="13"
              height="13"
              className="noborder"
            />
            <span data-moneda={operation.moneda} onClick={onClickMoneda}>
              {operation.moneda.slice(0, operation.moneda.indexOf('-'))}
            </span>
          </td>
          <td
            align="center"
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
          >
            {operation.numero_compra.toFixed(3) * 1}
          </td>
          <td
            align="center"
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
          >
            {operation.precio_compra.toFixed(3) * 1}
          </td>
          <td
            className="precio_actual"
            align="center"
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
          >
            {operation.precio_actual.toFixed(3) * 1}
          </td>
          <td
            align="center"
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
          >
            {operation.precio_total_compra.toFixed(1) * 1}
          </td>
          <td
            className="precio_actual"
            align="center"
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
          >
            {operation.precio_total_actual.toFixed(1) * 1}
          </td>
          <td
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
            className={classVar(
              operation.precio_total_compra,
              operation.precio_total_actual
            )}
            align="center"
          >
            {operation.rendimiento.toFixed(1) * 1}
          </td>
          <td
            data-moneda={operation.moneda}
            onClick={onClickMoneda}
            className={classVar(
              operation.precio_compra,
              operation.precio_actual
            )}
            align="center"
          >
            {operation.var_rendimiento
              ? operation.var_rendimiento.toFixed(2) * 1
              : '---'}
          </td>
        </tr>
      ))}
      {monedaSelect ? (
        <tr>
          <td>
            <Monedasdetails
              monedaSelect={monedaSelect}
              setMonedaSelect={setMonedaSelect}
              user={user}
              setAlert={setAlert}
              onlyFav={onlyFav}
              setOnlyFav={setOnlyFav}
            />
          </td>
        </tr>
      ) : null}
    </>
  ) : (
    <>
      <tr>
        <td colSpan="8">
          <Cargandodatos texto="Recuperando Datos de Operaciones.." />
        </td>
      </tr>
    </>
  );
}

export default InversionTotalOperaciones;
