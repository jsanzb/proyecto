import { useState, useEffect } from 'react';
import InversionTotalGrupos from './InversionTotalGrupos';
import TablaMercado from '../../components/tablamercado/TablaMercado';
import TopsMonedas from '../../components/stats/TopsMonedas';
import './consultar.css';

function Consultar({ user, setAlert, setSelMenu }) {
  document.title = 'giC-Consultar';
  const [changeFav, setChangeFav] = useState(false);

  useEffect(() => {
    !user.isUserLogged &&
      setAlert(
        '1-No has iniciado Sesión en la plataforma. Solo podrás ver los datos de cotizaciones del Mercado. Inicia Sesión o créate una cuenta.'
      );
    setSelMenu('Consultar');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="tablasConsultar">
      {user.isUserLogged && (
        <InversionTotalGrupos user={user} setAlert={setAlert} />
      )}
      <div className="consultarMercado">
        <TablaMercado
          user={user}
          setAlert={setAlert}
          changeFav={changeFav}
          setChangeFav={setChangeFav}
        />
        <div className="tablaMercadoTopsContenedor">
          <p className="tituloTabla">
            Las Mejores y las Peores del Periodo Actual %
          </p>
          <div className="tablasMercadoTops">
            <TopsMonedas
              user={user}
              setAlert={setAlert}
              periodo="3600"
              orden="desc"
              changeFav={changeFav}
              setChangeFav={setChangeFav}
            />
            <TopsMonedas
              user={user}
              setAlert={setAlert}
              periodo="3600"
              orden="asc"
              changeFav={changeFav}
              setChangeFav={setChangeFav}
            />
            <TopsMonedas
              user={user}
              setAlert={setAlert}
              periodo="86400"
              orden="desc"
              changeFav={changeFav}
              setChangeFav={setChangeFav}
            />
            <TopsMonedas
              user={user}
              setAlert={setAlert}
              periodo="86400"
              orden="asc"
              changeFav={changeFav}
              setChangeFav={setChangeFav}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Consultar;
