import { useEffect } from 'react';
import { useHistory } from 'react-router';
import './home.css';

function Home({ user, setSelMenu }) {
  const rutaImagenes = `${global.config.backend}/static/images/`;

  let history = useHistory();

  function clickhomeuno() {
    !user.isUserLogged ? history.push('/consultar') : history.push('/operar');
  }
  function clickhomedos() {
    !user.isUserLogged
      ? history.push('/consultar')
      : history.push('/gestionar');
  }

  function clickhometres() {
    history.push('/consultar');
  }

  document.title = 'giC-Home';

  useEffect(() => {
    setSelMenu(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="homeContenedor">
      <div className="home_uno" onClick={clickhomeuno}>
        <img
          id="homeImg1"
          src={rutaImagenes + 'home1.png'}
          alt="Imagen Home1"
        />
        <div className="home_texto">
          <p align="center">
            "Gestiona y haz seguimiento de todas tus operaciones con Criptos
            desde el mismo lugar"
          </p>
        </div>
      </div>

      <div className="home_dos" onClick={clickhomedos}>
        <div className="home_texto">
          <p align="center">
            "Crea grupos temáticos con tus operaciones o crea grupos de
            inversión compartidos con amigos y familiares"
          </p>
        </div>
        <img
          id="homeImg2"
          src={rutaImagenes + 'home2.png'}
          alt="Imagen Home2"
        />
      </div>

      <div className="home_tres" onClick={clickhometres}>
        <img
          id="homeImg3"
          src={rutaImagenes + 'home3.png'}
          alt="Imagen Home3"
        />
        <div className="home_texto">
          <p align="center">
            "Consulta las cotizaciones al minuto y usa los mapas de colores para
            visualizar las tendencias"
          </p>
        </div>
      </div>
    </div>
  );
}

export default Home;
