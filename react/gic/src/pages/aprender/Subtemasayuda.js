import { useState, useEffect, Fragment } from 'react';
import { getApiDataAxios } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';
import Contenidoayuda from './Contenidoayuda';

const rutaImagenes = `${global.config.backend}/static/images/`;
let arraytemas = [];

function Subtemasayuda({ user, idtema }) {
  const [temas, setTemas] = useState([]);
  const [tableState, setTableState] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [selectTema, setSelectTema] = useState([]);

  // Obtenemos los datos de grupos del usuario del Back
  async function getTemas() {
    const url = `${global.config.backend}/ayuda/subtemas/${idtema}`;

    const metodo = 'get';
    const datos = {};
    const cabeceras = {};
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setTemas(data.data.data);
      data.data.data.length === 0 ? setTableState(0) : setTableState(-1);
    } else {
      setTemas(data.statusText);
    }
  }

  // Expansion de temas
  function verDetallesTema(event) {
    let index = arraytemas.indexOf(event.target.dataset.tema * 1);
    if (index > -1) {
      arraytemas.splice(index, 1);
    } else {
      arraytemas.push(event.target.dataset.tema * 1);
    }
    setSelectTema([...arraytemas]);
  }

  // Carga inicial
  useEffect(() => {
    getTemas();

    return (arraytemas = []);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return tableState === -1 ? (
    <ul>
      {temas.map((tema) => (
        <Fragment key={tema.id}>
          <div key={tema.id} className="tablasSubTemas">
            <img
              onClick={tema.icono2 && verDetallesTema}
              src={
                arraytemas.indexOf(tema.id * 1) !== -1 && tema.icono2
                  ? rutaImagenes + tema.icono2
                  : rutaImagenes + tema.icono
              }
              title="Expandir Tema"
              alt="Expandir Tema"
              width="15"
              height="15"
              className="noborder"
              data-tema={tema.id}
            />
            <li className="tablasSubTemasli" data-tema={tema.id}>
              <p
                className="subtema"
                data-tema={tema.id}
                onClick={verDetallesTema}
              >
                {tema.texto}
              </p>
            </li>
            {tema.imagen && (
              <img
                // onClick={verDetallesTema}
                src={rutaImagenes + tema.imagen}
                title="Imagen Subtema"
                alt="Imagen Subtema"
                width="10"
                height="10"
                className="imagenSubtema"
                data-grupo={tema.id}
              />
            )}
          </div>
          {arraytemas.indexOf(tema.id * 1) !== -1 && tema.icono2 && (
            <>
              <Contenidoayuda user={user} idtema={tema.id} />
            </>
          )}
        </Fragment>
      ))}
    </ul>
  ) : (
    // </div>
    <>
      <br></br>
      <br></br>
      <br></br>
      {tableState === 0 ? (
        <Cargandodatos texto="1-No exiten subtemas de ayuda" />
      ) : (
        <Cargandodatos texto="Recuperando Datos de Ayuda.." />
      )}
    </>
  );
}

export default Subtemasayuda;
