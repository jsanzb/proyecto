import { useEffect } from 'react';
import Temasayuda from './Temasayuda';
import './aprender.css';

function Aprender({ user, setSelMenu }) {
  document.title = 'giC-Aprender';

  useEffect(() => {
    setSelMenu('Aprender');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="tablasAprenderTodas">
      <>
        <Temasayuda user={user} />
      </>
    </div>
  );
}

export default Aprender;
