import { useState, useEffect, Fragment } from 'react';
import { getApiDataAxios } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';

const rutaImagenes = `${global.config.backend}/static/images/`;
let arraytemas = [];

function Contenidoayuda({ user, idtema }) {
  const [temas, setTemas] = useState([]);
  const [tableState, setTableState] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [selectTema, setSelectTema] = useState([]);

  // Obtenemos los datos de grupos del usuario del Back
  async function getTemas() {
    const url = `${global.config.backend}/ayuda/contenido/${idtema}`;

    const metodo = 'get';
    const datos = {};
    const cabeceras = {};
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setTemas(data.data.data);
      data.data.data.length === 0 ? setTableState(0) : setTableState(-1);
    } else {
      setTemas(data.statusText);
    }
  }

  // Expansion de temas
  function verDetallesTema(event) {
    let index = arraytemas.indexOf(event.target.dataset.tema * 1);
    if (index > -1) {
      arraytemas.splice(index, 1);
    } else {
      arraytemas.push(event.target.dataset.tema * 1);
    }
    setSelectTema([...arraytemas]);
  }

  // Carga inicial y temporizacion 1 minuto
  useEffect(() => {
    getTemas();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return tableState === -1 ? (
    <ul>
      {temas.map((tema) => (
        <Fragment key={tema.id}>
          <div key={tema.id} className="tablasContenido">
            <img
              onClick={verDetallesTema}
              src={rutaImagenes + tema.icono}
              title="Expandir Tema"
              alt="Expandir Tema"
              width="20"
              height="20"
              className="noborder"
              data-tema={tema.id}
            />
            <li className="tablasContenidoli" data-tema={tema.id}>
              <p data-tema={tema.id} onClick={verDetallesTema}>
                {tema.texto}
              </p>
            </li>
          </div>
          {tema.imagen && (
            <img
              // onClick={verDetallesTema}
              src={rutaImagenes + tema.imagen}
              title={'Imagen Subtema ' + tema.id_subtema}
              alt={'Imagen Subtema ' + tema.id_subtema}
              width={tema.ancho_imagen}
              height={tema.alto_imagen}
              className="imagenContenido"
              data-grupo={tema.id}
            />
          )}
        </Fragment>
      ))}
    </ul>
  ) : (
    <>
      <br></br>
      <br></br>
      <br></br>
      {tableState === 0 ? (
        <Cargandodatos texto="1-No exite Contenido de ayuda" />
      ) : (
        <Cargandodatos texto="Recuperando Datos de Ayuda.." />
      )}
    </>
  );
}

export default Contenidoayuda;
