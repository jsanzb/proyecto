import { useState, useEffect, Fragment } from 'react';
import { getApiDataAxios } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';
import Subtemasayuda from './Subtemasayuda';
let arraytemas = [];
const rutaImagenes = `${global.config.backend}/static/images/`;

function Temasayuda({ user }) {
  const [temas, setTemas] = useState([]);
  const [tableState, setTableState] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [selectTema, setSelectTema] = useState([]);

  // Obtenemos los datos de grupos del usuario del Back
  async function getTemas() {
    const url = `${global.config.backend}/ayuda/temas`;

    const metodo = 'get';
    const datos = {};
    const cabeceras = {};
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setTemas(data.data.data);
      data.data.data.length === 0 ? setTableState(0) : setTableState(-1);
    } else {
      setTemas(data.statusText);
    }
  }

  // Expansion de temas
  function verDetallesTema(event) {
    let index = arraytemas.indexOf(event.target.dataset.tema * 1);
    if (index > -1) {
      arraytemas.splice(index, 1);
    } else {
      arraytemas.push(event.target.dataset.tema * 1);
    }
    setSelectTema([...arraytemas]);
  }

  // Carga inicial y temporizacion 1 minuto
  useEffect(() => {
    getTemas();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return tableState === -1 ? (
    <div className="tablasAprender">
      <p className="tituloTabla">Temas de ayuda</p>
      <ul>
        {temas.map((tema) => (
          <Fragment key={tema.id}>
            <div key={tema.id} className="tablasTemas">
              <img
                onClick={verDetallesTema}
                src={
                  arraytemas.indexOf(tema.id * 1) === -1
                    ? rutaImagenes + tema.icono
                    : rutaImagenes + tema.icono2
                }
                title="Expandir Tema"
                alt="Expandir Tema"
                width="20"
                height="20"
                className="noborder"
                data-tema={tema.id}
              />
              <li className="tablasTemasli" data-tema={tema.id}>
                <h3 data-tema={tema.id} onClick={verDetallesTema}>
                  {tema.texto}
                </h3>
              </li>
            </div>
            {arraytemas.indexOf(tema.id * 1) !== -1 && (
              <>
                <hr></hr>
                <Subtemasayuda user={user} idtema={tema.id} />
              </>
            )}
          </Fragment>
        ))}
      </ul>
    </div>
  ) : (
    <>
      <br></br>
      <br></br>
      <br></br>
      {tableState === 0 ? (
        <Cargandodatos texto="1-No exiten temas de ayuda" />
      ) : (
        <Cargandodatos texto="Recuperando Datos de Ayuda.." />
      )}
    </>
  );
}

export default Temasayuda;
