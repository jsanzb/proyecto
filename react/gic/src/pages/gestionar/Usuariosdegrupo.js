import { useState, useEffect } from 'react';
import { getApiDataAxios } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';
let varusuario;
let vargrupo;

function Usuariosdegrupo({
  user,
  idGroup,
  roleUser,
  tipoGrupo,
  setAlert,
  confirm,
  setConfirm,
  input,
  setInput,
  valorinput,
  setValorinput,
}) {
  const [usuarios, setusuarios] = useState([]);
  const [tableState, setTableState] = useState(false);
  const [newUserEmail, setNewUserEmail] = useState('');
  const [responseActivateUser, setResponseActivateUser] = useState(false);
  const rutaImagenes = `${global.config.backend}/static/images/`;

  // Obtenemos los datos de operaciones del usuario del Back
  async function getUsuariosGroups() {
    const url = `${global.config.backend}/group/members/${idGroup}`;
    const metodo = 'get';
    const datos = {};
    const cabeceras = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setusuarios(data.data.data);
      setTableState(true);
    } else {
      setTableState(data.statusText);
    }
  }

  // Funcion Llamada a Backend para aceptar invitacion
  async function aceptarInvitacion(event) {
    const url = `${global.config.backend}/group/validate/${idGroup}`;
    const metodo = 'put';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setResponseActivateUser(true);
      setAlert('0-Invitacion al Grupo aceptada');
      getUsuariosGroups();
    } else {
      setAlert('1-Error aceptando invitacion al grupo: ' + data.data.message);
    }
  }

  // PreEdit user
  function preeditarUser(event) {
    if (!input) {
      setInput({
        aviso: 1,
        mensaje: `Nuevo Alias del Usuario (Maximo 15 Carácteres)?`,
        proceso: 'editarUser',
        longitud: 15,
        valorpre: '',
      });
    }
  }

  // Funcion Llamada a Backend para editar alias usuario
  async function editarUser() {
    const url = `${global.config.backend}/group/user/${idGroup}`;
    const metodo = 'put';
    const datos = { newAlias: valorinput };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setResponseActivateUser(true);
      setAlert('0-Alias del Usuario modificado');
      getUsuariosGroups();
    } else {
      setAlert('1-Error editando usuario: ' + data.data.message);
    }
    // }
    setInput(false);
    setValorinput(false);
  }

  // Funcion Llamada a Backend para invitar usuario
  async function invitarUsuario() {
    if (newUserEmail) {
      const url = `${global.config.backend}/group/${idGroup}`;
      const metodo = 'post';
      const datos = { email: newUserEmail };
      const cabecera = {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      };
      const data = await getApiDataAxios(url, metodo, datos, cabecera);
      if (data.status === 200) {
        setResponseActivateUser(true);

        setAlert(
          `0-Enviada invitacion a "${newUserEmail}". Podra aceptarla cuando inicie sesion.`
        );
        getUsuariosGroups();
      } else {
        setAlert('1-Error invitando al usuario: ' + data.data.message);
      }
    }
  }

  // Preechar del grupo
  function preecharGrupo(event) {
    varusuario = event.target.dataset.usuario;
    vargrupo = event.target.dataset.grupo;
    if (!confirm) {
      setConfirm({
        aviso: 1,
        mensaje: 'Vas a echar al usuario del grupo. Estas seguro?',
        proceso: 'Usuariosdegrupo',
      });
    }
  }
  // Funcion Llamada a Backend echar del grupo
  async function echarGrupo() {
    const url = `${global.config.backend}/group/leave/${vargrupo}`;
    const metodo = 'put';
    const datos = { idUser: varusuario };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };

    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      getUsuariosGroups();
      setAlert(`0-Has echado al usuario del grupo.`);
    } else {
      setAlert('1-Error echando al usuario del grupo: ' + data.data.message);
    }
    setConfirm(false);
  }

  useEffect(() => {
    getUsuariosGroups();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    confirm === 'Usuariosdegrupo' && echarGrupo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [confirm]);

  useEffect(() => {
    input === 'editarUser' && editarUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [input]);

  return user.isUserLogged && tableState ? (
    <>
      <div className="tablasUsuarios">
        <table className="tablaUsuarios">
          <thead></thead>
          <tbody>
            <tr className="tablaHeader">
              <td align="center">Alias Usuario</td>
              <td align="center">Role</td>
              <td align="center">Estado</td>
              <td align="center">Acciones</td>
            </tr>

            {usuarios.map((usuario) => (
              <tr key={usuario.id}>
                <td align="center" title={usuario.alias}>
                  {usuario.alias}
                </td>
                {usuario.role === 'admin' ? (
                  <td align="center">Admin</td>
                ) : (
                  <td align="center">Usuario</td>
                )}

                {usuario.activado === -1 ? (
                  <td align="center" title="Activo">
                    Activo
                  </td>
                ) : (
                  <td align="center" title="Por Activar">
                    Por Activar
                  </td>
                )}
                <td align="center">
                  {usuario.id * 1 === user.idUser * 1 &&
                    usuario.activado === -1 && (
                      <img
                        onClick={preeditarUser}
                        src={rutaImagenes + 'editar.png'}
                        title="Editar Usuario"
                        alt="Editar Usuario"
                        width="20"
                        height="20"
                        className="noborder"
                      />
                    )}

                  {usuario.id * 1 === user.idUser * 1 &&
                    usuario.activado === 0 && (
                      <img
                        onClick={aceptarInvitacion}
                        src={rutaImagenes + 'aceptar.png'}
                        title="Aceptar Invitacion"
                        alt="Aceptar Invitacion"
                        width="20"
                        height="20"
                        className="noborder"
                      />
                    )}

                  {roleUser === 'admin' &&
                    usuario.id * 1 !== user.idUser * 1 && (
                      <img
                        onClick={preecharGrupo}
                        src={rutaImagenes + 'echar.png'}
                        title="Echar del Grupo"
                        data-usuario={usuario.id}
                        data-grupo={idGroup}
                        alt="Echar del Grupo"
                        width="20"
                        height="20"
                        className="noborder"
                      />
                    )}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {roleUser === 'admin' && !tipoGrupo && (
        <div className="tablasUsuarios">
          <div className="tablaNewUser">
            <p>Invitar a Usuario </p>
            <form>
              <label className={idGroup} htmlFor={idGroup}>
                <input
                  id={idGroup}
                  onChange={(event) => {
                    event.preventDefault();
                    setNewUserEmail(event.target.value);
                  }}
                  className="inputNewGroup"
                  placeholder="Introduce email"
                  autoComplete="off"
                  type="email"
                />
              </label>
            </form>
            <img
              onClick={invitarUsuario}
              src={rutaImagenes + 'agregar.png'}
              title="Añadir Usuario"
              alt="Añadir Usuario"
              width="20"
              height="20"
              className="noborder"
            />
          </div>
        </div>
      )}

      {responseActivateUser && (
        <div className="mensajeError">{responseActivateUser}</div>
      )}
    </>
  ) : (
    <Cargandodatos texto="Recuperando Datos de Usuarios.." />
  );
}

export default Usuariosdegrupo;
