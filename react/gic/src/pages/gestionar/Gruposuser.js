import { useState, useEffect } from 'react';
import { getApiDataAxios, parseSybaseDate } from '../../helpers';
import Usuariosdegrupo from './Usuariosdegrupo';
import './gestionar.css';
import Cargandodatos from '../../components/cargando/Cargando';
let arrayGroups = [];
let groupid;
let groupname;

function Gruposuser({
  user,
  setAlert,
  confirm,
  setConfirm,
  input,
  setInput,
  valorinput,
  setValorinput,
}) {
  const [groups, setGroups] = useState([]);
  const [tableState, setTableState] = useState(false);
  // const [responseGroupDelete, setResponseGroupDelete] = useState(false);
  const [newGroupName, setNewGroupName] = useState('');
  const [newSimulationName, setNewSimulationName] = useState('');
  // eslint-disable-next-line no-unused-vars
  const [selectGroup, setSelectGroup] = useState([]);

  const rutaImagenes = `${global.config.backend}/static/images/`;
  let nombreSimulacion;

  // Obtenemos los datos de grupos del usuario del Back
  async function getGroupsUser() {
    const url = `${global.config.backend}/group/user/${user.idUser}`;
    const metodo = 'get';
    const datos = { userId: user.idUser };
    const cabeceras = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setGroups(data.data.data);
      data.data.data.length === 0 ? setTableState(false) : setTableState(true);
    } else {
      setTableState(data.statusText);
    }
  }

  // Presalida grupo
  function preabandonarGroup(event) {
    groupname = event.target.dataset.nombre;
    groupid = event.target.dataset.grupo;
    if (!confirm) {
      setConfirm({
        aviso: 1,
        mensaje: `Vas a salir del grupo "${groupname}" , no volveras a tener acceso hasta que te vuelvan a invitar.
          Estas seguro?`,
        proceso: 'abandonarGroup',
      });
    }
  }

  // Funcion Llamada a Backend salir del grupo
  async function abandonarGroup() {
    const url = `${global.config.backend}/group/leave/` + groupid;
    const metodo = 'put';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Has abandonado el grupo "${groupname}".`);
      // setResponseGroupDelete(true);
      getGroupsUser();
    } else {
      // setResponseGroupDelete('Error saliendo del grupo: ' + data.data.message);
      setAlert('1-Error saliendo del grupo: ' + data.data.message);
    }
    setConfirm(false);
  }

  // Preborrado grupo
  function predeleteGroup(event) {
    groupname = event.target.dataset.nombre;
    groupid = event.target.dataset.grupo;
    if (!confirm) {
      setConfirm({
        aviso: 1,
        mensaje: `Vas a borrar todos los datos del grupo "${groupname}" y sus operaciones.
        Estas seguro?`,
        proceso: 'deleteGroup',
      });
    }
  }
  // Funcion Llamada a Backend borrar grupo
  async function deleteGroup() {
    const url = `${global.config.backend}/group/` + groupid;
    const metodo = 'delete';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };

    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      // setResponseGroupDelete(true);
      getGroupsUser();
      setAlert(`0-Grupo "${groupname}" Borrado`);
    } else {
      // setResponseGroupDelete('Error borrando el grupo: ' + data.data.message);
      setAlert('1-Error borrando el grupo: ' + data.data.message);
    }
    setConfirm(false);
  }

  // PreEdit grupo
  function preeditGroup(event) {
    groupname = event.target.dataset.nombre;
    groupid = event.target.dataset.grupo;
    if (!input) {
      setInput({
        aviso: 1,
        mensaje: `Nuevo nombre para el grupo "${groupname}" (Maximo 30 Carácteres)?`,
        proceso: 'editGroup',
        longitud: 30,
        valorpre: groupname,
      });
    }
  }
  // Funcion Llamada a Backend editar grupo
  async function editGroup() {
    const url = `${global.config.backend}/group/` + groupid;
    const metodo = 'put';
    const datos = { name: valorinput };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      getGroupsUser();
      setAlert(`0-Grupo "${valorinput}" Modificado`);
    } else {
      setAlert(`1-Error modificando el grupo: ' + data.data.message`);
    }
    setInput(false);
    setValorinput(false);
  }

  // Funcion Llamada a Backend añadir grupo
  async function nuevoGroup(event) {
    if (newGroupName && newGroupName.length < 30) {
      const url = `${global.config.backend}/group`;
      const metodo = 'post';
      const datos = { name: newGroupName };
      const cabecera = {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      };

      const data = await getApiDataAxios(url, metodo, datos, cabecera);
      if (data.status === 200) {
        // setResponseGroupDelete(true);
        setNewGroupName('');
        getGroupsUser();
        setAlert(`0-Nuevo Grupo "${newGroupName}" Creado`);
      } else {
        // setResponseGroupDelete('Error Creando el grupo: ' + data.data.message);
        setAlert('1-Error Creando el grupo: ' + data.data.message);
      }
    }
  }

  // Funcion Llamada a Backend lanzar Simulacion
  async function getSimulationUser() {
    if (!newSimulationName) {
      nombreSimulacion = 'Simulacion';
    } else {
      nombreSimulacion = newSimulationName;
    }

    const url = `${global.config.backend}/operations/simulation/${user.idUser}`;
    const metodo = 'post';
    const datos = { userId: user.idUser, nombreSimulacion: nombreSimulacion };
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);

    if (data.status === 200) {
      getGroupsUser();
      setAlert(
        `0-Grupo "${nombreSimulacion}" creado con operaciones simuladas`
      );
    } else {
      setAlert('1-' + data.data.message);
    }
  }

  // Expansion de grupos
  function verDetallesGrupo(event) {
    let index = arrayGroups.indexOf(event.target.dataset.grupo * 1);
    if (index > -1) {
      arrayGroups.splice(index, 1);
    } else {
      arrayGroups.push(event.target.dataset.grupo * 1);
    }

    setSelectGroup([...arrayGroups]);
  }

  useEffect(() => {
    getGroupsUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    confirm === 'deleteGroup' && deleteGroup();
    confirm === 'abandonarGroup' && abandonarGroup();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [confirm]);

  useEffect(() => {
    input === 'editGroup' && editGroup();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [input]);

  return user.isUserLogged && tableState ? (
    <>
      <div className="tablaGestionar">
        <p className="tituloTabla">Grupos de Inversion donde participas</p>
        {groups.map((group) => (
          <div key={group.id} className="contenedortablasGrupos">
            <div className="tablasGruposOper">
              <table className="tablaGruposOper">
                <thead></thead>
                <tbody>
                  <tr className="tablaHeader">
                    <td align="center">Nombre</td>
                    <td align="center">Fecha Creacion</td>
                    <td align="center">Role</td>
                    <td align="center">Tipo</td>
                    <td align="center">Acciones</td>
                  </tr>
                  <tr>
                    <td align="center">
                      <strong>{group.name}</strong>
                    </td>
                    <td align="center">{parseSybaseDate(group.createdAt)}</td>

                    {group.role === 'admin' ? (
                      <td align="center">Admin</td>
                    ) : (
                      <td align="center">Usuario</td>
                    )}

                    {group.personal ? (
                      <td align="center">Personal</td>
                    ) : (
                      <td align="center">Compart.</td>
                    )}

                    <td align="center">
                      {group.role === 'admin' && (
                        <img
                          align="center"
                          onClick={preeditGroup}
                          src={rutaImagenes + 'editar.png'}
                          title="Editar Grupo"
                          data-grupo={group.id}
                          data-nombre={group.name}
                          alt="Editar Grupo"
                          width="20"
                          height="20"
                          className="noborder"
                        />
                      )}

                      <span> </span>
                      {group.role === 'admin' && !group.personal && (
                        <img
                          align="center"
                          onClick={predeleteGroup}
                          src={rutaImagenes + 'borrar.png'}
                          title="Borrar Grupo"
                          data-grupo={group.id}
                          data-nombre={group.name}
                          alt="Borrar Grupo"
                          width="20"
                          height="20"
                          className="noborder"
                        />
                      )}
                      {group.role !== 'admin' && (
                        <img
                          align="center"
                          onClick={preabandonarGroup}
                          src={rutaImagenes + 'salir.png'}
                          title="Salir del Grupo"
                          data-grupo={group.id}
                          data-nombre={group.name}
                          alt="Salir del Grupo"
                          width="20"
                          height="20"
                          className="noborder"
                        />
                      )}

                      {arrayGroups.indexOf(group.id * 1) === -1 ? (
                        <img
                          align="right"
                          onClick={verDetallesGrupo}
                          src={rutaImagenes + 'vermas.png'}
                          title="Ver Detalles"
                          data-grupo={group.id}
                          data-nombre={group.name}
                          alt="Ver Detalles"
                          width="20"
                          height="20"
                          className="noborder"
                        />
                      ) : (
                        <img
                          align="right"
                          onClick={verDetallesGrupo}
                          src={rutaImagenes + 'atras.png'}
                          title="Cerrar Detalles"
                          data-grupo={group.id}
                          data-nombre={group.name}
                          alt="Cerrar Detalles"
                          width="20"
                          height="20"
                          className="noborder"
                        />
                      )}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            {arrayGroups.indexOf(group.id * 1) !== -1 && (
              <Usuariosdegrupo
                user={user}
                idGroup={group.id}
                roleUser={group.role}
                tipoGrupo={group.personal}
                alert={alert}
                setAlert={setAlert}
                confirm={confirm}
                setConfirm={setConfirm}
                input={input}
                setInput={setInput}
                valorinput={valorinput}
                setValorinput={setValorinput}
              />
            )}
            {/* {!responseGroupDelete && (
              <div className="mensajeErrorLogin">{responseGroupDelete} </div>
            )} */}
          </div>
        ))}
      </div>

      <div className="contenedorGrupos ">
        <p className="tituloTabla">Crear Nuevos Grupos</p>
        <div className="tablasGrupos ">
          <div className="tablaNewGrupo">
            <p>Grupo Normal</p>
            <form>
              <label className="nuevoGrupoLabel" htmlFor="inputNewGroup">
                <input
                  onChange={(event) => {
                    setNewGroupName(event.target.value);
                  }}
                  className="inputNewGroup"
                  value={newGroupName}
                  placeholder="Nombre del grupo"
                  autoComplete="on"
                  type="text"
                />
              </label>
            </form>
            <img
              onClick={nuevoGroup}
              src={rutaImagenes + 'agregar.png'}
              title="Crear un nuevo grupo"
              alt="Crear un nuevo grupo"
              width="20"
              height="20"
              className="noborder"
            />
          </div>
        </div>
        <div className="tablasGrupos">
          <div className="tablaNewGrupo">
            <p>Grupo Simulacion</p>
            <form>
              <label
                className="nuevaSimulacionLabel"
                htmlFor="inputNewSimulation"
              >
                <input
                  onChange={(event) => {
                    setNewSimulationName(event.target.value);
                  }}
                  className="inputNewSimulation"
                  value={newSimulationName}
                  placeholder="Nombre de la Simulacion"
                  autoComplete="on"
                  type="text"
                />
              </label>
            </form>
            <img
              onClick={getSimulationUser}
              src={rutaImagenes + 'agregar.png'}
              title="Crear una Nueva Simulacion"
              alt="Crear una Nueva Simulacion"
              width="20"
              height="20"
              className="noborder"
            />
          </div>
        </div>
      </div>
    </>
  ) : (
    <>
      <br></br>
      <br></br>
      <br></br>
      <Cargandodatos texto="Recuperando Datos de Grupos.." />
    </>
  );
}

export default Gruposuser;
