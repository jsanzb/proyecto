import { useEffect } from 'react';
import { useHistory } from 'react-router';
import Gruposuser from './Gruposuser';
import './gestionar.css';

function Consultar({
  user,
  alert,
  setAlert,
  setSelMenu,
  confirm,
  setConfirm,
  input,
  setInput,
  valorinput,
  setValorinput,
}) {
  document.title = 'giC-Gestionar';
  let history = useHistory();

  useEffect(() => {
    setSelMenu('Gestionar');
    !user.isUserLogged &&
      setAlert(
        '1-No has iniciado Sesión en la plataforma. Solo podrás ver los datos de cotizaciones del Mercado. Inicia Sesión o créate una cuenta.'
      );
    !user.isUserLogged && history.push('/login');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="tablasGestionar">
      {user.isUserLogged && (
        <Gruposuser
          user={user}
          setAlert={setAlert}
          confirm={confirm}
          setConfirm={setConfirm}
          input={input}
          setInput={setInput}
          valorinput={valorinput}
          setValorinput={setValorinput}
        />
      )}
    </div>
  );
}
export default Consultar;
