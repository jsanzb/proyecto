import React from 'react';
import ReactDOM from 'react-dom';
import './config';

import Gic from './Gic';

ReactDOM.render(
  <React.StrictMode>
    <Gic />
  </React.StrictMode>,
  document.getElementById('root')
);
