const { format, parse } = require('date-fns');

const axios = require('axios');

function sendHtmllog(data) {
  const htmlLog = document.querySelector('.htmlLog');
  htmlLog.innerHTML = `${data}`;
}

// Saca el porcentaje de variacion entre dos periodos
function varMoneda(var1, var2) {
  let varPor = (((var2 - var1) / var1) * 100).toFixed(2);
  if (!isFinite(varPor)) {
    varPor = '0.00';
  }
  return varPor;
}

// Define la clase en funcion del valor para asignar colores a las variaciones
function classVar(var1, var2) {
  const valorVarMoneda = varMoneda(var1, var2);

  if (valorVarMoneda > 0 && valorVarMoneda < 1) {
    return 'vargreen';
  }
  if (valorVarMoneda >= 1 && valorVarMoneda < 3) {
    return 'vargreenmedio';
  }
  if (valorVarMoneda >= 3) {
    return 'vargreenstrong';
  }

  if (valorVarMoneda < 0 && valorVarMoneda > -1) {
    return 'varred';
  }

  if (valorVarMoneda <= -1 && valorVarMoneda > -3) {
    return 'varredmedio';
  }
  if (valorVarMoneda <= -3) {
    return 'varredstrong';
  }
  if (valorVarMoneda * 1 === 0) {
    return 'vargrey';
  }
}

// Define la clase en funcion del valor para asignar colores a las variaciones
function classVarAbs(valor) {
  const valorVarMoneda = valor;

  if (valorVarMoneda > 0 && valorVarMoneda < 1) {
    return 'vargreen';
  }
  if (valorVarMoneda >= 1 && valorVarMoneda < 3) {
    return 'vargreenmedio';
  }
  if (valorVarMoneda >= 3) {
    return 'vargreenstrong';
  }

  if (valorVarMoneda < 0 && valorVarMoneda > -1) {
    return 'varred';
  }

  if (valorVarMoneda <= -1 && valorVarMoneda > -3) {
    return 'varredmedio';
  }
  if (valorVarMoneda <= -3) {
    return 'varredstrong';
  }
  if (valorVarMoneda * 1 === 0) {
    return 'vargrey';
  }
}

async function getApiData2Table(api, tittle, clase) {
  const response = await fetch(`http://192.168.0.5:4000${api}`);
  const data = await response.json();
  let inhtml = `
      <p>${tittle}</p>
      <table class="${clase}">
      <thead></thead>
      <tbody>
      <tr>
      `;
  for (const key in data[0]) {
    inhtml =
      inhtml +
      `  <td>${key}</td>
      `;
  }
  inhtml =
    inhtml +
    `</tr>
      `;
  for (let index = 0; index < data.length; index++) {
    inhtml =
      inhtml +
      `
      <tr>`;
    const element = data[index];
    for (const key in element) {
      inhtml =
        inhtml +
        `
        <td>${element[key]}</td>`;
    }
    inhtml =
      inhtml +
      `
      </tr>`;
  }

  return inhtml;
}

// Llamadas al backend con fetch
async function getApiData(api) {
  const response = await fetch(api);
  const data = await response.json();
  return data;
}

// Llamadas al backend con axios
async function getApiDataAxios(api, metodo, datos, cabeceras) {
  try {
    const response = await axios({
      method: metodo,
      url: api,
      data: datos,
      headers: cabeceras,
    });
    return response;
  } catch (error) {
    return error.response;
  }
}

// Parseador de fechas de sybase
function parseSybaseDate(date) {
  return new Date(date).toLocaleDateString('es-ES', {
    day: '2-digit',
    year: '2-digit',
    month: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    timeZone: 'UTC',
  });
}

// Parseador de fechas
function parseDate() {
  return new Date(new Date()).toLocaleDateString('es-ES', {
    day: '2-digit',
    month: '2-digit',
    year: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  });
}

/**
 * #####################
 * ## Formatear fecha ##
 * #####################
 *
 * Formatear un objeto fecha al formato DATETIME de SQL.
 *
 */

function formatDate(date) {
  const cdate = parse(date, 'dd/MM/yy HH:mm:ss', new Date());
  return format(cdate, 'yy/MM/dd HH:mm:ss');
}

async function getuserTokenInfo() {
  try {
    const idUser = window.localStorage.getItem('iduser');
    const url = `${global.config.backend}/user/${idUser}`;
    const metodo = 'get';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    // console.log(data.data.data.role);
    return data.data.data.role;
  } catch (error) {}
}

function redondearValoraciones(dato) {
  let resto;

  dato.substring(2, 3) >= 5 ? (resto = '.5') : (resto = '');

  return parseInt(dato) + resto;
}

export {
  varMoneda,
  classVar,
  classVarAbs,
  getApiData2Table,
  getApiData,
  getApiDataAxios,
  sendHtmllog,
  parseSybaseDate,
  getuserTokenInfo,
  parseDate,
  redondearValoraciones,
  formatDate,
};
