import { useState, useEffect } from 'react';
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './Gic.css';
import Navigator from './components/navigator/Navigator';
import Consultar from './pages/consultar/Consultar';
import Operar from './pages/operar/Operar';
import Gestionar from './pages/gestionar/Gestionar';
import Aprender from './pages/aprender/Aprender';
import Login from './pages/login/Login';
import Logout from './pages/logout/Logout';
import Home from './pages/home/Home';
import Profile from './pages/profile/Profile';
import Administrar from './pages/administrar/Administrar';
import Footer from './components/footer/Footer';
import Activate from './pages/activate/Activate';
import Nuevousuario from './pages/nuevousuario/Nuevousuario';
import Resetpassword from './pages/resetpassword/Resetpassword';
import Confirmresetpassword from './pages/resetpassword/Confirmresetpassword';
import { getApiDataAxios } from './helpers';
import Modalalert from './components/modal/Modalalert';
import Modalconfirm from './components/modal/Modalconfirm';
import Modalinput from './components/modal/Modalinput';

function App() {
  const [alert, setAlert] = useState();
  const [confirm, setConfirm] = useState(false);
  const [input, setInput] = useState(false);
  const [valorinput, setValorinput] = useState(false);
  const [selMenu, setSelMenu] = useState('home');
  // Comprobamos si tenemos ya token y propagamos el estado y la funcion en toda la aplicacion.
  const [user, setUser] = useState(
    window.localStorage.getItem('accesstoken')
      ? {
          idUser: window.localStorage.getItem('iduser'),
          emailUser: window.localStorage.getItem('emailUser'),
          avatarUser: window.localStorage.getItem('avatarUser'),
          roleUser:
            window.localStorage.getItem('vista') === '1' ? 'admin' : 'user',
          isUserLogged: true,
        }
      : {
          idUser: 0,
          emailUser: '',
          roleUser: '',
          avatarUser: '',
          isUserLogged: false,
        }
  );

  // Fijamos modo mano derecha si aun se ha seleccionado ninguna vez
  !window.localStorage.getItem('mano') &&
    window.localStorage.setItem('mano', '1');

  // Ocultamos menu flotante al recargar
  window.localStorage.setItem('menu', '0');

  // Validamos usuario y token
  async function ckeckUser(event) {
    const url = `${global.config.backend}/user/${user.idUser}`;
    const metodo = 'get';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);

    if (data.status === 200) {
    } else {
      window.localStorage.removeItem('accesstoken');
      window.localStorage.removeItem('iduser');
      window.localStorage.removeItem('emailUser');
      window.localStorage.removeItem('avatarUser');
      setAlert('1-Sesion Erronea o Caducada, vuelve a hacer Login.');
      setUser({
        idUser: 0,
        emailUser: '',
        roleUser: '',
        avatarUser: '',
        isUserLogged: false,
      });
    }
  }

  useEffect(() => {
    user.isUserLogged && ckeckUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    // <ErrorBoundary>
    <Router basename={'/gic'}>
      <div className="App">
        <Navigator user={user} selMenu={selMenu} setAlert={setAlert} />
        {alert && <Modalalert alert={alert} setAlert={setAlert} />}
        {confirm && <Modalconfirm confirm={confirm} setConfirm={setConfirm} />}
        {input && (
          <Modalinput
            input={input}
            setInput={setInput}
            valorinput={valorinput}
            setValorinput={setValorinput}
            alert={alert}
            setAlert={setAlert}
          />
        )}
        <Switch>
          <Route path="/consultar">
            <Consultar
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
            />
            <br></br>
          </Route>

          <Route path="/operar">
            <Operar
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
              confirm={confirm}
              setConfirm={setConfirm}
            />
            <br></br>
          </Route>

          <Route path="/gestionar">
            <Gestionar
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
              confirm={confirm}
              setConfirm={setConfirm}
              input={input}
              setInput={setInput}
              valorinput={valorinput}
              setValorinput={setValorinput}
            />
            <br></br>
          </Route>

          <Route path="/aprender">
            <Aprender
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
            />
            <br></br>
          </Route>

          <Route path="/administrar">
            <Administrar
              setUser={setUser}
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
            />
            <br></br>
            <br></br>
          </Route>

          <Route path="/login">
            <Login
              setUser={setUser}
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
            />
          </Route>

          <Route path="/logout">
            <Logout
              setUser={setUser}
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
            />
          </Route>

          <Route path="/profile">
            <Profile
              setUser={setUser}
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
              input={input}
              setInput={setInput}
              valorinput={valorinput}
              setValorinput={setValorinput}
            />
          </Route>

          <Route path="/nuevousuario">
            <Nuevousuario
              setUser={setUser}
              user={user}
              alert={alert}
              setAlert={setAlert}
              setSelMenu={setSelMenu}
            />
          </Route>

          <Route path="/activate/:activationCode">
            <Activate alert={alert} setAlert={setAlert} />
          </Route>

          <Route path="/resetpassword">
            <Resetpassword alert={alert} setAlert={setAlert} />
          </Route>

          <Route path="/confirmresetpassword/:recoverCode">
            <Confirmresetpassword alert={alert} setAlert={setAlert} />
          </Route>

          <Route path="/">
            <Home user={user} setSelMenu={setSelMenu} />
          </Route>
        </Switch>
        <Footer />
      </div>
      <div className="vacioScroll"></div>
    </Router>

    // </ErrorBoundary>
  );
}

export default App;

// class ErrorBoundary extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = { hasError: false };
//   }

//   static getDerivedStateFromError(error) {
//     // Actualiza el estado para que el siguiente renderizado muestre la interfaz de repuesto
//     return { hasError: true };
//   }

//   componentDidCatch(error, errorInfo) {
//     // También puedes ejecutar codigo  cuando hay un error

//     console.log('ERROR: ', error);
//   }

//   render() {
//     if (this.state.hasError) {
//       return <h1>Ha habido un error en la Aplicacion</h1>;
//     }

//     return this.props.children;
//   }
// }
