import { useState, useEffect } from 'react';
import { classVarAbs, getApiDataAxios } from '../../helpers';
import './topsmonedas.css';
import Cargandodatos from '../cargando/Cargando';
import Monedasdetails from '../../components/monedas/Monedasdetails';

let favoritas = [];

function TopsMonedas({
  user,
  periodo,
  orden,
  setAlert,
  changeFav,
  setChangeFav,
}) {
  const [monedas, setMonedas] = useState([]);
  const [tableState, settableState] = useState(false);
  const [favState, setfavState] = useState(false);
  const [monedaSelect, setMonedaSelect] = useState('');

  const rutaImagenes = `${global.config.backend}/static/images/`;
  let tiempo;

  switch (periodo + orden) {
    case '3600desc':
      tiempo = 'Mejores Hora';
      break;
    case '3600asc':
      tiempo = 'Peores Hora';
      break;
    case '86400desc':
      tiempo = 'Mejores Dia';
      break;
    case '86400asc':
      tiempo = 'Peores Dia';
      break;
    default:
      break;
  }

  // Obtenemos los datos de monedas Top del Back
  async function getTablaMonedas() {
    const url = `${global.config.backend}/stats/tops/${periodo}/${orden}`;
    const metodo = 'get';
    const datos = {};

    const data = await getApiDataAxios(url, metodo, datos);
    if (data.status === 200) {
      setMonedas(data.data.data);
      settableState('ok');
    } else {
      settableState(data.data.statusText);
    }
  }

  // Obtenemos los las monedas favoritas del usuario
  async function getFavMonedas() {
    const url = `${global.config.backend}/monedas/favoritas/${user.idUser}/fav`;
    const metodo = 'get';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const dataFav = await getApiDataAxios(url, metodo, datos, cabecera);
    if (dataFav.status === 200) {
      favoritas.length = 0;
      for (const moneda of dataFav.data.data) {
        favoritas.push(moneda.moneda);
      }
      setfavState('ok');
    } else {
      setfavState(dataFav.data.statusText);
    }
  }

  // OnClick para detalle monedas
  function onClickMoneda(event) {
    event.preventDefault();
    setMonedaSelect(event.target.dataset.moneda);
  }

  // Temporizamos las actualizaciones a 15 minutos
  useEffect(() => {
    getFavMonedas();
    getTablaMonedas();
    setChangeFav(false);

    const intervalID = setInterval(() => {
      getFavMonedas();
      getTablaMonedas();
      // favoritas = [];
    }, 900000);
    return () => {
      clearInterval(intervalID);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [changeFav]);

  // Generamos las tablas con los datos Top
  return !tableState && !!favState ? (
    <Cargandodatos texto="Recuperando Datos de Cotizaciones.." />
  ) : (
    <>
      <div className="tablaMercadoTops">
        <table className="preciosmercadoTops">
          <thead></thead>
          <tbody>
            <tr>
              <td className="noborder" align="center" colSpan="2">
                {tiempo}
              </td>
            </tr>
            {monedas.map((moneda) => (
              <tr key={moneda.moneda}>
                <td
                  onClick={onClickMoneda}
                  valign="middle"
                  title={moneda.nombre}
                  data-moneda={moneda.moneda}
                  className={
                    favoritas.indexOf(moneda.moneda) !== -1 && user.isUserLogged
                      ? 'favorita moneda'
                      : 'nofavorita moneda'
                  }
                >
                  <img
                    onClick={onClickMoneda}
                    valign="middle"
                    title={moneda.nombre}
                    data-moneda={moneda.moneda}
                    src={
                      rutaImagenes +
                      moneda.moneda.slice(0, moneda.moneda.indexOf('-')) +
                      '.png'
                    }
                    alt={moneda.moneda}
                    width="13"
                    height="13"
                  />
                  <span
                    title={moneda.nombre}
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    {moneda.moneda.slice(0, moneda.moneda.indexOf('-'))}
                  </span>
                </td>

                <td
                  onClick={onClickMoneda}
                  data-moneda={moneda.moneda}
                  align="center"
                  className={classVarAbs(moneda.var) + ' varTops'}
                >
                  {moneda.var}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {monedaSelect ? (
        <Monedasdetails
          monedaSelect={monedaSelect}
          setMonedaSelect={setMonedaSelect}
          user={user}
          setAlert={setAlert}
          changeFav={changeFav}
          setChangeFav={setChangeFav}
        />
      ) : null}
    </>
  );
}

export default TopsMonedas;
