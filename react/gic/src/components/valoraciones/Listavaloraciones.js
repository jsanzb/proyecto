import { useState, useEffect, Fragment } from 'react';
import { getApiDataAxios, redondearValoraciones } from '../../helpers';
import Cargandodatos from '../../components/cargando/Cargando';

import './valoraciones.css';

function Listavaloraciones({ user, moneda, setAlert }) {
  const [valoraciones, setValoraciones] = useState([]);
  const [valoracionesTotales, setValoracionesTotales] = useState([]);
  const rutaImagenes = `${global.config.backend}/static/images/`;
  const [valorEstrella, setValorEstrella] = useState('1');
  const [editValoracion, seteditValoracion] = useState('');
  const [valMoneda, setvalMoneda] = useState('0');

  // Obtenemos los datos de valoraciones detalladas del Back
  async function getValoraciones() {
    const url = `${global.config.backend}/ratingusers`;
    const metodo = 'get';
    const datos = {};
    const cabeceras = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setValoraciones(data.data.data);

      // Recojemos los valores si el usuario ya ha puesto comentario.
      const valUser = data.data.data.filter(
        (val) => val.moneda === moneda && val.id_user * 1 === user.idUser * 1
      );
      if (valUser[0]?.valoracion) {
        setValorEstrella(valUser[0].valoracion);
        seteditValoracion(valUser[0].comentario);
      }
    } else {
      setValoraciones(false);
    }

    // Comprobamos si hay valoraciones de esa moneda
    setvalMoneda(data.data.data.filter((val) => val.moneda === moneda).length);
  }

  // Obtenemos los datos de valoraciones totales del Back
  async function getValoracionestotales() {
    const url = `${global.config.backend}/ratings`;
    const metodo = 'get';
    const datos = {};
    const cabeceras = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabeceras);
    if (data.status === 200) {
      setValoracionesTotales(data.data.data);
    } else {
      setValoracionesTotales(false);
    }
  }

  // Funcion Llamada a Backend borrar valoracion
  async function borrarValoracion() {
    const url = `${global.config.backend}/rating/` + user.idUser;
    const metodo = 'delete';
    const datos = { moneda: moneda };

    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };

    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Valoracion Borrada`);
      getValoraciones();
    } else {
      setAlert('1-Error borrando la valoracion:' + data.data.message);
    }
  }

  async function addValoracion() {
    let varValoracion = editValoracion;
    if (!editValoracion) {
      seteditValoracion('Sin Comentario.');
      varValoracion = 'Sin Comentario.';
    }

    const url = `${global.config.backend}/rating/` + user.idUser;
    const metodo = 'post';
    const datos = {
      moneda: moneda,
      valoracion: valorEstrella,
      comentario: varValoracion,
    };

    // console.log(datos);
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };

    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setAlert(`0-Valoracion Modificada`);
      getValoraciones();
      getValoracionestotales();
    } else {
      setAlert('1-Error modificando la valoracion:' + data.data.message);
    }
    // }
  }

  function clickStar(event) {
    setValorEstrella(Math.floor(event.nativeEvent.offsetX / 20) + 1);
  }

  useEffect(() => {
    getValoraciones();
    getValoracionestotales();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return valoracionesTotales && valoraciones ? (
    <div className="valoracionessMonedascont">
      <div className="valoracionessMonedas">
        {/* Si no hay valoraciones, lo mostramos */}
        {valMoneda === 0 && user.isUserLogged && (
          <Cargandodatos texto="1-Aún no hay valoraciones de esta moneda. Se el primero en hacerlo." />
        )}
        {valMoneda === 0 && !user.isUserLogged && (
          <Cargandodatos texto="1-Aún no hay valoraciones de esta moneda. Para añadir una debes tener usuario y hacer Login" />
        )}

        {valoracionesTotales.map((valoracionTotal) => (
          <Fragment key={valoracionTotal.moneda}>
            {valoracionTotal.moneda === moneda && (
              <>
                {/* <hr></hr> */}

                <img
                  key={moneda}
                  valign="center"
                  src={
                    rutaImagenes +
                    'Star_' +
                    redondearValoraciones(valoracionTotal.valoracion_promedio) +
                    '.png'
                  }
                  alt="Valoracion Moneda"
                  width="60"
                  height="12"
                  className="estrellasValoracion"
                  title="Valoracion Moneda"
                />
              </>
            )}
          </Fragment>
        ))}
        <ul>
          {valoraciones.map((valoracion) => (
            <Fragment key={valoracion.id_user + valoracion.moneda}>
              {valoracion.moneda === moneda && (
                <div key={valoracion.id_user}>
                  <hr></hr>
                  <li
                    className={
                      valoracion.id_user * 1 === user.idUser * 1
                        ? 'liValoracionUser'
                        : 'liSinValoracionUser'
                    }
                  >
                    <img
                      valign="center"
                      src={
                        rutaImagenes + 'Star_' + valoracion.valoracion + '.png'
                      }
                      alt="Valoracion Moneda"
                      width="60"
                      height="12"
                      className="estrellasValoracion"
                      title="Valoracion Moneda"
                    />
                    {`(${valoracion.alias}) "${valoracion.comentario}"`}
                  </li>
                </div>
              )}
            </Fragment>
          ))}
        </ul>
      </div>
      {user.isUserLogged && (
        <div className="divInputValoracion">
          <div className="divInputValoracionLeft">
            <span>
              <p valign="bottom">Valoracion</p>
              <img
                onClick={clickStar}
                valign="center"
                align="center"
                src={rutaImagenes + 'Star_' + valorEstrella + '.png'}
                alt="Valoracion Moneda"
                id="estrellasValoracionClick"
                title="Valoracion Moneda"
              />
            </span>
            <textarea
              onChange={(event) => {
                event.preventDefault();
                seteditValoracion(event.target.value);
              }}
              value={editValoracion}
              className="inputEditOperation"
            />
          </div>
          <div className="divInputValoracionRight">
            <img
              onClick={addValoracion}
              src={rutaImagenes + 'aceptar.png'}
              title="Editar/Añadir Valoracion"
              alt="Editar Valoracion"
              width="20"
              height="20"
              align="left"
              className="noborder operacionEditImg"
            />
            <img
              onClick={borrarValoracion}
              src={rutaImagenes + 'borrar.png'}
              title="Borrar Valoracion"
              alt="Borrar Valoracion"
              width="20"
              height="20"
              align="left"
              className="noborder operacionEditImg"
            />
          </div>
        </div>
      )}
    </div>
  ) : (
    <Cargandodatos texto="Recuperando Datos de Valoraciones Totales.." />
  );
}

export default Listavaloraciones;
