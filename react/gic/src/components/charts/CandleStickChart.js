import { useState, useEffect } from 'react';
import Chart from 'react-apexcharts';
import { getApiDataAxios } from '../../helpers';

function CandleStickChart({ alert, setAlert, moneda, periodo, textoPeriodo }) {
  // eslint-disable-next-line no-unused-vars
  const [state, setState] = useState({
    series: [{ data: [] }],
    options: {
      chart: {
        type: 'candlestick',
      },
      title: {
        text: textoPeriodo,
        align: 'left',
      },
      xaxis: {
        type: 'datetime',
        labels: {
          format: 'dd/MM HH:mm',
        },
      },

      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm',
        },
      },
    },
  });

  const [tableState, settableState] = useState(false);

  // Obtenemos los datos de monedas Top del Back
  async function getStats() {
    const url = `${global.config.backend}/stats/${moneda}/${periodo}`;
    const metodo = 'get';
    const datos = {};

    const data = await getApiDataAxios(url, metodo, datos);
    if (data.status === 200) {
      for (const vpunto of data.data.data) {
        state.series[0].data.push([
          vpunto.time * 1000 + 7200000,
          vpunto.open,
          vpunto.high,
          vpunto.low,
          vpunto.close,
        ]);
      }
      settableState('ok');
    } else {
      settableState(data.data.statusText);
    }
  }

  useEffect(() => {
    getStats();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    tableState && (
      <div className="mixed-chart">
        <Chart
          options={state.options}
          series={state.series}
          type="candlestick"
          // width={'95%'}
          height={'140%'}
        />
      </div>
    )
  );
}

export default CandleStickChart;
