import { useState, useEffect } from 'react';
import { getApiDataAxios } from '../../helpers';
import CandleStickChart from '../charts/CandleStickChart';
import Listavaloraciones from '../valoraciones/Listavaloraciones';
import './monedasdetails.css';

function Monedadetails({
  monedaSelect,
  setMonedaSelect,
  setAlert,
  user,
  changeFav,
  setChangeFav,
}) {
  const rutaImagenes = `${global.config.backend}/static/images/`;
  const [monedaDetails, setMonedaDetails] = useState(false);
  const [monedaCorta, setMonedaCorta] = useState(false);

  const [verValoraciones, setVervaloraciones] = useState(false);
  const [verGraficas, setVergraficas] = useState(true);

  // Añadir o quitar moneda favorita
  async function clickFavorita(event) {
    if (user.isUserLogged) {
      const url = `${global.config.backend}/monedas/favoritas/${monedaDetails.moneda}`;
      const metodo = 'post';
      const datos = {};
      const cabecera = {
        authorization: window.localStorage.getItem('accesstoken'),
        'Content-Type': 'application/json',
      };
      const data = await getApiDataAxios(url, metodo, datos, cabecera);
      if (data.status === 200) {
        !monedaDetails.id_user || monedaDetails.id_user === '0'
          ? setAlert('0-Moneda Añadida a Favoritas')
          : setAlert('0-Moneda Eliminada de Favoritas');
        setChangeFav(true);
      } else {
        setAlert('1-Error al modificar moneda: ' + data.data.message);
      }
    } else {
      setAlert(
        '1-No tienes usuario o no has iniciacio sesion. No puedes usar monedas Favoritas'
      );
    }
  }

  async function getMonedaDetails(event) {
    const url = `${global.config.backend}/monedas/favoritas/${user.idUser}/${monedaSelect}`;
    const metodo = 'get';
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setMonedaDetails(data.data.data[0]);
      setMonedaCorta(
        data.data.data[0].moneda.slice(0, data.data.data[0].moneda.indexOf('-'))
      );
    } else {
      setAlert('1-Error cargando los datos de ' + monedaSelect);
    }
  }

  function clickVerValoraciones() {
    setVervaloraciones(true);
    setVergraficas(false);
  }

  function clickVerGraficas() {
    setVervaloraciones(false);
    setVergraficas(true);
  }

  useEffect(() => {
    getMonedaDetails();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [monedaSelect, changeFav]);

  return (
    monedaCorta && (
      <>
        <div className="monedadetailsFondo"></div>
        <div className="monedadetails">
          <div className="cabeceraMonedaDetails">
            <img
              src={rutaImagenes + monedaCorta + '.png'}
              alt={monedaDetails.moneda}
              width="30"
              height="30"
              className="noborder"
            />
            <p align="left">{monedaCorta}</p>
            <p align="right">{monedaDetails.ultimo_precio} €</p>

            {monedaDetails.id_user && monedaDetails.id_user !== '0' ? (
              <img
                onClick={clickFavorita}
                src={rutaImagenes + 'star.png'}
                alt="Moneda Favorita"
                width="25"
                height="25"
                className="detailsMonedaFavorita"
                title="Quitar Moneda Favorita"
              />
            ) : (
              <img
                onClick={clickFavorita}
                src={rutaImagenes + 'starmas.png'}
                alt="Añadir Moneda Favorita"
                width="25"
                height="25"
                className="detailsMonedaFavorita"
                title="Añadir Moneda Favorita2"
              />
            )}
          </div>
          <div className="cabeceradetailsmonedas">
            <p>{monedaDetails.nombre}</p>
            <div>
              <img
                onClick={clickVerValoraciones}
                src={rutaImagenes + 'editar.png'}
                alt="Valoraciones"
                width="25"
                height="25"
                // className="1"
                title="Valoraciones"
              />
              <img
                onClick={clickVerGraficas}
                src={rutaImagenes + 'chart.png'}
                alt="Graficos"
                width="25"
                height="25"
                title="Graficos"
              />
            </div>
          </div>
          {verValoraciones && (
            <Listavaloraciones
              user={user}
              moneda={monedaDetails.moneda}
              setAlert={setAlert}
            />
          )}
          {verGraficas && (
            <>
              {/* <p>Aqui van las graficas</p> */}
              <div className="chartsContainer">
                <CandleStickChart
                  moneda={monedaDetails.moneda}
                  periodo="3600"
                  textoPeriodo="Datos Horarios"
                ></CandleStickChart>
                <hr></hr>
                <CandleStickChart
                  moneda={monedaDetails.moneda}
                  periodo="86400"
                  textoPeriodo="Datos Diarios"
                ></CandleStickChart>
              </div>
            </>
          )}
          <button onClick={() => setMonedaSelect(false)}>Cerrar</button>
          <br></br>
        </div>
      </>
    )
  );
}

export default Monedadetails;
