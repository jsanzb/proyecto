import './modalconfirm.css';

function Modalconfirm({ confirm, setConfirm }) {
  const rutaImagenes = `${global.config.backend}/static/images/`;

  function devolverConfirmacion(confirmacion) {
    setConfirm(confirmacion);
  }

  return (
    <div className="modalContenedor">
      <div className="modalFondoConfirm"></div>
      <div className="modalMensajeConfirm">
        <br></br>
        {confirm.aviso === 1 ? (
          <img
            src={rutaImagenes + 'borrar.png'}
            alt="Error"
            width="50"
            height="50"
            className="noborder"
          />
        ) : (
          <img
            src={rutaImagenes + 'aceptar.png'}
            alt="Realizado"
            width="50"
            height="50"
            className="noborder"
          />
        )}
        <p>{confirm.mensaje}</p>

        <br></br>
        <div className="modalButtonConfirm">
          <button
            className="buttonConfirm"
            onClick={() => devolverConfirmacion(confirm.proceso)}
          >
            Ok
          </button>
          <button
            className="buttonConfirm"
            onClick={() => devolverConfirmacion(false)}
          >
            Cancelar
          </button>
        </div>
        <br></br>
      </div>
    </div>
  );
}

export default Modalconfirm;
