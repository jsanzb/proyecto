import { useState } from 'react';
import './modalinput.css';

function Modalinput({
  input,
  setInput,
  valorinput,
  setValorinput,
  alert,
  setAlert,
}) {
  const [inputdata, setInputdata] = useState(input.valorpre);
  const rutaImagenes = `${global.config.backend}/static/images/`;

  function devolverConfirmacion(confirmacion) {
    if (
      confirmacion &&
      (inputdata === '' || inputdata.length > input.longitud)
    ) {
      setAlert('1-Longitud de los datos incorrecta.');
      setInput(false);
      setValorinput(false);
    } else {
      if (inputdata !== input.valorpre) {
        setValorinput(inputdata);
        setInput(confirmacion);
      } else {
        setInput(false);
      }
    }
  }

  return (
    <div className="modalContenedor">
      <div className="modalFondoInput"></div>
      <div className="modalMensajeInput">
        <br></br>
        {input.aviso === 1 ? (
          <img
            src={rutaImagenes + 'editar.png'}
            alt="Error"
            width="50"
            height="50"
            className="noborder"
          />
        ) : (
          <img
            src={rutaImagenes + 'aceptar.png'}
            alt="Realizado"
            width="50"
            height="50"
            className="noborder"
          />
        )}
        <p>{input.mensaje}</p>

        <br></br>

        <form className="inputDataForm">
          <label className="inputDataLabel" htmlFor="inputData">
            {input.password ? (
              <input
                onChange={(event) => {
                  setInputdata(event.target.value);
                }}
                className="inputData"
                value={inputdata}
                type="password"
              />
            ) : (
              <input
                onChange={(event) => {
                  setInputdata(event.target.value);
                }}
                className="inputData"
                value={inputdata}
                type="text"
              />
            )}
          </label>
        </form>
        <br></br>

        <div className="modalButtonInput">
          <button
            className="buttonConfirmInput"
            onClick={() => devolverConfirmacion(input.proceso)}
          >
            Ok
          </button>
          <button
            className="buttonConfirmInput"
            onClick={() => devolverConfirmacion(false)}
          >
            Cancelar
          </button>
        </div>
        <br></br>
      </div>
    </div>
  );
}

export default Modalinput;
