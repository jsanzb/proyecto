import './modalalert.css';

function Modalalert({ alert, setAlert }) {
  const rutaImagenes = `${global.config.backend}/static/images/`;
  let error = false;
  let mensaje = false;

  if (alert.charAt(0) === '0') {
    error = false;
    mensaje = alert.slice(2);
  } else if (alert.charAt(0) === '1') {
    error = true;
    mensaje = alert.slice(2);
  } else {
    mensaje = alert;
  }

  return (
    <div className="modalContenedor">
      <div className="modalFondo"></div>
      <div className="modalMensaje">
        <br></br>
        {error ? (
          <img
            src={rutaImagenes + 'borrar.png'}
            alt="Error"
            width="50"
            height="50"
            className="noborder"
          />
        ) : (
          <img
            src={rutaImagenes + 'aceptar.png'}
            alt="Realizado"
            width="50"
            height="50"
            className="noborder"
          />
        )}
        <p>{mensaje}</p>
        <div></div>
        <br></br>
        <button className="button" onClick={() => setAlert(false)}>
          {' '}
          Cerrar
        </button>
        <br></br>
      </div>
    </div>
  );
}

export default Modalalert;
