// import { FaGreaterThan } from 'react-icons/fa';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './navigator.css';

function Navigator({ user, selMenu, setAlert }) {
  const [theme, setTheme] = useState(window.localStorage.getItem('tema'));
  const [mano, setMano] = useState(window.localStorage.getItem('mano'));
  const [showMenu, setShowMenu] = useState('contenedorMenuhidden');
  const [showmenuflecha, setShowMenuFlecha] = useState(true);

  // Listener para ocultar el menu flotante cuando se hace click en cualquier sitio.
  const cuerpo = document.querySelector('html');
  cuerpo.addEventListener('click', function (e) {
    if (!showmenuflecha && e.target.className !== 'cerrarmenu') {
      mano === '1'
        ? setShowMenu('contenedorMenuhiddenDer')
        : setShowMenu('contenedorMenuhidden');
      setShowMenuFlecha(true);
      window.localStorage.setItem('menu', '0');
    }
  });

  // Cambio de Tema
  function onChangetheme(event) {
    let temaActual;
    window.localStorage.getItem('tema') === '0'
      ? (temaActual = '1')
      : (temaActual = '0');
    setTheme(temaActual);
    window.localStorage.setItem('tema', temaActual);
    // window.localStorage.setItem('tema', event.target.value);
  }

  // Cambio de Mano
  function onChangemano(event) {
    window.localStorage.getItem('menu') === '1' && onClickMenu();
    let manoActual;
    window.localStorage.getItem('mano') === '0'
      ? (manoActual = '1')
      : (manoActual = '0');
    setMano(manoActual);
    window.localStorage.setItem('mano', manoActual);
    // setShowMenuFlecha(true);
    if (manoActual === '1') {
      setAlert('Cambiado a modo Diestros para Movil.');
      setShowMenu('contenedorMenuhiddenDer');
    } else {
      setAlert('Cambiado a modo Zurdos para Movil.');
      setShowMenu('contenedorMenuhidden');
    }
    onClickMenu();
  }

  // Toggle para menu flotante
  function onClickMenu(event) {
    if (window.localStorage.getItem('menu') === '0') {
      mano === '1'
        ? setShowMenu('contenedorMenuvisibleDer')
        : setShowMenu('contenedorMenuvisible');

      setShowMenuFlecha(false);
      window.localStorage.setItem('menu', '1');
    } else {
      mano === '1'
        ? setShowMenu('contenedorMenuhiddenDer')
        : setShowMenu('contenedorMenuhidden');
      setShowMenuFlecha(true);
      window.localStorage.setItem('menu', '0');
    }
  }

  // Marcamos el html para el tema a usar al cargar el componente
  useEffect(() => {
    document
      .getElementsByTagName('HTML')[0]
      .setAttribute('data-tema', localStorage.getItem('tema'));

    window.localStorage.getItem('mano') === '1'
      ? setShowMenu('contenedorMenuhiddenDer')
      : setShowMenu('contenedorMenuhidden');

    setMano(window.localStorage.getItem('mano'));
  }, [theme, mano]);

  return (
    <>
      <nav className="nav-main">
        <h1 className="nav-logo">
          <Link to="/">giC</Link>
        </h1>
        <div className={'contenedorMenu ' + showMenu}>
          <ul className="nav-menu">
            {selMenu === 'Consultar' ? (
              <li className="menuSelected">
                <Link to="/consultar">Consultar</Link>
              </li>
            ) : (
              <li>
                <Link to="/consultar">Consultar</Link>
              </li>
            )}

            {user.isUserLogged && selMenu === 'Operar' && (
              <li className="menuSelected">
                <Link to="/operar">Operar</Link>
              </li>
            )}
            {user.isUserLogged && selMenu !== 'Operar' && (
              <li>
                <Link to="/operar">Operar</Link>
              </li>
            )}

            {user.isUserLogged && selMenu === 'Gestionar' && (
              <li className="menuSelected">
                <Link to="/gestionar">Gestionar</Link>
              </li>
            )}
            {user.isUserLogged && selMenu !== 'Gestionar' && (
              <li>
                <Link to="/gestionar">Gestionar</Link>
              </li>
            )}

            {selMenu === 'Aprender' ? (
              <li className="menuSelected">
                <Link to="/aprender">Aprender</Link>
              </li>
            ) : (
              <li>
                <Link to="/aprender">Aprender</Link>
              </li>
            )}

            {selMenu === 'Administrar' &&
              user.isUserLogged &&
              user.roleUser === 'admin' && (
                <li className="menuSelected">
                  <Link to="/administrar">Administrar</Link>
                </li>
              )}

            {selMenu !== 'Administrar' &&
              user.isUserLogged &&
              user.roleUser === 'admin' && (
                <li>
                  <Link to="/administrar">Administrar</Link>
                </li>
              )}

            {user.isUserLogged && (
              <li>
                <Link to="/logout">Logout</Link>
              </li>
            )}
            {!user.isUserLogged && selMenu === 'Login' && (
              <li className="menuSelected">
                <Link to="/login">Login</Link>
              </li>
            )}
            {!user.isUserLogged && selMenu !== 'Login' && (
              <li>
                <Link to="/login">Login</Link>
              </li>
            )}
          </ul>

          {showmenuflecha && (
            <h2 onClick={onClickMenu} className="cerrarmenu">
              {/* &gt; */}
              👇
            </h2>
          )}
        </div>
      </nav>
      {user.isUserLogged && user.avatarUser ? (
        <Link to="/profile">
          <img
            className={'avatar-image'}
            width="40px"
            height="40px"
            title={user.emailUser}
            src={`${global.config.backend}/static/uploads/${user.avatarUser}`}
            alt="Imagen Avatar"
          />
        </Link>
      ) : (
        <Link to="/nuevousuario">
          <img
            className={'avatar-image'}
            width="40px"
            height="40px"
            title="Nuevo Usuario"
            src={`${global.config.backend}/static/uploads/newuser.png`}
            alt="Imagen Avatar"
          />
        </Link>
      )}

      <img
        onClick={onChangetheme}
        className="nav-theme-imagen"
        src={`${global.config.backend}/static/images/modo_oscuro.jpg`}
        alt="Modo Oscuro"
        width="20"
        height="20"
        title="Cambiar Tema"
      />

      <img
        onClick={onChangemano}
        className="nav-mano-imagen"
        src={`${global.config.backend}/static/images/mano.png`}
        alt="Diestro o Zurdo"
        width="20"
        height="20"
        title="Diestro o Zurdo"
      />
    </>
  );
}

export default Navigator;
