import { useState, useEffect } from 'react';
import { varMoneda, classVar, getApiDataAxios } from '../../helpers';
import './tablamercado.css';
import Monedasdetails from '../monedas/Monedasdetails';
import Cargandodatos from '../cargando/Cargando';

function TablaMercado({ user, setAlert, changeFav, setChangeFav }) {
  const [monedas, setMonedas] = useState([]);
  const [tableState, settableState] = useState(false);
  const [time, setTime] = useState(new Date().toLocaleString());
  const [onlyFav, setOnlyFav] = useState(
    window.localStorage.getItem('monedasFavoritas')
  );
  const [simpleView, setSimpleView] = useState(
    window.localStorage.getItem('vistaSimple')
  );
  const [monedaSelect, setMonedaSelect] = useState('');

  const rutaImagenes = `${global.config.backend}/static/images/`;

  // Obtenemos los datos del Back
  async function getTablaMonedas(url, metodo) {
    const datos = {};
    const cabecera = {
      authorization: window.localStorage.getItem('accesstoken'),
      'Content-Type': 'application/json',
    };
    const data = await getApiDataAxios(url, metodo, datos, cabecera);
    if (data.status === 200) {
      setMonedas(data.data.data);
      settableState('ok');
    } else {
      settableState(data.data.statusText);
    }
  }
  // Temporizamos las actualizaciones a un minuto y todas o solo las favoritas segun toggle
  useEffect(() => {
    let vurl;
    if (user.isUserLogged) {
      if (onlyFav === '1') {
        vurl = `${global.config.backend}/monedas/favoritas/${user.idUser}/fav`;
      } else {
        vurl = `${global.config.backend}/monedas/favoritas/${user.idUser}/all`;
      }
    } else {
      vurl = `${global.config.backend}/monedas/all`;
    }
    getTablaMonedas(vurl, 'get');

    const intervalID = setInterval(() => {
      getTablaMonedas(vurl, 'get');
      setTime(new Date().toLocaleString('es-ES'));
    }, 60000);
    return () => {
      clearInterval(intervalID);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [changeFav, onlyFav, user.idUser, user.isUserLogged]);

  // Toggle para monedas favoritas
  function onChangeFav(event) {
    setOnlyFav(event.target.value);
    window.localStorage.setItem('monedasFavoritas', event.target.value);
  }
  // Toggle para vista simple
  function onChangeSimpleView(event) {
    setSimpleView(event.target.value);
    window.localStorage.setItem('vistaSimple', event.target.value);
  }

  // OnClick para detalle monedas
  function onClickMoneda(event) {
    event.preventDefault();
    setMonedaSelect(event.target.dataset.moneda);
  }

  // Generamos las tablas con los datos adecuados
  return !tableState ? (
    <Cargandodatos texto="Recuperando Datos de Cotizaciones.." />
  ) : (
    <div className="conttablaMercado">
      <p className="tituloTabla">
        Datos del Mercado en Euros {' (' + time + ')'}
      </p>
      <div className="contenedorOpciones">
        <label
          className="vistaSimple-label opciones-label"
          htmlFor="vistaSimple"
        >
          Vista Simple
        </label>
        <input
          onChange={onChangeSimpleView}
          value={simpleView}
          className="vistaSimple opciones"
          type="range"
          name="vistaSimple"
          min="0"
          max="1"
        />
        {user.isUserLogged && (
          <>
            <label
              className="monedasFavoritas-label opciones-label"
              htmlFor="monedasFavoritas"
            >
              Solo Favoritas
            </label>
            <input
              onChange={onChangeFav}
              value={onlyFav}
              className="monedasFavoritas opciones"
              type="range"
              name="monedasFavoritas"
              min="0"
              max="1"
            />
          </>
        )}
      </div>
      <div className="contenedorTablas">
        <div className="tablaMercado">
          <table className="preciosmercado">
            <thead></thead>
            <tbody>
              <tr>
                <td className="noborder" align="center">
                  Moneda
                </td>
                <td className="noborder" align="center">
                  Bid
                </td>
                <td className="noborder" align="center">
                  Ask
                </td>
                <td className="noborder" align="center">
                  Volumen
                </td>
                <td className="noborder" align="center">
                  Precio
                </td>
                <td align="center">Tend.</td>
                <td colSpan="2">%VarTot. 10 Per.</td>
              </tr>
              {monedas.map((moneda) => (
                <tr key={moneda.moneda}>
                  <td
                    className={
                      moneda.id_user !== '0' && moneda.id_user
                        ? 'favorita moneda'
                        : 'nofavorita moneda'
                    }
                    valign="middle"
                    title={moneda.nombre}
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    <img
                      onClick={onClickMoneda}
                      valign="middle"
                      title={moneda.nombre}
                      data-moneda={moneda.moneda}
                      src={
                        rutaImagenes +
                        moneda.moneda.slice(0, moneda.moneda.indexOf('-')) +
                        '.png'
                      }
                      alt={moneda.moneda}
                      width="13"
                      height="13"
                      className={
                        moneda.id_user !== 0 && moneda.id_user
                          ? 'moneda_favorita noborder'
                          : 'moneda_nofavorita noborder'
                      }
                    />
                    <span
                      title={moneda.nombre}
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                    >
                      {moneda.moneda.slice(0, moneda.moneda.indexOf('-'))}
                    </span>
                  </td>

                  <td
                    align="center"
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    {moneda.bid.toFixed(3) * 1}
                  </td>
                  <td
                    align="center"
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    {moneda.ask.toFixed(3) * 1}
                  </td>
                  <td
                    align="center"
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    {moneda.volumen.toFixed(2) * 1}
                  </td>
                  <td
                    className="precio_actual"
                    align="center"
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    {moneda.ultimo_precio &&
                      moneda.ultimo_precio.toFixed(3) * 1}
                  </td>
                  <td
                    align="center"
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    <img
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      src={
                        moneda.ultimo_precio > moneda.up1
                          ? rutaImagenes + 'flecha_verde_arriba.png'
                          : moneda.ultimo_precio === moneda.up1
                          ? rutaImagenes + 'igual.png'
                          : rutaImagenes + 'flecha_roja_abajo.png'
                      }
                      alt={moneda.moneda}
                      width="13"
                      height="13"
                      className="noborder"
                      valign="bottom"
                    />
                  </td>

                  <td
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                    align="center"
                    className={classVar(moneda.up10, moneda.ultimo_precio)}
                  >
                    {varMoneda(moneda.up10, moneda.ultimo_precio)}
                  </td>

                  <td
                    align="center"
                    data-moneda={moneda.moneda}
                    onClick={onClickMoneda}
                  >
                    <img
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      src={
                        moneda.ultimo_precio > moneda.up10
                          ? rutaImagenes + 'flecha_verde_arriba.png'
                          : moneda.ultimo_precio === moneda.up1
                          ? rutaImagenes + 'igual.png'
                          : rutaImagenes + 'flecha_roja_abajo.png'
                      }
                      alt={moneda.moneda}
                      width="13"
                      height="13"
                      className="noborder"
                      valign="bottom"
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        {simpleView === '0' ? (
          <div className="tablaMercadovar">
            <table className="preciosmercado">
              <thead></thead>
              <tbody>
                <tr>
                  <td className="noborder" align="center" colSpan="2">
                    Moneda
                  </td>
                  <td colSpan="10">
                    %Variacion Ultimos 10 Periodos de 1 Minuto (p1-p10)
                  </td>
                </tr>
                {monedas.map((moneda) => (
                  <tr key={moneda.moneda}>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      valign="middle"
                      title={moneda.nombre}
                      className={
                        moneda.id_user !== '0' && moneda.id_user
                          ? 'favorita moneda'
                          : 'nofavorita moneda'
                      }
                    >
                      <img
                        title={moneda.nombre}
                        data-moneda={moneda.moneda}
                        onClick={onClickMoneda}
                        src={
                          rutaImagenes +
                          moneda.moneda.slice(0, moneda.moneda.indexOf('-')) +
                          '.png'
                        }
                        valign="middle"
                        alt={moneda.moneda}
                        width="13"
                        height="13"
                        className={
                          moneda.id_user !== '0' && moneda.id_user
                            ? 'favorita moneda noborder'
                            : 'nofavorita moneda noborder'
                        }
                      />
                      <span
                        title={moneda.nombre}
                        data-moneda={moneda.moneda}
                        onClick={onClickMoneda}
                      >
                        {moneda.moneda.slice(0, moneda.moneda.indexOf('-'))}
                      </span>
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up1, moneda.ultimo_precio)}
                    >
                      {varMoneda(moneda.up1, moneda.ultimo_precio)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up2, moneda.up1)}
                    >
                      {varMoneda(moneda.up2, moneda.up1)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up3, moneda.up2)}
                    >
                      {varMoneda(moneda.up3, moneda.up2)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up4, moneda.up3)}
                    >
                      {varMoneda(moneda.up4, moneda.up3)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up5, moneda.up4)}
                    >
                      {varMoneda(moneda.up5, moneda.up4)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up6, moneda.up5)}
                    >
                      {varMoneda(moneda.up6, moneda.up5)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up7, moneda.up6)}
                    >
                      {varMoneda(moneda.up7, moneda.up6)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up8, moneda.up7)}
                    >
                      {varMoneda(moneda.up8, moneda.up7)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up9, moneda.up8)}
                    >
                      {varMoneda(moneda.up8, moneda.up8)}
                    </td>
                    <td
                      data-moneda={moneda.moneda}
                      onClick={onClickMoneda}
                      align="center"
                      className={classVar(moneda.up10, moneda.up9)}
                    >
                      {varMoneda(moneda.up10, moneda.up9)}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ) : (
          ''
        )}
      </div>
      {monedaSelect && (
        <Monedasdetails
          monedaSelect={monedaSelect}
          setMonedaSelect={setMonedaSelect}
          user={user}
          setAlert={setAlert}
          onlyFav={onlyFav}
          setOnlyFav={setOnlyFav}
          changeFav={changeFav}
          setChangeFav={setChangeFav}
        />
      )}
    </div>
  );
}

export default TablaMercado;
