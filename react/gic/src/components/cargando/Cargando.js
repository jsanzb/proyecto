import './cargando.css';

function Cargandodatos({ texto }) {
  const rutaImagenes = `${global.config.backend}/static/images/`;
  let error = false;
  let mensaje = false;

  if (texto.charAt(0) === '0') {
    error = false;
    mensaje = texto.slice(2);
  } else if (texto.charAt(0) === '1') {
    error = true;
    mensaje = texto.slice(2);
  } else {
    mensaje = texto;
  }

  return (
    <>
      <br></br>
      <div align="center" className="cargandoDatos">
        {error ? (
          <img
            valign="middle"
            align="center"
            src={rutaImagenes + 'borrar.png'}
            alt="Sin Datos"
            width="25"
            height="25"
            className="noborder"
          />
        ) : (
          <img
            valign="middle"
            align="center"
            src={rutaImagenes + 'reload.png'}
            alt="Cargando Datos"
            width="25"
            height="25"
          />
        )}
        <p align="center" valign="middle">
          {mensaje}
        </p>
      </div>
      <br></br>
    </>
  );
}

export default Cargandodatos;
