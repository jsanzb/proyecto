const jwt = require('jsonwebtoken');

const isAdmin = async (req, res, next) => {
  // Chequeamos Role.
  try {
    if (req.userAuth.role !== 'admin') {
      const error = new Error('No eres Administrador');
      error.httpStatus = 403;
      throw error;
    }

    next();
  } catch (error) {
    next(error);
  }
};

module.exports = isAdmin;
