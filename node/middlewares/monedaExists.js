const getDB = require('../bbdd/db');

const monedaExists = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { moneda } = req.body;

    const [user] = await connection.query(
      `SELECT moneda FROM monedas WHERE moneda = ?;`,
      [moneda]
    );

    if (user.length < 1) {
      const error = new Error('Moneda desconocida');
      error.httpStatus = 404;
      throw error;
    }

    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = monedaExists;
