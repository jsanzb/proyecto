require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const app = express();

const { PORT } = process.env;

// Importamos los middlewares.
const userExists = require('./middlewares/userExists');
const monedaExists = require('./middlewares/monedaExists');
const authUser = require('./middlewares/authUser');
const isAdmin = require('./middlewares/isAdmin');
const { apiLanzarSql } = require('./controllers/generico/apiLanzarSql.js');

// Importamos los controladores de usuarios.
const {
  getUser,
  // getUsers,
  newUser,
  validateUser,
  loginUser,
  editUser,
  editUserPass,
  recoverUserPass,
  resetUserPass,
  deleteUser,
  newGroup,
  deleteGroup,
  addUserToGroup,
  validateUserToGroup,
  leaveGroup,
  getGroups,
  getGroup,
  getMembers,
  editGroup,
  editUserGroup,
} = require('./controllers/users');

// Monedas Favoritas
const favCoin = require('./controllers/coins/favCoin');
const getCoins = require('./controllers/coins/getCoins');
const getCoinsfav = require('./controllers/coins/getCoinsfav');

// Importamos los controladores de operaciones.
const {
  newOp,
  deleteOp,
  editOp,
  editOpClose,
  closeOp,
  doSimulation,
  getOpOpen,
  getOpClose,
  getOpsClose,
  getOpsOpen,
} = require('./controllers/ops');

// Importamos los controladores de Ayuda.
const { getTemas, getSubtemas, getContenido } = require('./controllers/ayuda');

// Importamos los controladores de Inversion.
const {
  getTotalsGroups,
  getTotalsGroupsCerradas,
  getTotalsGroupsOperaciones,
  // getTotalsGroupsOperacionesCerradas,
} = require('./controllers/inversiones');

// const { getTotalsGroups } = require('./controllers/inversiones');
// const { getTotalsGroupsOperaciones } = require('./controllers/inversiones');

// Importamos los controladores de Administracion.
const { getLog } = require('./controllers/admins');

// Importamos los controladores de Ratings.
const {
  getRatings,
  getRatingUsers,
  addRating,
  deleteRating,
} = require('./controllers/ratings');

// Importamos los controladores de Estadisticas.
const { getTops, getStats } = require('./controllers/stats');

// Cors
app.use(
  cors({
    origin: [
      'http://casa.sanz.family',
      'http://localhost:3001',
      'http://84.121.174.196',
    ],
  })
);

// Logger.
app.use(morgan('dev'));

// Traduce el body y lo transforma en un objeto JS.
app.use(express.json());

// Nos permite leer bodys en formato "form-data".
app.use(fileUpload());

// Servidor ficheros estaticos
app.use(express.static('static'));

/**
 * ############################
 * ## Endpoint Genericos API ##
 * ############################
 */
app.get('/api*', apiLanzarSql);

app.get('/monedas/:moneda', getCoins); //Obtener todos los datos de monedas

/**
 * ############################
 * ## Endpoint Ayuda         ##
 * ############################
 */

app.get('/ayuda/temas', getTemas); //Obtener todos los temas de ayuda
app.get('/ayuda/subtemas/:tema', getSubtemas); //Obtener subtemas por el tema enviado
app.get('/ayuda/contenido/:subtema', getContenido); //Obtener contenido por el subtema enviado

/**
 * ############################
 * ## Endpoint Estadisticas  ##
 * ############################
 */

app.get('/stats/tops/:periodo/:orden', getTops); //Obtener los tops de cotizaciones de monedas
app.get('/stats/:moneda/:periodo', getStats); //Obtener los los datos de una moneda en el periodo

/**
 * ########################
 * ## Endpoints Usuarios ##
 * ########################
 */

// // Crear un usuario.
app.post('/user', newUser);

// // Validar usuario.
app.get('/user/validate/:registrationCode', validateUser);

// // Obtener info de un usuario en concreto.
app.get('/user/:idUser', authUser, userExists, getUser);

// // Obtener info todos usuarios.
// app.get('/users', authUser, getUsers);

// // Login de usuario.
app.post('/user/login', loginUser);

// // Editar nombre, email y avatar de usuario.
app.put('/user/:idUser', authUser, userExists, editUser);

// // Editar Alias de usuario en grupo.
app.put('/group/user/:idGroup', authUser, editUserGroup);

// // Editar contraseña de usuario.
app.put('/user/:idUser/password', authUser, userExists, editUserPass);

// // Enviar correo de recuperación de contraseña.
app.put('/user/password/recover', recoverUserPass);

// Resetear contraseña de usuario con un código de recuperación.
app.put('/user/password/reset', resetUserPass);

// Eliminar usuario.
app.delete('/user/:idUser', authUser, userExists, deleteUser);

//Crear nuevo grupo.
app.post('/group', authUser, newGroup);

//Borrar un grupo.
app.delete('/group/:idGroup', authUser, deleteGroup);

//Editar un grupo.
app.put('/group/:idGroup', authUser, editGroup);

//Añadir un usuario a un grupo.
app.post('/group/:idGroup', authUser, addUserToGroup);

// Usuario acepta invitación de grupo
app.put('/group/validate/:idGroup', authUser, validateUserToGroup);

// Usuario sale de un grupo
app.put('/group/leave/:idGroup', authUser, leaveGroup);

// Mostrar información de todos los grupos de un usuario
app.get('/group/user/:idUser', authUser, userExists, getGroups);

// Mostrar información de un grupo
app.get('/group/:idGroup', authUser, getGroup);

// Mostrar miembros de un grupo
app.get('/group/members/:idGroup', authUser, getMembers);

//Añadir monedas favoritas.
app.post('/monedas/favoritas/:moneda', authUser, favCoin);

// Obtener lista de monedas incluidas las favoritas del usuario (all para todas, fav para favoritas)
app.get('/monedas/favoritas/:userId/:moneda', getCoinsfav);

// Endpoint servidor imagenes avatar
app.get('/static/:folder/:img', function (req, res) {
  const { img } = req.params;
  const { folder } = req.params;
  res.sendFile(__dirname + `/static/${folder}/${img}`);
});

/**
 * ##########################
 * ## Endpoints Operations ##
 * ##########################
 */

//Abrir una nueva operación.
app.post('/operations/open', authUser, newOp);

//Borrar una operación.
app.delete('/operations/delete/:idOperation', authUser, deleteOp); //cambiar a operation

//Editar una operación
app.put('/operations/edit/:idOperation', authUser, editOp); //cambiar a operation

//Editar una operación cerrada
app.put('/operations/editclose/:idOperation', authUser, editOpClose);

//Cerrar una operación
app.post('/operations/close', authUser, closeOp);

//Crear Simulacion
app.post('/operations/simulation/:idUser', authUser, userExists, doSimulation);

//Mostrar operacion abierta
app.get('/operation/open/:idOperation', authUser, getOpOpen);

//Mostrar operacion cerrada
app.get('/operation/close/:idOperation', authUser, getOpClose);

//Mostrar operaciones abiertas
app.get('/operations/open/:idGroup', authUser, getOpsOpen);

//Mostrar operaciones cerradas
app.get('/operations/close/:idGroup', authUser, getOpsClose);

/**
 * ##############################
 * ## Endpoints Inversiones    ##
 * ##############################
 */

//Mostrar inversion de todos los grupos de un usuario
app.get('/inversion/groups/:idUser', authUser, userExists, getTotalsGroups);
app.get(
  '/inversion/groups/closed/:idUser',
  authUser,
  userExists,
  getTotalsGroupsCerradas
);

//Mostrar inversion de todas las operaciones de un grupo
app.get('/inversion/operations/:idGroup', authUser, getTotalsGroupsOperaciones);
// app.get(
//   '/inversion/operations/closed/:idGroup',
//   authUser,
//   getTotalsGroupsOperacionesCerradas
// );

/**
 * ##############################
 * ## Endpoints Administracion ##
 * ##############################
 */

//Abrir una nueva operación.
app.get('/logs', authUser, isAdmin, getLog);

/**
 * #######################
 * ## Endpoints Ratings ##
 * #######################
 */

//Ver Ratings Globales, permisos publicos.
app.get('/ratings', getRatings);

//Ver Ratings Globales, permisos publicos.
app.get('/ratingusers', getRatingUsers);

//Add Rating.
app.post('/rating/:idUser', authUser, userExists, monedaExists, addRating);

//Delete Rating.
app.delete('/rating/:idUser', authUser, userExists, monedaExists, deleteRating);

/**
 * #######################
 * ## Error & Not Found ##
 * #######################
 */

// Middleware de error.
app.use((error, req, res, next) => {
  console.error(error);
  res.status(error.httpStatus || 500).send({
    status: 'error',
    message: error.message,
  });
});

// Middleware de not found.
app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    message: 'Not found',
  });
});

app.listen(PORT, () =>
  console.log(`Server listening at http://localhost:${PORT}`)
);
