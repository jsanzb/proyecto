require('dotenv').config();
const { format } = require('date-fns');
const axios = require('axios');
const { COINBASE_API } = process.env;
const getDB = require('./bbdd/db.js');
// let monedas = [];
// let listaMonedas;

const getMonedas = async () => {
  try {
    // Cargamos monedas activadas
    connection = await getDB();
    const cadenaSql = `select moneda from monedas where activada = -1;`;
    const [result] = await connection.query(cadenaSql);

    // Por cada moneda, consultamos ticker y cargamos en db
    for (let index = 0; index < result.length; index++) {
      await getTickers(result[index].moneda);
    }
    const fechaEjecucion = new Date();
    const formatFechaEjecucion = format(fechaEjecucion, 'yyyy-MM-dd HH:mm:ss');
    console.log(`${formatFechaEjecucion} -> Tickers de Monedas Cargados`);
  } catch (error) {
    console.error(error);
  } finally {
    if (connection) connection.release();
  }
};

const getTickers = async (moneda) => {
  let result;
  // let datos = [];
  try {
    // Consultamos api para la moneda pasada
    result = await axios.get(`${COINBASE_API}/products/${moneda}/ticker`);
    result.data.moneda = moneda;

    // Parseamos Fecha
    const fechaLocal = new Date(result.data.time);
    const formatDate = format(fechaLocal, 'yyyy-MM-dd HH:mm:ss');

    // Cargamos datos nuevos en DB
    await connection.query(`
    UPDATE monedas SET 
    fecha_ultimo_precio='${formatDate}',
    size=${result.data.size},
    bid=${result.data.bid},
    ask=${result.data.ask},
    volumen=${result.data.volume},
    up10=up9,
    up9=up8,
    up8=up7,
    up7=up6,
    up6=up5,
    up5=up4,
    up4=up3,
    up3=up2,
    up2=up1,
    up1=ultimo_precio,
    ultimo_precio=${result.data.price}
    WHERE moneda='${result.data.moneda}';
    `);
  } catch (error) {
    console.error(error);
  } finally {
    if (connection) connection.release();
  }
};

getMonedas();
let interval = 0;
interval = setInterval(function () {
  getMonedas();
}, 60000);
