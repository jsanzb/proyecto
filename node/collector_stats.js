require('dotenv').config();
const { format } = require('date-fns');
const axios = require('axios');
const { COINBASE_API } = process.env;
const getDB = require('./bbdd/db.js');

const getMonedas = async (periodo) => {
  try {
    // Cargamos monedas activadas
    connection = await getDB();
    const cadenaSql = `select moneda from monedas where activada = -1;`;
    const [result] = await connection.query(cadenaSql);

    // Borramos tabla del periodo a cargar
    await connection.query(`TRUNCATE TABLE stats_${periodo}`);

    // Por cada moneda, consultamos ticker y cargamos en db
    for (let index = 0; index < result.length; index++) {
      await getTickers(result[index].moneda, periodo);
    }

    const fechaEjecucion = new Date();
    const formatFechaEjecucion = format(fechaEjecucion, 'yyyy-MM-dd HH:mm:ss');
    console.log(
      `${formatFechaEjecucion} -> Estadisticas periodo ${periodo} Cargadas`
    );
  } catch (error) {
    console.error(error);
  } finally {
    if (connection) connection.release();
  }
};

const getTickers = async (moneda, periodo) => {
  let result;
  try {
    // Consultamos api para la moneda pasada
    result = await axios.get(
      `${COINBASE_API}/products/${moneda}/candles?granularity=${periodo}`
    );

    for (const row of result.data) {
      // Parseamos Fecha
      const fechaLocal = new Date(row[0] * 1000);
      const formatDate = format(fechaLocal, 'yyyy-MM-dd HH:mm:ss');
      row.unshift(formatDate);
      row.unshift(moneda);
      row.push(null);
    }

    // Cargamos datos nuevos en DB
    await connection.query(
      `INSERT INTO stats_${periodo}
    VALUES ?`,
      [result.data]
    );
    // Rellenamos campo variacion
    await connection.query(
      `update stats_${periodo} a join stats_${periodo} b on a.moneda=b.moneda and a.moneda='${moneda}' and a.time=b.time+${periodo}
              set a.var=varMoneda(b.close,a.close)`
    );
  } catch (error) {
    console.error(error);
  } finally {
    if (connection) connection.release();
  }
};

// Ejecucion de arranque
try {
  getMonedas(3600);
} finally {
}

try {
  getMonedas(86400);
} finally {
}

// Ejecucion temporizada
let interval = 0;
interval = setInterval(function () {
  try {
    getMonedas(3600);
  } finally {
  }

  try {
    getMonedas(86400);
  } finally {
  }
}, 1800000);
