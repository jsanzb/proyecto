# gic

## API Generica

✅ GET [/API*] Distintas consultas definidas en una tabla Mysql

## Endpoints de usuario

✅ GET [/user/:idUser] - Retorna información de un usuario generico \
✅ POST [/user] - Crea un usuario pendiente de activar. \
✅ POST [/user/login] - Logea a un usuario retornando un token.\
✅ GET [/user/validate/:registrationCode] - Valida un usuario recién registrado (lo cambiaría a put o post)\
✅ PUT [/user/:idUser] - Edita el nombre, el email, avatar de un usuario y alias.\
✅ PUT [/user/:idUser/password] - Edita la contraseña de un usuario.\
✅ PUT [/user/password/recover] - Envia un email con el código de reset de password.\
✅ PUT [/user/password/reset] - Cambia el password de un usuario con el codigo.\
✅ DELETE [/user/:idUser] - Borra un usuario.
✅ POST [/monedas/favoritas/:idUser/:moneda] - Añade/Quita una moneda favorita al usuario
✅ GET [/monedas/favoritas/:userId/:moneda] - Lista todas las monedas incluidas las favoritas del usuario

## Endpoints Administradores

✅ GET [/log] - Informacion del Log de la APP, solo Administradores

## Endpoints de grupo

✅ POST [/group] - Crea un grupo. \
✅ PUT [/group/:idGroup] - Editar un Grupo. Si eres admin no puedes \
✅ DELETE [/group/:idGroup] - Borra un grupo. Solo si eres admin \
✅ POST [/group/:idGroup] - Añade un usuario al grupo. En el body pasamos el email \
✅ PUT [/group/validate/:idGroup] - Acepta la entrada a un grupo \
✅ PUT [/group/leave/:idGroup] - Salir de un grupo. Si eres admin no puedes, si llega userid en el body, sacamos a ese del grupo \
✅ GET [/group/:idGroup] - Mostrar información de un grupo (debemos añadir mas info según necesitemos, por ejemplo, miembros, invitaciones recibidas y enviadas) \
✅ GET [/group/user/:idUser] - Mostrar información de todos los grupos de un usuario (debemos añadir mas info según necesitemos, por ejemplo, miembros, invitaciones recibidas y enviadas) \
✅ PUT [/group/user/:idGroup] - Editar Alias del Usuario en el grupo\
✅ GET [/group/members/:idGroup] - Mostrar miembros de un grupo (si lo incluimos en los de arriba se podría borrar) \
Ver invitaciones recibidas (creo que mejor lo metemos en los de arriba) \
Ver invitaciones enviadas (creo que mejor lo metemos en los de arriba) \

## Endpoints de Operaciones

✅ POST [/operations/open] - Abrir una operacion \
✅ POST [/operations/close] - Cerrar una operacion \
✅ PUT [/operations/edit/:idOperacion] - Modificar una operacion abierta \
✅ DELETE [/operations/delete/:idOperacion] - Borra una operacion abierta \
✅ PUT [/operations/editclose/:idOperacion] - Modificar una operacion cerrada \
✅ POST [/operations/simulation/:idUser] - Crear una Simulacion para el usuario \
✅ GET [/operation/open/:idOperation] - Muestra una operación abierta \
✅ GET [/operation/close/:idOperation] - Muestra una operación cerrada \
✅ GET [/operations/open/:idGroup] - Muestra todas las operaciones abiertas de un grupo. Quizas deberíamos juntarlo con el de abajo y devolver un objeto con 2 arrays, uno de abiertas y otro de cerradas. \
✅ GET [/operations/close/:idGoup] - Muestra todas las operaciones cerradas de un grupo

## Endpoints de Inversiones

✅ GET [/inversion/groups/:idUser] - Mostrar información de inversion de todos los grupos de un usuario
✅ GET [/inversion/operations/:idGroup] - Mostrar información de inversion de todas las operaciones de un grupo
✅ GET [/monedas/:moneda] - Mostrar información de todas las monedas (con param all) o una especifica.

## Endpoints de valoraciones

Valoración 0-5 y comentario opcional (no hay registro si no ha valorado la moneda)
Las valoraciones globales (promedio de puntuacion) y detalladas (con los comentarios) son publicas, las puede ver todo el mundo

✅ GET [/ratings] Ver valoración total de todas las monedas \
✅ GET [/ratingusers] Ver todas las valoraciones \
✅ POST [/rating/:idUser] Editar valoración (o poner la primera), la moneda viene en el Body \
✅ DELETE [/rating/:idUser] Borrar valoración, la moneda viene en el Body

## Colector API Coinbase

✅ Recoger cotizaciones Actuales y guardarlas a db (periodicidad 1 Minuto)
✅ Recoger cotizaciones Historicas y guardarlas a db (periodicidad variable en base a que periodos recojamos)
