const getDB = require('../../bbdd/db');

const validateUserToGroup = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idGroup } = req.params;
    const { idUser } = req.body;
    let userId;

    //  Si viene id user en el body, sacamos a ese del grupo, sino al que hace la llamada
    if (idUser) {
      userId = idUser;
      const [grupo] = await connection.query(
        `SELECT id_group FROM users_groups WHERE id_group = ? AND id_user= ? AND role="admin";`,
        [idGroup, req.userAuth.idUser]
      );
      if (grupo.length < 1) {
        const error = new Error('No eres un administrador del grupo');
        error.httpStatus = 404;
        throw error;
      }
    } else {
      userId = req.userAuth.idUser;
    }

    //Debemos comprobar que estamos en ese grupo (o estamos invitados sin aceptar)
    //Si somos admin no podemos salir (solo si somos user), solo borrar el grupo
    const [grupo] = await connection.query(
      `SELECT id_group FROM users_groups WHERE id_group = ? AND id_user= ? AND role="user";`,
      [idGroup, userId]
    );

    //Si hay menos de un registro es que no encuentra el grupo indicado como que estamos invitados
    if (grupo.length < 1) {
      const error = new Error(
        'No estas ese grupo o el grupo no existe o es tu grupo personal'
      );
      error.httpStatus = 404;
      throw error;
    }

    //Nos salimos del grupo, borrando la relacion en users_groups. Si no habiamos aceptado, también borramos la relacion.
    await connection.query(
      `
            DELETE FROM users_groups WHERE id_user=? AND id_group=?;`,
      [userId, idGroup]
    );

    res.send({
      status: 'ok',
      message: `Has salido del grupo ${idGroup}`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = validateUserToGroup;
