const getDB = require('../../bbdd/db');

const getGroup = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idGroup } = req.params;

    const [grupo] = await connection.query(
      `SELECT * FROM api_grupo_x_users WHERE id_group= ${idGroup};`
    );

    if (grupo.length < 1) {
      const error = new Error('Ese grupo no existe');
      error.httpStatus = 404;
      throw error;
    }

    let groupInfo = [];

    // metemos todos los usaurios del grupo para ver si alguno es que hace la consulta
    let grupos = [];
    for (let i = 0; i < grupo.length; i++) {
      grupos.push(grupo[i].id_user * 1);
    }

    // Si el usuario que solicita los datos es el propio usuario o un administrador.
    if (
      grupos.includes(req.userAuth.idUser * 1) || //Vemos si el usuario esta en el array
      req.userAuth.role === 'admin'
    ) {
      for (let i = 0; i < grupo.length; i++) {
        groupInfo[i] = {
          id: grupo[i].id,
          name: grupo[i].name,
          comentario: grupo[i].comentario,
          grupo_activado: grupo[i].grupo_activado,
          personal: grupo[i].personal,
          role: grupo[i].role,
          inversion: grupo[i].inversion_grupo,
          activado: grupo[i].grupo_activado,
        };
      }
    } else {
      const error = new Error(
        'No tienes permisos para ver los grupos de ese usuario'
      );
      error.httpStatus = 403;
      throw error;
    }

    res.send({
      status: 'ok',
      data: groupInfo,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getGroup;
