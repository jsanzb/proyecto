const getDB = require('../../bbdd/db');

const getMembers = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idGroup } = req.params;

    const [grupo] = await connection.query(
      `SELECT * FROM api_grupo_x_users WHERE id_group= ${idGroup};`
    );

    if (grupo.length < 1) {
      const error = new Error('Ese grupo no existe');
      error.httpStatus = 404;
      throw error;
    }

    let members = [];

    // metemos todos los usuarios del grupo para ver si alguno es que hace la consulta
    let grupos = [];
    for (let i = 0; i < grupo.length; i++) {
      grupos.push(grupo[i].id_user * 1);
    }

    // Si el usuario que solicita los datos es el propio usuario o un administrador.
    if (
      grupos.includes(req.userAuth.idUser * 1) || //Vemos si el usuario esta en el array
      req.userAuth.role === 'admin'
    ) {
      for (let i = 0; i < grupos.length; i++) {
        members[i] = {
          id: grupos[i],
          alias: grupo[i].alias,
          role: grupo[i].role,
          activado: grupo[i].activado,
        };
      }
    } else {
      const error = new Error(
        'No tienes permisos para ver los usuarios de ese grupo'
      );
      error.httpStatus = 403;
      throw error;
    }

    res.send({
      status: 'ok',
      data: members,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getMembers;
