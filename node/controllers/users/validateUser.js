const getDB = require('../../bbdd/db');

const validateUser = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { registrationCode } = req.params;

    // Comprobamos si hay algún usuario pendiente de validar con ese código.
    const [user] = await connection.query(
      `SELECT id,alias FROM users WHERE registrationCode = ?;`,
      [registrationCode]
    );

    if (user.length < 1) {
      const error = new Error(
        'No hay usuarios pendientes de validar con este código'
      );
      error.httpStatus = 404;
      throw error;
    }

    // LOG ################################
    await connection.query(
      `call datos2log ('users',concat('id=',?),?,'validate');`,
      [user[0].id, user[0].id]
    );

    // Activamos el usuario y eliminamos el código.
    await connection.query(
      `UPDATE users SET active = true,avatar='defaultAvatar.png', registrationCode = NULL WHERE registrationCode = ?;`,
      [registrationCode]
    );

    //Creamos un grupo con el campo nombre igual al usuario
    await connection.query(
      `
            INSERT INTO grupos (name,comentario)
            VALUES 
            (?,"Cartera Personal");`,
      [user[0].id]
    );

    //Sacamos el id de ese grupo recien creado
    const [grupo] = await connection.query(
      `SELECT id FROM grupos WHERE name = ?;`,
      [user[0].id]
    );

    //Ponemos el nombre correspondiente
    await connection.query(
      `UPDATE grupos SET name = "Personal" WHERE id = ?;`,
      [grupo[0].id]
    );

    // LOG ################################
    await connection.query(
      `call datos2log ('grupos',concat('id=',?),?,'add');`,
      [grupo[0].id, user[0].id]
    );

    //Creamos la relacion en users_groups
    await connection.query(
      `
            INSERT INTO users_groups (id_user,id_group,role,alias,activado,inversion_grupo)
            VALUES 
            (?,?,"admin",?,-1,100);`,
      [user[0].id, grupo[0].id, user[0].alias]
    );

    // LOG ################################
    await connection.query(
      `call datos2log ('users_groups',concat('id_group=',?),?,'add');`,
      [grupo[0].id, user[0].id]
    );

    res.send({
      status: 'ok',
      message: 'Usuario verificado',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = validateUser;
