const getDB = require('../../bbdd/db');

const deleteGroup = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idGroup } = req.params;

    //esta es la id de usuario
    const userId = req.userAuth.idUser;

    // El admin de la app debe poder borrar el grupo
    if (req.userAuth.role !== 'admin') {
      //Comprobamos que existe el grupo y que el usuario es admin del grupo
      const [grupo] = await connection.query(
        `SELECT id_group FROM users_groups WHERE id_group = ? AND id_user= ? AND role="admin";`,
        [idGroup, userId]
      );
      if (grupo.length < 1) {
        const error = new Error(
          'No estas en este grupo, no eres administrador o no existe el grupo'
        );
        error.httpStatus = 404;
        throw error;
      }
    } else {
      // Para admin comprobamos que existe el grupo
      const [grupo] = await connection.query(
        `SELECT id FROM grupos WHERE id = ? ;`,
        [idGroup]
      );
      if (grupo.length < 1) {
        const error = new Error('No existe ese grupo');
        error.httpStatus = 404;
        throw error;
      }
    }

    //Comprobamos que no es un grupo personal
    const [grupo2] = await connection.query(
      `SELECT id FROM grupos WHERE id = ? AND personal=0;`,
      [idGroup]
    );
    if (grupo2.length < 1) {
      const error = new Error('No se puede borrar un grupo personal');
      error.httpStatus = 404;
      throw error;
    }

    //Posible comprobación de ser el único usuario de ese grupo, si nos sobra tiempo

    //Borramos en todas las tablas con log previo
    await connection.query(
      `call datos2log ('users_groups',concat('id_group=',?),?,'delete');`,
      [idGroup, userId]
    );
    await connection.query(
      `call datos2log ('operaciones_abiertas',concat('id_group=',?),?,'delete');`,
      [idGroup, userId]
    );
    await connection.query(
      `call datos2log ('operaciones_cerradas',concat('id_group=',?),?,'delete');`,
      [idGroup, userId]
    );
    await connection.query(
      `call datos2log ('grupos',concat('id=',?),?,'delete');`,
      [idGroup, userId]
    );

    await connection.query(`DELETE from users_groups WHERE id_group=?;`, [
      idGroup,
    ]);
    await connection.query(
      `DELETE from operaciones_abiertas WHERE id_group=?;`,
      [idGroup]
    );
    await connection.query(
      `DELETE from operaciones_cerradas WHERE id_group=?;`,
      [idGroup]
    );
    await connection.query(`DELETE from grupos WHERE id=?;`, [idGroup]);

    res.send({
      status: 'ok',
      message: `Grupo ${idGroup} Borrado`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteGroup;
