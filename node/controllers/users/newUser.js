const { intervalToDuration } = require('date-fns');
const getDB = require('../../bbdd/db');
const { generateRandomString, sendMail, formatDate } = require('../../helpers');

const newUser = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    let { email, password, alias } = req.body;

    if (!email || !password) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }

    // TODO: Segundo regristro de un usario no activado.

    // Comprobamos si existe en la base de datos un usuario con ese email.
    const [user] = await connection.query(
      `SELECT id FROM users WHERE email = ? and active = 1;`,
      [email]
    );

    if (user.length > 0) {
      const error = new Error(
        'Ya existe un usuario con ese email en la base de datos'
      );
      error.httpStatus = 409;
      throw error;
    }

    // Borramos por si no lo ha activado y hace un doble registro.
    const [deleteuser] = await connection.query(
      `delete FROM users WHERE email = ?;`,
      [email]
    );

    // Sacamos el alias del email sino lo ha pasado el usuario
    if (!alias || alias === '') {
      alias = email.slice(0, email.indexOf('@'));
    }

    // Creamos un código de registro (de un solo uso).
    const registrationCode = generateRandomString(40);

    // Mensaje que enviaremos al usuario.
    const emailBody = `
            Has recibido este correo porque se ha creado una cuenta con este email en la plataforma giC.
            <br></br>
            Si es correcto, pulsa en este link para verificar y activar tu cuenta:
            <br></br>
            ${process.env.PUBLIC_HOST}/activate/${registrationCode}
            <br></br>
            Si nos ha sido tu, puedes ignorar este correo.
            <br></br>
            <br></br>
            Saludos.
        `;

    // Enviamos el mensaje.
    await sendMail({
      to: email,
      subject: 'Activacion Usuario en giC',
      body: emailBody,
    });

    // Guardamos al usuario en la base de datos junto al código de registro.
    await connection.query(
      `INSERT INTO users (email, password,alias, registrationCode, createdAt) VALUES (?, SHA2(?, 512),?, ?, ?);`,
      [email, password, alias, registrationCode, formatDate(new Date())]
    );
    // Sacamos el id asignado al usuario
    [row] = await connection.query(`select id from users where email=?;`, [
      email,
    ]);

    // LOG  ################################
    await connection.query(
      `call datos2log ('users',concat('id=',?),?,'add');`,
      [row[0].id, row[0].id]
    );

    res.send({
      status: 'ok',
      message: 'Usuario registrado, comprueba tu email para activarlo',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newUser;
