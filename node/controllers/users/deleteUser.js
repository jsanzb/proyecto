const getDB = require('../../bbdd/db');

const {
  deletePhoto,
  // generateRandomString,
  // formatDate,
} = require('../../helpers');

const deleteUser = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idUser } = req.params;

    // Comprobamos que idUser no sea 1 (el administrador).
    if (Number(idUser) === 1) {
      const error = new Error(
        'El administrador principal no puede ser eliminado'
      );
      error.httpStatus = 403;
      throw error;
    }

    // Comprobamos si el usuario que intenta borrar no es el propio usuario
    // y además no es un administrador.
    if (
      req.userAuth.idUser !== Number(idUser) &&
      req.userAuth.role !== 'admin'
    ) {
      const error = new Error(
        'No tienes permisos para eliminar a este usuario'
      );
      error.httpStatus = 401;
      throw error;
    }

    // Obtenemos el nombre del avatar.
    const [user] = await connection.query(
      `SELECT avatar FROM users WHERE id = ?;`,
      [idUser]
    );

    // Si el usuario tiene avatar lo borramos del disco.
    if (user[0].avatar && user[0].avatar !== 'defaultAvatar.png') {
      await deletePhoto(user[0].avatar);
    }

    // Sacamos todas las tablas que tiene el usuarios y las borramos junto al resto de datos.
    [rows] = await connection.query(
      `select id_group FROM users_groups where id_user=? and role='admin'`,
      [idUser]
    );
    // Recorremos los grupos
    for (const grupo of rows) {
      // Primero volcamos a log
      await connection.query(
        `call datos2log ('users_groups',concat('id_group=',?),?,'delete');`,
        [grupo.id_group, req.userAuth.idUser]
      );
      await connection.query(
        `call datos2log ('operaciones_abiertas',concat('id_group=',?),?,'delete');`,
        [grupo.id_group, req.userAuth.idUser]
      );
      await connection.query(
        `call datos2log ('operaciones_cerradas',concat('id_group=',?),?,'delete');`,
        [grupo.id_group, req.userAuth.idUser]
      );
      await connection.query(
        `call datos2log ('grupos',concat('id=',?),?,'delete');`,
        [grupo.id_group, req.userAuth.idUser]
      );
      // Luego borramos de todos los datos relacionados con grupos.
      await connection.query(
        `DELETE FROM operaciones_abiertas where id_group=?`,
        [grupo.id_group]
      );
      await connection.query(
        `DELETE FROM operaciones_cerradas where id_group=?`,
        [grupo.id_group]
      );
      await connection.query(`DELETE FROM users_groups where id_group=?`, [
        grupo.id_group,
      ]);
      await connection.query(`DELETE FROM grupos where id=?`, [grupo.id_group]);
    }

    // Primero volcamos a log los borrados que vamos a hacer
    await connection.query(
      `call datos2log ('users_groups',concat('id_user=',?),?,'delete');`,
      [idUser, req.userAuth.idUser]
    );
    await connection.query(
      `call datos2log ('users_monedas_favoritas',concat('id_user=',?),?,'delete');`,
      [idUser, req.userAuth.idUser]
    );
    await connection.query(
      `call datos2log ('users',concat('id=',?),?,'delete');`,
      [idUser, req.userAuth.idUser]
    );

    // Borramos las relaciones de los grupos en los que no es admin
    await connection.query(`DELETE FROM users_groups where id_user=?`, [
      idUser,
    ]);

    // Borramos sus monedas favoritas
    await connection.query(
      `DELETE FROM users_monedas_favoritas where id_user=?`,
      [idUser]
    );

    // Borramos sus valoraciones
    await connection.query(`DELETE FROM valoraciones_users where id_user=?`, [
      idUser,
    ]);

    // Borramos al usuario
    await connection.query(`DELETE FROM users where id=?`, [idUser]);

    res.send({
      status: 'ok',
      message: 'Usuario eliminado',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteUser;
