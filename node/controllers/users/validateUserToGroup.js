const getDB = require('../../bbdd/db');

const validateUserToGroup = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

         const { idGroup } = req.params;

        //esta es la id de usuario
        const userId=req.userAuth.idUser;   

        //Debemos comprobar que estamos invitados 
        const [grupo] = await connection.query(
            `SELECT id_group FROM users_groups WHERE id_group = ? AND id_user= ? AND activado=0;`,
            [idGroup, userId]
        );
        
        //Si hay menos de un registro es que no encuentra el grupo indicado como que estamos invitados
        if (grupo.length < 1) {
            const error = new Error('No estas invitado a este grupo o no existe el grupo');
            error.httpStatus = 404;
            throw error;
        }

        
         //Aceptamos la invitación : activado=-1
         await connection.query(
            `
            UPDATE users_groups SET activado=-1 WHERE id_user=? AND id_group=? AND activado=0;`,
            [userId,idGroup]
        );




        res.send({
            status: 'ok',
            message: `Has aceptado al invitación y estas en el grupo ${idGroup}`,
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = validateUserToGroup;
