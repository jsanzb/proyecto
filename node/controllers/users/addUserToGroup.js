const getDB = require('../../bbdd/db');

const addUserToGroup = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { email } = req.body;

    const { idGroup } = req.params;

    //esta es la id de usaurio
    const userId = req.userAuth.idUser;

    // Pendiente para v2 invitar a usuario que no exista por correo.

    console.log(email);
    //Chequeamos el usuario a invitar existe
    const [user] = await connection.query(
      `SELECT id FROM users WHERE email = ? AND deleted=0;`,
      [email]
    );

    if (user.length < 1) {
      const error = new Error('Usuario no encontrado');
      error.httpStatus = 404;
      throw error;
    }

    //Debemos comprobar que somos admin en ese grupo
    const [grupo] = await connection.query(
      `SELECT id_group FROM users_groups WHERE id_group = ? AND id_user= ? AND role="admin";`,
      [idGroup, userId]
    );

    //Si hay menos de un registro es que no encuentra ningún grupo
    if (grupo.length < 1) {
      const error = new Error(
        'No estas en este grupo o no eres administrador o no existe'
      );
      error.httpStatus = 404;
      throw error;
    }

    //Buscar si es un grupo personal
    const [grupo2] = await connection.query(
      `SELECT id FROM grupos WHERE id = ? AND personal=0;`,
      [idGroup]
    );

    if (grupo2.length < 1) {
      const error = new Error(
        'No se puede invitar a nadie a tu grupo personal'
      );
      error.httpStatus = 404;
      throw error;
    }

    //Creamos la relacion en users_groups
    await connection.query(
      `
            INSERT INTO users_groups (id_user,id_group,role,activado,inversion_grupo)
            VALUES 
            (?,?,"user",0,0);`,
      [user[0].id, idGroup]
    );

    // Actualizamos el alias
    await connection.query(
      `update users_groups 
    set alias= (
    SELECT  alias FROM users where id=?)
    where isnull(users_groups.alias);`,
      [user[0].id]
    );

    res.send({
      status: 'ok',
      message: `Usuario añadido al grupo ${idGroup}`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = addUserToGroup;
