const getUser = require('./getUser');
// const getUsers = require('./getUsers');
const newUser = require('./newUser');
const validateUser = require('./validateUser');
const loginUser = require('./loginUser');
const editUser = require('./editUser');
const editUserPass = require('./editUserPass');
const recoverUserPass = require('./recoverUserPass');
const resetUserPass = require('./resetUserPass');
const deleteUser = require('./deleteUser');
const newGroup = require('./newGroup');
const deleteGroup = require('./deleteGroup');
const addUserToGroup = require('./addUserToGroup');
const validateUserToGroup = require('./validateUserToGroup');
const leaveGroup = require('./leaveGroup');
const getGroups = require('./getGroups');
const getGroup = require('./getGroup');
const getMembers = require('./getMembers');
const editGroup = require('./editGroup');
const editUserGroup = require('./editUserGroup');

module.exports = {
  getUser,
  // getUsers,
  newUser,
  validateUser,
  loginUser,
  editUser,
  editUserPass,
  recoverUserPass,
  resetUserPass,
  deleteUser,
  newGroup,
  deleteGroup,
  addUserToGroup,
  validateUserToGroup,
  leaveGroup,
  getGroups,
  getGroup,
  getMembers,
  editGroup,
  editUserGroup,
};
