const { id } = require('date-fns/locale');
const { response } = require('express');
const getDB = require('../../bbdd/db');

const editUserGroup = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();
    let aliasChange;
    const { idGroup } = req.params;
    const { newAlias } = req.body;
    const userId = req.userAuth.idUser;

    //  Alias tabla Users_groups (Hacemos independiente el alias global con el de los grupos)
    await connection.query(
      `UPDATE users_groups SET alias = ? WHERE id_user = ? and id_group=?`,
      [newAlias, userId, idGroup]
    );

    res.send({
      status: 'ok',
      message: 'Datos de usuario actualizados',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUserGroup;
