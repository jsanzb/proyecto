const getDB = require('../../bbdd/db');

const getGroups = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idUser } = req.params;

    const [grupo] = await connection.query(
      `SELECT * FROM api_grupo_x_users WHERE id_user= ${idUser} order by personal,name;`
    );

    let groupInfo = [];

    // Si el usuario que solicita los datos es el propio usuario o un administrador.
    if (
      idUser * 1 === req.userAuth.idUser * 1 ||
      req.userAuth.role === 'admin'
    ) {
      for (let i = 0; i < grupo.length; i++) {
        groupInfo[i] = {
          id: grupo[i].id,
          name: grupo[i].name,
          comentario: grupo[i].comentario,
          grupo_activado: grupo[i].grupo_activado,
          personal: grupo[i].personal,
          role: grupo[i].role,
          inversion: grupo[i].inversion_grupo,
          activado: grupo[i].grupo_activado,
          createdAt: grupo[i].createdAt,
        };
      }
    } else {
      const error = new Error(
        'No tienes permisos para ver los grupos de ese usuario'
      );
      error.httpStatus = 403;
      throw error;
    }

    res.send({
      status: 'ok',
      data: groupInfo,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getGroups;
