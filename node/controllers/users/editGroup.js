const getDB = require('../../bbdd/db');

const editGroup = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();
    const { name } = req.body;
    const { idGroup } = req.params;

    //esta es la id de usuario
    const userId = req.userAuth.idUser;

    // El admin de la app debe poder borrar el grupo
    if (req.userAuth.role !== 'admin') {
      //Comprobamos que existe el grupo y que el usuario es admin del grupo
      const [grupo] = await connection.query(
        `SELECT id_group FROM users_groups WHERE id_group = ? AND id_user= ? AND role="admin";`,
        [idGroup, userId]
      );
      if (grupo.length < 1) {
        const error = new Error(
          'No estas en este grupo, no eres administrador o no existe el grupo'
        );
        error.httpStatus = 404;
        throw error;
      }
    } else {
      // Para admin comprobamos que existe el grupo
      const [grupo] = await connection.query(
        `SELECT id FROM grupos WHERE id = ? ;`,
        [idGroup]
      );
      if (grupo.length < 1) {
        const error = new Error('No existe ese grupo');
        error.httpStatus = 404;
        throw error;
      }
    }

    //modificamos el campo nombre que nos envian
    await connection.query(`update grupos set name='${name}' where id=?;`, [
      idGroup,
    ]);

    res.send({
      status: 'ok',
      message: `Grupo ${name} editado`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editGroup;
