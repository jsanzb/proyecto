const { id } = require('date-fns/locale');
const { response } = require('express');
const getDB = require('../../bbdd/db');
const { savePhoto, deletePhoto, formatDate } = require('../../helpers');

const editUser = async (req, res, next) => {
  let connection;
  let nombreAvatar;

  try {
    connection = await getDB();
    let aliasChange;
    const { idUser } = req.params;
    const { name, email, alias } = req.body;

    // Comprobamos si no somos dueños de este usuario o admin.
    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error('No tienes permisos para editar este usuario');
        error.httpStatus = 403;
        throw error;
      }
    }

    // Si no llega ningún dato lanzamos un error.

    if (!name && !email && !(req.files && req.files.avatar)) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }

    // Obtenemos el email del usuario actual.
    const [user] = await connection.query(
      `SELECT email, avatar,alias FROM users WHERE id = ?`,
      [idUser]
    );

    // Fecha actual.
    const now = new Date();

    /**
     * ############
     * ## Avatar ##
     * ############
     *
     * Comprobamos si el usuario quiere insertar un nuevo avatar.
     *
     */
    // No admitimos el avatar si tiene el nombre que usamos por defecto para los usuarios.

    if (
      req.files &&
      req.files.avatar &&
      req.files.avatar.name.toUpperCase().slice(0, 13) === 'DEFAULTAVATAR'
    ) {
      const error = new Error(
        'No puedes usar ese nombre en el fichero paa el avatar'
      );
      error.httpStatus = 409;
      throw error;
    }

    if (req.files && req.files.avatar) {
      // Comprobamos si el usuario ya tiene un avatar previo.
      // De ser así eliminamos el avatar del disco.
      if (user[0].avatar && user[0].avatar !== 'defaultAvatar.png') {
        await deletePhoto(user[0].avatar);
      }

      // Guardamos la foto en disco y obtenemos el nombre con el
      // cuál la guardamos.
      const avatarName = await savePhoto(req.files.avatar);

      req.files ? console.log('si') : console.log('No');
      if (req.files) {
        // Guardamos el avatar en la base de datos.
        await connection.query(
          `UPDATE users SET avatar = ?, modifiedAt = ? WHERE id = ?;`,
          [avatarName, formatDate(now), idUser]
        );
        nombreAvatar = avatarName;
      }
    }

    /**
     * ###########
     * ## Email ##
     * ###########
     *
     * En caso de que haya email, comprobamos si es distinto al existente.
     *
     */
    if (email && email !== user[0].email) {
      // Comrpobamos que el nuevo email no este repetido.
      const [existingEmail] = await connection.query(
        `SELECT id FROM users WHERE email = ?;`,
        [email]
      );

      if (existingEmail.length > 0) {
        const error = new Error(
          'Ya existe un usuario con el email proporcionado en la base de datos'
        );
        error.httpStatus = 409;
        throw error;
      }

      // Si hemos llegado hasta aquí procederemos a actualizar el email del usuario.
      await connection.query(
        `UPDATE users SET email = ?, modifiedAt = ? WHERE id = ?`,
        [email, formatDate(now), idUser]
      );
    }

    /**
     * ##################
     * ## Name y alias ##
     * ##################
     *
     * En caso de que haya nombre, comprobamos si es distinto al existente.
     *
     */

    if (alias && user[0].alias !== alias) {
      aliasChange = alias;
      // } else {
      //   aliasChange = user[0].alias;
      // }
      // Alias tabla Users
      await connection.query(
        `UPDATE users SET alias = ?, modifiedAt = ?  WHERE id = ?`,
        [aliasChange, formatDate(now), idUser]
      );
      // Alias tabla Users_groups (Hacemos independiente el alias global con el de los grupos)
      // await connection.query(
      //   `UPDATE users_groups SET alias = ? WHERE id_user = ?`,
      //   [aliasChange, idUser]
      // );
    }

    if (name && user[0].name !== name) {
      await connection.query(
        `UPDATE users SET name = ?, modifiedAt = ?  WHERE id = ?`,
        [name, formatDate(now), idUser]
      );
    }

    res.send({
      avatar: nombreAvatar,
      status: 'ok',
      message: 'Datos de usuario actualizados',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUser;
