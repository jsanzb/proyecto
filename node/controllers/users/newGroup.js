const getDB = require('../../bbdd/db');

const newGroup = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();
    //const userId=4
    const { name } = req.body;

    //esta es la id de usaurio
    const userId = req.userAuth.idUser;
    //Creamos un grupo con el campo nombre igual al usuario
    await connection.query(
      `
            INSERT INTO grupos (name,comentario,personal)
            VALUES 
            (?,"Cartera de Grupo",0)`,
      [userId]
    );

    //Sacamos el id de ese grupo recien creado
    const [grupo] = await connection.query(
      `SELECT id FROM grupos WHERE name = ?;`,
      [userId]
    );

    //Ponemos el nombre correspondiente
    await connection.query(`UPDATE grupos SET name = ? WHERE id = ?;`, [
      name,
      grupo[0].id,
    ]);

    //Creamos la relacion en users_groups
    await connection.query(
      `
            INSERT INTO users_groups (id_user,id_group,role,activado,inversion_grupo)
            VALUES 
            (?,?,"admin",-1,100);`,
      [userId, grupo[0].id]
    );

    // Actualizamos el alias
    await connection.query(
      `update users_groups 
set alias= (
SELECT  alias FROM users where id=?)
where isnull(users_groups.alias);`,
      [userId]
    );

    res.send({
      status: 'ok',
      message: `Grupo ${name} creado`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newGroup;
