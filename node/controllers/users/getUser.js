const getDB = require('../../bbdd/db');

const getUser = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idUser } = req.params;
    const { idCurrentUser } = req.body; // creo que esta linea sobra

    const [user] = await connection.query(`SELECT * from users WHERE id = ?;`, [
      idUser,
    ]);

    //Objeto con información básica.
    const userInfo = {
      name: user[0].name,
      avatar: user[0].avatar,
    };

    // Si el usuario que solicita los datos es el propio usuario agregamos información extra.
    if (user[0].id === req.userAuth.idUser || req.userAuth.role === 'admin') {
      userInfo.idUser = user[0].id;
      userInfo.alias = user[0].alias;
      userInfo.email = user[0].email;
      userInfo.role = user[0].role;
      userInfo.createdAt = user[0].createdAt;
      userInfo.soyadmin = 'Si';
    }

    res.send({
      status: 'ok',
      data: userInfo,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getUser;
