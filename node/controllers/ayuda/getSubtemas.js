const getDB = require('../../bbdd/db');

const getSubtemas = async (req, res, next) => {
  let connection;

  const { tema } = req.params;

  try {
    connection = await getDB();

    // Con el Param all, sacamos todas, sino solo la pedida

    [subtemas] = await connection.query(
      `select * from ayuda_subtemas where activado=-1 and id_tema=?`,
      [tema]
    );

    res.send({
      status: 'ok',
      data: subtemas,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getSubtemas;
