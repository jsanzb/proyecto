const getDB = require('../../bbdd/db');

const getContenido = async (req, res, next) => {
  let connection;

  const { subtema } = req.params;

  try {
    connection = await getDB();

    // Con el Param all, sacamos todas, sino solo la pedida

    [contenido] = await connection.query(
      `select * from ayuda_contenido where activado=-1 and id_subtema=?`,
      [subtema]
    );

    res.send({
      status: 'ok',
      data: contenido,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getContenido;
