const getDB = require('../../bbdd/db');

const getTemas = async (req, res, next) => {
  let connection;
  let monedas = [];

  //   const { moneda } = req.params;

  try {
    connection = await getDB();

    // Con el Param all, sacamos todas, sino solo la pedida

    [temas] = await connection.query(
      `select * from ayuda_temas where activado=-1`
    );

    res.send({
      status: 'ok',
      data: temas,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getTemas;
