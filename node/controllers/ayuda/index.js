const getTemas = require('./getTemas');
const getSubtemas = require('./getSubtemas');
const getContenido = require('./getContenido');

module.exports = {
  getTemas,
  getSubtemas,
  getContenido,
};
