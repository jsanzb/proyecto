const getDB = require('../../bbdd/db');

const getLog = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const [log] = await connection.query(`SELECT * from api_logresumen;`);

    res.send({
      status: 'ok',
      data: log,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getLog;
