const getDB = require('../../bbdd/db');

const getCoinsfav = async (req, res, next) => {
  let connection;
  let sql;

  try {
    connection = await getDB();

    //Si nos llega "all" como moneda, listamos todas, si no llega "fav" solo las favoritas
    const { userId, moneda } = req.params;

    if (moneda === 'all') {
      sql = `SELECT uf.id_user , m.*
    FROM monedas m  JOIN users_monedas_favoritas uf ON m.moneda = uf.moneda
    where id_user=${userId} and  activada=-1
    union
    select '0',monedas.* from monedas where moneda NOT IN (SELECT m.moneda
    FROM monedas m  JOIN users_monedas_favoritas uf ON m.moneda = uf.moneda 
    where id_user=${userId}) and  activada=-1
    order by id_user desc,moneda;`;
    } else if (moneda === 'fav') {
      sql = `SELECT uf.id_user , m.*
    FROM monedas m  JOIN users_monedas_favoritas uf ON m.moneda = uf.moneda 
    where id_user=${userId}  and  activada=-1;`;
    } else {
      sql = `SELECT uf.id_user , m.*
    FROM monedas m  JOIN users_monedas_favoritas uf ON m.moneda = uf.moneda 
    where id_user=${userId} and m.moneda='${moneda}' and  activada=-1
    union
    select '0',monedas.* from monedas where moneda NOT IN (SELECT m.moneda
    FROM monedas m  JOIN users_monedas_favoritas uf ON m.moneda = uf.moneda 
    where id_user=${userId}) and moneda='${moneda}' and  activada=-1
    order by id_user desc,moneda;`;
    }

    const [monedas] = await connection.query(sql);

    res.send({
      status: 'ok',
      data: monedas,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getCoinsfav;
