const getDB = require('../../bbdd/db');

const favCoin = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { moneda } = req.params;

    //esta es la id de usuario conectado
    const userId = req.userAuth.idUser;

    //Existe la moneda?
    const [coin3] = await connection.query(
      `SELECT moneda FROM monedas WHERE moneda = ?;`,
      [moneda]
    );

    if (coin3.length < 1) {
      const error = new Error('Esa moneda no existe');
      error.httpStatus = 404;
      throw error;
    }

    //Buscamos si la moneda que nos pasa es favorita
    const [coin] = await connection.query(
      `SELECT moneda FROM users_monedas_favoritas WHERE id_user = ? AND moneda= ?;`,
      [userId, moneda]
    );

    //Si hay menos de un registro es que no esta esa moneda como favorita
    // entonces la ponemos como favorita
    if (coin.length < 1) {
      const [coin2] = await connection.query(
        `INSERT INTO users_monedas_favoritas (id_user,moneda)
                VALUES
                (?,?);`,
        [userId, moneda]
      );
      // Añadimos movimiento a Log
      await connection.query(
        `call datos2log ('users_monedas_favoritas',concat('id_user=',?,' AND moneda="',?,'"'),?,'add');`,
        [userId, moneda, userId]
      );
      res.send({
        status: 'ok',
        message: `Has añadido la moneda ${moneda} a las favoritas`,
      });
    }
    //En caso contrario es que está como favorita, por lo que la quitamos
    else {
      // Añadimos movimiento a Log
      await connection.query(
        `call datos2log ('users_monedas_favoritas',concat('id_user=',?,' AND moneda="',?,'"'),?,'delete');`,
        [userId, moneda, userId]
      );

      const [coin2] = await connection.query(
        `DELETE FROM users_monedas_favoritas WHERE id_user=? AND moneda=?;`,
        [userId, moneda]
      );

      res.send({
        status: 'ok',
        message: `Has eliminado la moneda ${moneda} de las favoritas`,
      });
    }
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = favCoin;
