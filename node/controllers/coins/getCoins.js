const getDB = require('../../bbdd/db');

const getCoins = async (req, res, next) => {
  let connection;
  let monedas = [];

  const { moneda } = req.params;

  try {
    connection = await getDB();

    // Con el Param all, sacamos todas, sino solo la pedida

    if (moneda === 'all') {
      [monedas] = await connection.query(
        `select * from monedas where activada=-1`
      );
    } else {
      [monedas] = await connection.query(
        `select * from monedas where activada=-1 and moneda='${moneda}'`
      );
    }

    res.send({
      status: 'ok',
      data: monedas,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getCoins;
