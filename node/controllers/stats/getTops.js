const getDB = require('../../bbdd/db');

const getTops = async (req, res, next) => {
  let connection;
  let monedas = [];

  const { periodo } = req.params;
  const { orden } = req.params;

  try {
    connection = await getDB();

    [monedas] = await connection.query(
      `SELECT * FROM stats_${periodo} where time in (select max(time) from stats_${periodo})
      and var<>'NULL'
        order by var ${orden} limit 10`
    );

    res.send({
      status: 'ok',
      data: monedas,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getTops;
