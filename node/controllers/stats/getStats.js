const getDB = require('../../bbdd/db');

const getStats = async (req, res, next) => {
  let connection;
  let monedas = [];

  const { periodo } = req.params;
  const { moneda } = req.params;

  try {
    connection = await getDB();

    [monedas] = await connection.query(
      `SELECT time,open,high,low,close FROM stats_${periodo} where moneda='${moneda}' 
      order by time asc`
    );

    res.send({
      status: 'ok',
      data: monedas,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getStats;
