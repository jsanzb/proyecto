const getTops = require('./getTops');
const getStats = require('./getStats');

module.exports = {
  getTops,
  getStats,
};
