const getTotalsGroups = require('./getTotalsGroups');
const getTotalsGroupsCerradas = require('./getTotalsGroupsCerradas');
const getTotalsGroupsOperaciones = require('./getTotalsGroupsOperaciones');
const getTotalsGroupsOperacionesCerradas = require('./getTotalsGroupsOperacionesCerradas');

module.exports = {
  getTotalsGroups,
  getTotalsGroupsCerradas,
  getTotalsGroupsOperaciones,
  getTotalsGroupsOperacionesCerradas,
};
