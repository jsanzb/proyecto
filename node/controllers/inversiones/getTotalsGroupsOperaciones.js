const getDB = require('../../bbdd/db');

const getTotalsGroupsOperaciones = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idGroup } = req.params;

    const [grupo] = await connection.query(
      `SELECT g.*,ro.* FROM api_grupo_x_users g 
      inner join api_rendimiento_operaciones ro
      on g.id_group=ro.id_group
      WHERE id_user= ${req.userAuth.idUser} and g.id_group=${idGroup} order by fecha_compra;`
    );

    if (grupo.length < 1) {
      const error = new Error('No hay operaciones del usuario en ese grupo');
      error.httpStatus = 404;
      throw error;
    }

    let groupInfo = [];

    // Si tenemos usuario o un administrador.
    if (req.userAuth.idUser || req.userAuth.role === 'admin') {
      for (let i = 0; i < grupo.length; i++) {
        groupInfo[i] = {
          id: grupo[i].id,
          name: grupo[i].name,
          comentario: grupo[i].comentario,
          grupo_activado: grupo[i].grupo_activado,
          personal: grupo[i].personal,
          role: grupo[i].role,
          inversion: grupo[i].inversion_grupo,
          activado: grupo[i].grupo_activado,
          moneda: grupo[i].moneda,
          fecha_compra: grupo[i].fecha_compra,
          numero_compra: grupo[i].numero_compra,
          precio_compra: grupo[i].precio_compra,
          comision_compra: grupo[i].comision_compra,
          precio_actual: grupo[i].precio_actual,
          precio_total_compra: grupo[i].precio_total,
          precio_total_actual: grupo[i].precio_total_actual,
          rendimiento: grupo[i].rendimiento,
          var_rendimiento: grupo[i].Var_rendimiento,
        };
      }
    } else {
      const error = new Error(
        'No tienes permisos para ver la inversion de las operaciones'
      );
      error.httpStatus = 403;
      throw error;
    }

    res.send({
      status: 'ok',
      data: groupInfo,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getTotalsGroupsOperaciones;
