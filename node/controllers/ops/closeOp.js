const getDB = require('../../bbdd/db');
const closeOp = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    let {
      idOperacion,
      numero_venta,
      precio_venta,
      comision_venta,
      fecha_venta,
    } = req.body;

    //Comprobamos si están todos los campos
    if (
      !idOperacion ||
      !numero_venta ||
      !precio_venta ||
      !comision_venta ||
      !fecha_venta
    ) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }
    // Validamos que no sean negativos
    if (
      numero_venta * 1 < 0 ||
      precio_venta * 1 < 0 ||
      comision_venta * 1 < 0
    ) {
      const error = new Error('No se admiten valores negativos');
      error.httpStatus = 400;
      throw error;
    }

    if (numero_venta * 1 <= 0) {
      const error = new Error('No puedes vender 0 o menos Monedas');
      error.httpStatus = 404;
      throw error;
    }

    //Tenemos que sacar el el idgrupo e idUser de la operación
    const [grupo] = await connection.query(
      `SELECT id_group FROM operaciones_abiertas WHERE id = ?;`,
      [idOperacion]
    );
    if (grupo.length < 1) {
      const error = new Error('No existe esa operación');
      error.httpStatus = 404;
      throw error;
    }
    const [admin] = await connection.query(
      `SELECT id_user FROM users_groups WHERE id_group = ? AND role="admin" AND activado=-1;`,
      [grupo[0].id_group]
    );

    //El idUser es el admin del grupo donde está la operación
    let idUser = admin[0].id_user;

    //Comprobamos si es el usuario dueño o un admin
    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error('No tienes permisos para hacer esta operacion');
        error.httpStatus = 403;
        throw error;
      }
    }

    // Parseamos comas a puntos para la db
    precio_venta = precio_venta.replace(',', '.');
    comision_venta = comision_venta.replace(',', '.');

    // Para los parciales, sacamos numero_compra que lo necesitaremos para actualizar
    const [numMonedasCompra] = await connection.query(
      `select numero_compra from operaciones_abiertas WHERE id = ?;`,
      [idOperacion]
    );
    let numeroCompra = numMonedasCompra[0].numero_compra;

    // Comprobamos que hay bastantes monedas para cerrar
    if (numeroCompra * 1 < numero_venta * 1) {
      const error = new Error('No puedes vender mas monedas de las que tienes');
      error.httpStatus = 404;
      throw error;
    }

    //Copiamos la operación abierta a la tabla de operaciones cerradas
    const [operation] = await connection.query(
      `INSERT INTO operaciones_cerradas 
      (id_group, moneda, 
      numero_compra,
      precio_compra,
      comision_compra,
      total_compra,
      fecha_compra,
      numero_venta,
      precio_venta,
      comision_venta,
      total_venta,
      fecha_venta,
      porcentaje_bruto,
      rendimiento_bruto,
      porcentaje_neto,
      rendimiento_neto) 
      SELECT 
      id_group, 
      moneda, 
      ${numero_venta},
      precio_compra,
      (comision_compra/(${numeroCompra}/${numero_venta})),
      ${numero_venta}*precio_compra,
      fecha_compra,
      ${numero_venta},
      ${precio_venta},
      ${comision_venta},
      ${numero_venta}*${precio_venta},
      "${fecha_venta}",
      varMoneda(precio_compra*${numero_venta},${precio_venta}*${numero_venta}),
      (${precio_venta}*${numero_venta})-(precio_compra*${numero_venta}),
      varMoneda((precio_compra*${numero_venta})+(comision_compra/(${numeroCompra}/${numero_venta})),((${precio_venta}*${numero_venta})-${comision_venta})),
      (${precio_venta}*${numero_venta})-(precio_compra*${numero_venta})-((comision_compra/(${numeroCompra}/${numero_venta}))+${comision_venta}) from operaciones_abiertas WHERE id=${idOperacion};`
    );
    // Añadimos movimiento a Log
    await connection.query(
      `call datos2log ('operaciones_cerradas',concat('id_group=',?,' AND total_venta=',?,' AND fecha_venta="',?,'"'),?,'add');`,
      [
        grupo[0].id_group,
        numero_venta * precio_venta,
        fecha_venta,
        req.userAuth.idUser,
      ]
    );

    // Comprobamos si se venden todas o solo una parte
    if (numero_venta * 1 === numeroCompra * 1) {
      // Añadimos movimiento a Log
      await connection.query(
        `call datos2log ('operaciones_abiertas',concat('id=',?),?,'delete');`,
        [idOperacion, req.userAuth.idUser]
      );
      // Borramos la operacion que estaba abierta porque son todas las monedas
      const [opBorrada] = await connection.query(
        `DELETE FROM operaciones_abiertas WHERE id = ?;`,
        [idOperacion]
      );
      res.send({
        status: 'ok',
        message: 'Operación Cerrada',
      });
    } else {
      // Actualizamos los datos porque se cierra solo una parte
      // Añadimos movimiento a Log
      await connection.query(
        `call datos2log ('operaciones_abiertas',concat('id=',?),?,'update');`,
        [idOperacion, req.userAuth.idUser]
      );
      const [opActualizada] = await connection.query(
        `UPDATE operaciones_abiertas SET numero_compra=?,comision_compra=comision_compra/(${numeroCompra}/${numero_venta})  WHERE id = ?;`,
        [numeroCompra - numero_venta, idOperacion]
      );
      res.send({
        status: 'ok',
        message: 'Operación Cerrada Parcialmente',
      });
    }
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = closeOp;
