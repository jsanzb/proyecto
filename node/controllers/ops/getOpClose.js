const getDB = require('../../bbdd/db');
const getOpClose = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idOperation } = req.params;


    //Tenemos que sacar el el idgrupo e idUser de la operación
    const [operation] = await connection.query(
      `SELECT operaciones_cerradas.*, grupos.name FROM operaciones_cerradas JOIN grupos ON 
      operaciones_cerradas.id_group=grupos.id WHERE operaciones_cerradas.id = ?;`,
      [idOperation]
    );
    if (operation.length < 1) {
      const error = new Error('No existe esa operación');
      error.httpStatus = 404;
      throw error;
    }


    const [users] = await connection.query(
      `SELECT id_user FROM users_groups WHERE id_group = ? AND activado=-1;`,
      [operation[0].id_group]
    );

    //En userList tendremos la lista de usuarios del grupo donde se hizo la operacion
    let userList=[]
    for (let i = 0; i < users.length; i++) {
      userList.push((users[i].id_user)*1)}
    
    let opInfo={}
    //Comprobamos si es el usuario dueño o un admin
    if (
      userList.includes(req.userAuth.idUser*1) ||  //Vemos si el usuario esta en el array
      req.userAuth.role === 'admin'
  ) {


    opInfo={
      id_group:operation[0].id_group,
      group_name:operation[0].name,
      moneda:operation[0].moneda,
      numero_compra:operation[0].numero_compra,
      precio_compra:operation[0].precio_compra,
      comision_compra:operation[0].comision_compra,
      fecha_compra:operation[0].fecha_compra,
      numero_venta:operation[0].numero_venta,
      precio_venta:operation[0].precio_venta,
      comision_venta:operation[0].comision_venta,
      fecha_venta:operation[0].fecha_venta,
      comentario:operation[0].comentario,
      porcentaje_bruto:operation[0].porcentaje_bruto,
      rendimiento_bruto:operation[0].rendimiento_bruto,
      porcentaje_neto:operation[0].porcentaje_neto,
      rendimiento_neto:operation[0].rendimiento_neto

    }

  }else

  {
    const error = new Error(
      'No tienes permisos para ver los grupos de ese usuario'
    );
    error.httpStatus = 403;
    throw error;

  }
  res.send({
    status: 'ok',
    data: opInfo,
  });



  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getOpClose;
