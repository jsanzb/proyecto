const getDB = require('../../bbdd/db');

const { generateRandomString, sendMail, formatDate } = require('../../helpers');

const deleteOp = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idOperation } = req.params;

    //Tenemos que sacar el idUser de la operación
    const [grupo] = await connection.query(
      `SELECT id_group FROM operaciones_abiertas WHERE id = ?;`,
      [idOperation]
    );
    if (grupo.length < 1) {
      const error = new Error('No existe esa operación');
      error.httpStatus = 404;
      throw error;
    }

    const [admin] = await connection.query(
      `SELECT id_user FROM users_groups WHERE id_group = ? AND role="admin" AND activado=-1;`,
      [grupo[0].id_group]
    );
    //El idUser es el admin del grupo donde está la operación
    let idUser = admin[0].id_user;

    //Comprobamos si es el usuario dueño o un admin

    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error('No tienes permisos para hacer esta operacion');
        error.httpStatus = 403;
        throw error;
      }
    }

    // Añadimos movimiento a Log
    await connection.query(
      `call datos2log ('operaciones_abiertas',concat('id=',?),?,'delete');`,
      [idOperation, req.userAuth.idUser]
    );

    // Borramos la operacion
    const [operacion2] = await connection.query(
      `DELETE FROM operaciones_abiertas WHERE id = ?;`,
      [idOperation]
    );

    res.send({
      status: 'ok',
      message: 'Operación borrada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteOp;
