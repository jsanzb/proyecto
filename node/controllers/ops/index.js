const newOp = require('./newOp');
const deleteOp = require('./deleteOp');
const editOp = require('./editOp');
const editOpClose = require('./editOpClose');
const closeOp = require('./closeOp');
const doSimulation = require('./doSimulation');
const getOpOpen = require('./getOpOpen');
const getOpClose = require('./getOpClose');
const getOpsClose = require('./getOpsClose');
const getOpsOpen = require('./getOpsOpen');

module.exports = {
  newOp,
  deleteOp,
  editOp,
  editOpClose,
  closeOp,
  doSimulation,
  getOpOpen,
  getOpClose,
  getOpsClose,
  getOpsOpen,
};
