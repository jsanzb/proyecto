const getDB = require('../../bbdd/db');
const getOpsOpen = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idGroup } = req.params;


    //Tenemos que sacar el el idgrupo e idUser de la operación
    const [operation] = await connection.query(
      `SELECT operaciones_abiertas.*, grupos.name FROM operaciones_abiertas JOIN grupos ON 
      operaciones_abiertas.id_group=grupos.id WHERE operaciones_abiertas.id_group = ?;`,
      [idGroup]
    );
    if (operation.length < 1) {
      const error = new Error('No existen operaciones en ese grupo');
      error.httpStatus = 404;
      throw error;
    }


    const [users] = await connection.query(
      `SELECT id_user FROM users_groups WHERE id_group = ? AND activado=-1;`,
      [idGroup]
    );

    //En userList tendremos la lista de usuarios del grupo donde se hizo la operacion
    let userList=[]
    for (let i = 0; i < users.length; i++) {
      userList.push((users[i].id_user)*1)}
    
    let opInfo=[]
    //Comprobamos si es el usuario dueño o un admin
    if (
      userList.includes(req.userAuth.idUser*1) ||  //Vemos si el usuario esta en el array
      req.userAuth.role === 'admin'
  ) {
    for (let i = 0; i < operation.length; i++) {
    

    opInfo[i]={
      id_group:operation[i].id_group,
      group_name:operation[i].name,
      moneda:operation[i].moneda,
      numero_compra:operation[i].numero_compra,
      precio_compra:operation[i].precio_compra,
      comision_compra:operation[i].comision_compra,
      fecha_compra:operation[i].fecha_compra,
      comentario:operation[i].comentario

    }
  }

  }else

  {
    const error = new Error(
      'No tienes permisos para ver los grupos de ese usuario'
    );
    error.httpStatus = 403;
    throw error;

  }
  res.send({
    status: 'ok',
    data: opInfo,
  });



  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getOpsOpen;
