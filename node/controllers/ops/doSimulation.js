const getDB = require('../../bbdd/db');

const doSimulation = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();
    const { idUser } = req.params;
    let { nombreSimulacion } = req.body;

    // Comprobamos si no somos dueños de este usuario.
    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error(
          'No tienes permisos crear simulacion a este usuario'
        );
        error.httpStatus = 403;
        throw error;
      }
    }
    // console.log(req);

    // Lanzamos Simulacion.
    [result] = await connection.query(`call crearSimulacion(?,?,?)`, [
      idUser,
      req.userAuth.idUser,
      nombreSimulacion,
    ]);
    res.send({
      status: 'ok',
      message: 'Simulacion Creada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = doSimulation;
