const getDB = require('../../bbdd/db');

// const { generateRandomString, sendMail, formatDate } = require('../../helpers');

const newOp = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    //usuario,moneda,precio compra, comision, cantidad, fecha operacion,grupo

    let { idUser, idGroup, coin, price, comision, quantity, date } = req.body;

    //Comprobamos si están todos los campos
    if (
      !idUser ||
      !idGroup ||
      !coin ||
      !price ||
      !comision ||
      !quantity ||
      !date
    ) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }
    // Validamos que no sean negativos
    if (coin * 1 < 0 || price * 1 < 0 || comision * 1 < 0 || quantity * 1 < 0) {
      const error = new Error('No se admiten valores negativos');
      error.httpStatus = 400;
      throw error;
    }

    //Comprobamos si es el usuario dueño o un admin

    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error('No tienes permisos para hacer esta operacion');
        error.httpStatus = 403;
        throw error;
      }
    }

    //Debemos comprobar que estamos en ese grupo y existe y somos admin

    const [grupo] = await connection.query(
      `SELECT id_group FROM users_groups WHERE id_group = ? AND id_user= ? AND role="admin" AND activado =-1;`,
      [idGroup, idUser]
    );

    //Si hay menos de un registro es que no encuentra el grupo indicado o no eres admin o no estas activado
    if (grupo.length < 1) {
      const error = new Error(
        'No estas ese grupo el grupo no existe o no eres administrador del grupo'
      );
      error.httpStatus = 404;
      throw error;
    }

    //Verificamos que la moneda existe
    const [coin3] = await connection.query(
      `SELECT moneda FROM monedas WHERE moneda = ?;`,
      [coin]
    );

    if (coin3.length < 1) {
      const error = new Error('Esa moneda no existe');
      error.httpStatus = 404;
      throw error;
    }

    price = price.replace(',', '.');
    comision = comision.replace(',', '.');
    quantity = quantity.replace(',', '.');

    // Añadimos operacion
    const [operation] = await connection.query(
      `INSERT INTO operaciones_abiertas (id_group,moneda,numero_compra,precio_compra,comision_compra,fecha_compra)
        VALUES (?,?,?,?,?,?);`,
      [idGroup, coin, quantity, price, comision, date]
    );

    // Añadimos movimiento a Log
    await connection.query(
      `call datos2log ('operaciones_abiertas',concat('id_group=',?,' AND numero_compra=',?,' AND precio_compra=',?,' AND fecha_compra="',?,'"'),?,'add');`,
      [idGroup, quantity, price, date, req.userAuth.idUser]
    );

    res.send({
      status: 'ok',
      message: 'Operación anotada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newOp;
