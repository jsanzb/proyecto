const getDB = require('../../bbdd/db');

// const { generateRandomString, sendMail, formatDate } = require('../../helpers');

const editOpClose = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    //usuario,moneda,precio compra, comision, cantidad, fecha operacion,grupo

    let { price, comision, quantity, date } = req.body;

    //Comprobamos si están todos los campos
    if (!price || !comision || !quantity || !date) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }

    // Validamos que no sean negativos
    if (price * 1 < 0 || comision * 1 < 0 || quantity * 1 < 0) {
      const error = new Error('No se admiten valores negativos');
      error.httpStatus = 400;
      throw error;
    }

    const { idOperation } = req.params;

    //Tenemos que sacar el idUser de la operación
    const [grupo] = await connection.query(
      `SELECT id_group FROM operaciones_cerradas WHERE id = ?;`,
      [idOperation]
    );
    if (grupo.length < 1) {
      const error = new Error('No existe esa operación');
      error.httpStatus = 404;
      throw error;
    }

    const [admin] = await connection.query(
      `SELECT id_user FROM users_groups WHERE id_group = ? AND role="admin" AND activado=-1;`,
      [grupo[0].id_group]
    );

    //El idUser es el admin del grupo donde está la operación
    let idUser = admin[0].id_user;

    //Comprobamos si es el usuario dueño o un admin

    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error('No tienes permisos para hacer esta operacion');
        error.httpStatus = 403;
        throw error;
      }
    }

    price = price.replace(',', '.');
    comision = comision.replace(',', '.');
    quantity = quantity.replace(',', '.');

    // Añadimos movimiento a Log
    await connection.query(
      `call datos2log ('operaciones_cerradas',concat('id=',?),?,'edit');`,
      [idOperation, req.userAuth.idUser]
    );
    // Editamos la operacion
    const [operation] = await connection.query(
      `UPDATE  operaciones_cerradas SET numero_venta=?,precio_venta=?,comision_venta=?,fecha_venta=?
        WHERE id=?;`,
      [quantity, price, comision, date, idOperation]
    );

    res.send({
      status: 'ok',
      message: 'Operación modificada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editOpClose;
