const getRatings = require('./getRatings');
const getRatingUsers = require('./getRatingUsers');
const addRating = require('./addRating');
const deleteRating = require('./deleteRating');

module.exports = {
  getRatings,
  getRatingUsers,
  addRating,
  deleteRating,
};
