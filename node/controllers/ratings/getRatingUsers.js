const getDB = require('../../bbdd/db');

const getRatings = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const [ratings] = await connection.query(
      `select v.*,u.alias from api_Tvaloraciones_users v join users u on
      v.id_user =u.id ;`
    );

    res.send({
      status: 'ok',
      data: ratings,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getRatings;
