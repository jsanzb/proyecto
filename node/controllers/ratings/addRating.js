const getDB = require('../../bbdd/db');

const getRatings = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idUser } = req.params;
    const { moneda } = req.body;
    const { valoracion } = req.body;
    const { comentario } = req.body;

    // Comprobamos si no somos dueños de este usuario o admin.
    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error(
          'No tienes permisos para editar esta valoracion'
        );
        error.httpStatus = 403;
        throw error;
      }
    }

    // Si no llega por lo menos la valoracion, lanzamos un error.
    if (!valoracion) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }
    // Si la valoracion es <0 o >5 lanzamos error.
    if (valoracion < 0 || valoracion > 5) {
      const error = new Error('Las Valoraciones deben estar entre 0 y 5');
      error.httpStatus = 400;
      throw error;
    }

    const [rating] = await connection.query(
      `select * from api_Tvaloraciones_users where id_user=? and moneda=?;`,
      [idUser, moneda]
    );

    if (rating.length > 0) {
      // Editamos valoracion si  existe
      await connection.query(
        `UPDATE api_Tvaloraciones_users SET valoracion = ?, comentario = ?  WHERE id_user=? and moneda=?`,
        [valoracion, comentario, idUser, moneda]
      );
      // LOG  ################################
      await connection.query(
        `call datos2log ('valoraciones_users',concat('id_user=',?,' AND moneda="',?,'"'),?,'edit');`,
        [idUser, moneda, req.userAuth.idUser]
      );
    } else {
      // Insertamos valoracion si no existe
      await connection.query(
        `INSERT INTO api_Tvaloraciones_users (id_user, moneda,valoracion,comentario) VALUES (?,?,?, ?);`,
        [idUser, moneda, valoracion, comentario]
      );

      // LOG  ################################
      await connection.query(
        `call datos2log ('valoraciones_users',concat('id_user=',?,' AND moneda="',?,'"'),?,'add');`,
        [idUser, moneda, req.userAuth.idUser]
      );
    }

    res.send({
      status: 'ok',
      data: 'Valoracion Actualizada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getRatings;
