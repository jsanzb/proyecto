const getDB = require('../../bbdd/db');

const getRatings = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const [ratings] = await connection.query(
      `select * from api_valoraciones_users_promedio;`
    );

    res.send({
      status: 'ok',
      data: ratings,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getRatings;
