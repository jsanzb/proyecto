const getDB = require('../../bbdd/db');

const deleteRating = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idUser } = req.params;
    const { moneda } = req.body;

    // Comprobamos si no somos dueños de este usuario o admin.
    if (req.userAuth.idUser !== Number(idUser)) {
      if (req.userAuth.role !== 'admin') {
        const error = new Error(
          'No tienes permisos para editar esta valoracion'
        );
        error.httpStatus = 403;
        throw error;
      }
    }

    const [rating] = await connection.query(
      `select * from api_Tvaloraciones_users where id_user=? and moneda=?;`,
      [idUser, moneda]
    );

    if (rating.length > 0) {
      // LOG  ################################
      await connection.query(
        `call datos2log ('valoraciones_users',concat('id_user=',?,' AND moneda="',?,'"'),?,'delete');`,
        [idUser, moneda, req.userAuth.idUser]
      );
      // Borramos valoracion si existe
      await connection.query(
        `DELETE from api_Tvaloraciones_users where id_user=? and moneda=?;`,
        [idUser, moneda]
      );
    } else {
      const error = new Error('No existe valoracion sobre esa moneda');
      error.httpStatus = 403;
      throw error;
    }

    res.send({
      status: 'ok',
      data: 'Valoracion Borrada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteRating;
