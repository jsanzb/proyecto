require('dotenv').config();
const getDB = require('../../bbdd/db');
const { format } = require('date-fns');

let connection;
let sqlWhere;
let path;
let formatDate;
const { PORT } = process.env;

async function apiLanzarSql(req, res) {
  try {
    connection = await getDB();
    path = req.originalUrl.replace(/\?.*$/, '');
    const now = new Date();
    formatDate = format(now, 'dd/MM/yyyy HH:mm:ss');
    console.log(path);
    [result] = await connection.query(
      `select * from api where endpoint='${path}' or concat(endpoint,'/')='${path}' 
      or endpoint='http://localhost:4000${path}' or concat(endpoint,'/')='http://localhost:4000${path}';`
    );

    if (req.query.where) {
      sqlWhere = ' where ' + req.query.where;
      sqlWhere = sqlWhere.replace(/%22/g, "'");
      sqlWhere = sqlWhere.replace(/%27/g, "'");
      sqlWhere = sqlWhere.replace(/%20/g, ' ');
    } else {
      sqlWhere = '';
    }

    if (result.length !== 0) {
      const sqlapi = result[0].sqlvar;

      connection = await getDB();
      [result] = await connection.query(`${sqlapi}${sqlWhere}`);
      console.log(`${formatDate} - > Endpoint ${path}${sqlWhere} OK`);
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Headers', '*');

      res.end(JSON.stringify(result));
      if (connection) connection.release();
    } else {
      console.log(`${formatDate} - > API "${path}${sqlWhere}" no existe`);
      res.statusCode = 404;
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Headers', '*');
      res.end(`API "${path}${sqlWhere}" no existe`);
      if (connection) connection.release();
    }
  } catch (error) {
    console.log(`${formatDate} - > Endpoint "${path}${sqlWhere}" Fail`);
    res.statusCode = 404;
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.end(`Endpoint "${path}${sqlWhere}" Fail`);
    if (connection) connection.release();
  } finally {
    if (connection) connection.release();
  }
}

module.exports = {
  apiLanzarSql,
};
