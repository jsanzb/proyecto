require('dotenv').config();
// const faker = require('faker/locale/es');
const getDB = require('./db');

const { formatDate, getRandomValue } = require('../helpers');

let connection;

const main = async () => {
  try {
    connection = await getDB();

    //  Desactivamos chequeo constraints
    await connection.query('SET FOREIGN_KEY_CHECKS = 0');

    // Eliminamos los objetos existentes.
    await connection.query('DROP TABLE IF EXISTS users;');
    await connection.query('DROP TABLE IF EXISTS grupos;');
    await connection.query('DROP TABLE IF EXISTS users_groups;');
    await connection.query('DROP TABLE IF EXISTS api;');
    await connection.query('DROP TABLE IF EXISTS monedas;');
    await connection.query('DROP TABLE IF EXISTS users_monedas_favoritas;');
    await connection.query('DROP TABLE IF EXISTS operaciones_abiertas;');
    await connection.query('DROP TABLE IF EXISTS operaciones_cerradas;');
    await connection.query('DROP TABLE IF EXISTS valoraciones_users;');
    await connection.query('DROP TABLE IF EXISTS _log_users;');
    await connection.query('DROP TABLE IF EXISTS _log_grupos;');
    await connection.query('DROP TABLE IF EXISTS _log_users_groups;');
    await connection.query('DROP TABLE IF EXISTS _log_monedas;');
    await connection.query(
      'DROP TABLE IF EXISTS _log_users_monedas_favoritas;'
    );
    await connection.query('DROP TABLE IF EXISTS _log_valoraciones_users;');
    await connection.query('DROP TABLE IF EXISTS _log_operaciones_abiertas;');
    await connection.query('DROP TABLE IF EXISTS _log_operaciones_cerradas;');

    await connection.query('DROP VIEW IF EXISTS api_Tgrupos;');
    await connection.query('DROP VIEW IF EXISTS api_Tmonedas;');
    await connection.query('DROP VIEW IF EXISTS api_Toperaciones_abiertas;');
    await connection.query('DROP VIEW IF EXISTS api_Toperaciones_cerradas;');
    await connection.query('DROP VIEW IF EXISTS api_Tusers;');
    await connection.query('DROP VIEW IF EXISTS api_Tusers_groups;');
    await connection.query('DROP VIEW IF EXISTS api_Tusers_monedas_favoritas;');
    await connection.query('DROP VIEW IF EXISTS api_Tvaloraciones_users;');
    await connection.query('DROP VIEW IF EXISTS api_LTgrupos;');
    await connection.query('DROP VIEW IF EXISTS api_LToperaciones_abiertas;');
    await connection.query('DROP VIEW IF EXISTS api_LToperaciones_cerradas;');
    await connection.query('DROP VIEW IF EXISTS api_LTusers;');
    await connection.query('DROP VIEW IF EXISTS api_LTusers_groups;');
    await connection.query('DROP VIEW IF EXISTS api_LTvaloraciones_users;');
    await connection.query('DROP VIEW IF EXISTS api_grupo_x_users;');
    await connection.query(
      'DROP VIEW IF EXISTS api_valoraciones_users_promedio;'
    );
    await connection.query(
      'DROP VIEW IF EXISTS api_LTusers_monedas_favoritas;'
    );
    await connection.query('DROP VIEW IF EXISTS api_users_x_grupo;');
    await connection.query(
      'DROP VIEW IF EXISTS api_monedas_favoritas_x_users;'
    );
    await connection.query(
      'DROP VIEW IF EXISTS api_monedas_mas_favoritas_x_users_test;'
    );
    await connection.query('DROP VIEW IF EXISTS api_monedas_activas;');
    await connection.query(
      'DROP VIEW IF EXISTS api_operaciones_abiertas_x_grupo;'
    );
    await connection.query(
      'DROP VIEW IF EXISTS api_operaciones_cerradas_x_grupo;'
    );
    await connection.query('DROP VIEW IF EXISTS api_logresumen;');
    await connection.query(
      'DROP VIEW IF EXISTS api_rendimiento_monedas_grupos;'
    );
    await connection.query('DROP VIEW IF EXISTS api_rendimiento_operaciones;');
    await connection.query(
      'DROP VIEW IF EXISTS api_rendimiento_operaciones_cerradas;'
    );
    await connection.query('DROP VIEW IF EXISTS api_rendimiento_grupos;');
    await connection.query(
      'DROP VIEW IF EXISTS api_rendimiento_grupos_cerradas;'
    );

    await connection.query('DROP FUNCTION IF EXISTS varMoneda;');

    await connection.query('DROP PROCEDURE IF EXISTS crearSimulacion;');
    await connection.query('DROP PROCEDURE IF EXISTS datos2log;');
    console.log('Objetos eliminados');

    // Crear tabla de usuarios.
    await connection.query(`
    CREATE TABLE users (
        id int unsigned NOT NULL AUTO_INCREMENT,
        email varchar(45) NOT NULL,
        name varchar(45) DEFAULT NULL,
        avatar VARCHAR(50),
        lastname varchar(45) DEFAULT NULL,
        alias varchar(40) DEFAULT NULL,
        last_login datetime DEFAULT NULL,
        password varchar(512) NOT NULL,
        active tinyint DEFAULT '0',
        deleted tinyint DEFAULT '0',
        role varchar(15) DEFAULT 'user',
        registrationCode VARCHAR(100),
        createdAt DATETIME NOT NULL, 
        modifiedAt DATETIME,
        recoverCode VARCHAR(100),
        PRIMARY KEY (id),
        UNIQUE KEY id_UNIQUE (id),
        UNIQUE KEY email_UNIQUE (email)
      );       
        `);

    // Crear tabla de grupos.
    await connection.query(`
    CREATE TABLE grupos (
        id int unsigned NOT NULL AUTO_INCREMENT,
        name varchar(45) NOT NULL,
        comentario varchar(200) DEFAULT NULL,
        activado tinyint DEFAULT '-1',
        personal tinyint DEFAULT '-1',
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP, 
        PRIMARY KEY (id),
        UNIQUE KEY id_UNIQUE (id)

      );`);
    //personal es para indicar que ese grupo no se puede borrar, pq es el primario

    // Crear tabla de grupos-usuarios.
    await connection.query(`
    CREATE TABLE users_groups (
        id_user int unsigned NOT NULL,
        id_group int unsigned NOT NULL,
        alias varchar(40) DEFAULT NULL,
        role varchar(40) NOT NULL,
        inversion_grupo float unsigned NOT NULL,
        activado tinyint DEFAULT '0',
        PRIMARY KEY (id_user,id_group),
        KEY users_groups_groups (id_group),
        CONSTRAINT users_groups_groups FOREIGN KEY (id_group) REFERENCES grupos (id),
        CONSTRAINT users_groups_users FOREIGN KEY (id_user) REFERENCES users (id)
      );
        `);

    // Crear tabla API.
    await connection.query(`
    CREATE TABLE api (
        name varchar(100) NOT NULL,
        endpoint varchar(100) NOT NULL,
        sqlvar varchar(500) NOT NULL,
        activado tinyint DEFAULT '0',
        PRIMARY KEY (endpoint),
        UNIQUE KEY endpoint_UNIQUE (endpoint)
      );`);

    // Crear tabla Monedas
    await connection.query(`
    CREATE TABLE monedas (
      moneda varchar(10) NOT NULL,
      activada tinyint NOT NULL,
      nombre varchar(40) DEFAULT NULL,
      fecha_ultimo_precio datetime DEFAULT NULL,
      ultimo_precio float DEFAULT NULL,
      size float DEFAULT NULL,
      bid float DEFAULT NULL,
      ask float DEFAULT NULL,
      volumen float DEFAULT NULL,
      comentario varchar(200) DEFAULT NULL,
      up1 float DEFAULT NULL,
      up2 float DEFAULT NULL,
      up3 float DEFAULT NULL,
      up4 float DEFAULT NULL,
      up5 float DEFAULT NULL,
      up6 float DEFAULT NULL,
      up7 float DEFAULT NULL,
      up8 float DEFAULT NULL,
      up9 float DEFAULT NULL,
      up10 float DEFAULT NULL,
      PRIMARY KEY (moneda),
      UNIQUE KEY moneda_UNIQUE (moneda)
      );`);

    // Crear tabla monedas favoritas.
    await connection.query(`
    CREATE TABLE users_monedas_favoritas (
      id_user int unsigned NOT NULL,
      moneda varchar(10) NOT NULL,
      PRIMARY KEY (id_user,moneda),
      KEY users_monedas_favoritas_monedas (id_user),
      KEY monedas_monedas_favoritas (moneda),
      CONSTRAINT monedas_monedas_favoritas FOREIGN KEY (moneda) REFERENCES monedas (moneda),
      CONSTRAINT users_monedas_favoritas FOREIGN KEY (id_user) REFERENCES users (id)
      );`);

    // Crear tabla operaciones abiertas.
    await connection.query(`
    CREATE TABLE operaciones_abiertas (
      id int unsigned NOT NULL AUTO_INCREMENT,
      id_group int unsigned NOT NULL,
      moneda varchar(10) NOT NULL,
      numero_compra float NOT NULL,
      precio_compra float NOT NULL,
      comision_compra float NOT NULL,
      fecha_compra datetime NOT NULL,
      comentario varchar(200) DEFAULT NULL,
      PRIMARY KEY (id),
      UNIQUE KEY id_UNIQUE (id),
      KEY operaciones_grupos_idx (id_group),
      KEY operaciones_monedas_idx (moneda),
      CONSTRAINT operaciones_abiertas_groups FOREIGN KEY (id_group) REFERENCES grupos (id),
      CONSTRAINT operaciones_abiertas_monedas FOREIGN KEY (moneda) REFERENCES monedas (moneda)
      );`);

    // Crear tabla operaciones cerradas.
    await connection.query(`
    CREATE TABLE operaciones_cerradas (
      id int unsigned NOT NULL AUTO_INCREMENT,
      id_group int unsigned NOT NULL,
      moneda varchar(10) NOT NULL,
      numero_compra float NOT NULL,
      precio_compra float NOT NULL,
      comision_compra float NOT NULL,
      total_compra float NOT NULL,
      fecha_compra datetime NOT NULL,
      numero_venta float NOT NULL,
      precio_venta float NOT NULL,
      comision_venta float NOT NULL,
      total_venta float NOT NULL,
      fecha_venta datetime NOT NULL,
      porcentaje_bruto float NOT NULL,
      rendimiento_bruto float NOT NULL,
      porcentaje_neto float NOT NULL,
      rendimiento_neto float NOT NULL,
      comentario varchar(200) DEFAULT NULL,
      PRIMARY KEY (id),
      UNIQUE KEY id_UNIQUE (id),
      KEY operaciones_cerradas_groups (id_group),
      KEY operaciones_cerradas_monedas (moneda),
      CONSTRAINT operaciones_cerradas_groups FOREIGN KEY (id_group) REFERENCES grupos (id),
      CONSTRAINT operaciones_cerradas_monedas FOREIGN KEY (moneda) REFERENCES monedas (moneda)
      );`);

    // Tabla Valoraciones Usuarios
    await connection.query(`
      CREATE TABLE valoraciones_users (
        id_user int unsigned NOT NULL,
        moneda varchar(10) NOT NULL,
        valoracion tinyint NOT NULL DEFAULT '0',
        comentario varchar(255) DEFAULT NULL,
        PRIMARY KEY (id_user,moneda),
        KEY valoraciones_users_users (id_user),
        KEY valoraciones_monedas (moneda),
        CONSTRAINT valoraciones_monedas FOREIGN KEY (moneda) REFERENCES monedas (moneda),
        CONSTRAINT valoraciones_users_users FOREIGN KEY (id_user) REFERENCES users (id),
        CONSTRAINT valoraciones_users_chk_1 CHECK ((valoracion >= 0) and (valoracion <= 5))
        )`);

    // Creamos las tablas Log
    await connection.query(`
    CREATE TABLE _log_grupos (
      id int unsigned NOT NULL,
      name varchar(45) NOT NULL,
      comentario varchar(200) DEFAULT NULL,
      activado tinyint DEFAULT '-1',
      personal tinyint DEFAULT '-1',
      createdAt DATETIME DEFAULT CURRENT_TIMESTAMP, 
      id_ejecutor int unsigned NOT NULL,
      accion varchar(45) NOT NULL,
      fecha_accion datetime NOT NULL
  );`);

    await connection.query(`
    CREATE TABLE _log_operaciones_abiertas (
      id int unsigned NOT NULL,
      id_group int unsigned NOT NULL,
      moneda varchar(10) NOT NULL,
      numero_compra float NOT NULL,
      precio_compra float NOT NULL,
      comision_compra float NOT NULL,
      fecha_compra datetime NOT NULL,
      comentario varchar(200) DEFAULT NULL,
      id_ejecutor int unsigned NOT NULL,
      accion varchar(45) NOT NULL,
      fecha_accion datetime NOT NULL
  );`);
    await connection.query(`
    CREATE TABLE _log_operaciones_cerradas (
      id int unsigned NOT NULL,
      id_group int unsigned NOT NULL,
      moneda varchar(10) NOT NULL,
      numero_compra float NOT NULL,
      precio_compra float NOT NULL,
      comision_compra float NOT NULL,
      total_compra float NOT NULL,
      fecha_compra datetime NOT NULL,
      numero_venta float NOT NULL,
      precio_venta float NOT NULL,
      comision_venta float NOT NULL,
      total_venta float NOT NULL,
      fecha_venta datetime NOT NULL,
      porcentaje_bruto float NOT NULL,
      rendimiento_bruto float NOT NULL,
      porcentaje_neto float NOT NULL,
      rendimiento_neto float NOT NULL,
      comentario varchar(200) DEFAULT NULL,
      id_ejecutor int unsigned NOT NULL,
      accion varchar(45) NOT NULL,
      fecha_accion datetime NOT NULL
  );`);
    await connection.query(`
    CREATE TABLE _log_users_groups (
      id_user int unsigned NOT NULL,
      id_group int unsigned NOT NULL,
      alias varchar(40) DEFAULT NULL,
      role varchar(40) NOT NULL,
      inversion_grupo float unsigned NOT NULL,
      activado tinyint DEFAULT '0',
      id_ejecutor int unsigned NOT NULL,
      accion varchar(45) NOT NULL,
      fecha_accion datetime NOT NULL
  );`);
    await connection.query(`
    CREATE TABLE _log_users (
      id int unsigned NOT NULL,
      email varchar(45) NOT NULL,
      name varchar(45) DEFAULT NULL,
      avatar varchar(50) DEFAULT NULL,
      lastname varchar(45) DEFAULT NULL,
      alias varchar(40) DEFAULT NULL,
      last_login datetime DEFAULT NULL,
      password varchar(512) NOT NULL,
      active tinyint DEFAULT '0',
      deleted tinyint DEFAULT '0',
      role varchar(15) DEFAULT 'user',
      registrationCode varchar(100) DEFAULT NULL,
      createdAt datetime NOT NULL,
      modifiedAt datetime DEFAULT NULL,
      recoverCode varchar(100) DEFAULT NULL,
      id_ejecutor int unsigned NOT NULL,
      accion varchar(45) NOT NULL,
      fecha_accion datetime NOT NULL
  );`);

    await connection.query(`
    CREATE TABLE _log_users_monedas_favoritas (
      id_user int unsigned NOT NULL,
      moneda varchar(10) NOT NULL,
      id_ejecutor int unsigned NOT NULL,
      accion varchar(45) NOT NULL,
      fecha_accion datetime NOT NULL
  );`);

    await connection.query(`
    CREATE TABLE _log_valoraciones_users (
      id_user int unsigned NOT NULL,
      moneda varchar(10) NOT NULL,
      valoracion tinyint NOT NULL DEFAULT '0',
      comentario varchar(255) DEFAULT NULL,
      id_ejecutor int unsigned NOT NULL,
      accion varchar(45) NOT NULL,
      fecha_accion datetime NOT NULL
    ) ;`);

    console.log('Tablas creadas');

    await connection.query(`CREATE  FUNCTION varMoneda (valor1 FLOAT, valor2 FLOAT) RETURNS float
    DETERMINISTIC
    BEGIN
    DECLARE salida FLOAT;
    IF valor1=0 THEN SET salida=100;
    ELSE SET salida = ROUND(((valor2 - valor1) / valor1) * 100,2);
    END IF;
    RETURN salida;
    END;`);

    console.log('Funciones creadas');

    // Reactivamos chequeo constraints
    await connection.query('SET FOREIGN_KEY_CHECKS = 1');

    // Crear vistas API
    await connection.query(`
    CREATE VIEW api_Tgrupos AS SELECT * FROM grupos;`);
    await connection.query(`
    CREATE VIEW api_Tmonedas AS SELECT * FROM monedas;`);
    await connection.query(`
    CREATE VIEW api_Toperaciones_abiertas AS SELECT * FROM operaciones_abiertas;`);
    await connection.query(`
    CREATE VIEW api_Toperaciones_cerradas AS SELECT * FROM operaciones_cerradas;`);
    await connection.query(`
    CREATE VIEW api_Tusers AS SELECT * FROM users;`);
    await connection.query(`
    CREATE VIEW api_Tusers_groups AS SELECT * FROM users_groups;`);
    await connection.query(`
    CREATE VIEW api_Tusers_monedas_favoritas AS SELECT * FROM users_monedas_favoritas;`);
    await connection.query(`
    CREATE VIEW api_Tvaloraciones_users AS select * from valoraciones_users;`);
    await connection.query(`
    CREATE VIEW api_valoraciones_users_promedio AS select moneda,avg(valoracion) as valoracion_promedio,
    COUNT(valoracion) AS numero_valoraciones from valoraciones_users group by moneda;`);
    await connection.query(`
    CREATE VIEW api_LTgrupos AS SELECT * FROM _log_grupos;`);
    await connection.query(`
    CREATE VIEW api_LToperaciones_abiertas AS SELECT * FROM _log_operaciones_abiertas;`);
    await connection.query(`
    CREATE VIEW api_LToperaciones_cerradas AS SELECT * FROM _log_operaciones_cerradas;`);
    await connection.query(`
    CREATE VIEW api_LTusers AS SELECT * FROM _log_users;`);
    await connection.query(`
    CREATE VIEW api_LTusers_groups AS SELECT * FROM _log_users_groups;`);
    await connection.query(`
    CREATE VIEW api_LTusers_monedas_favoritas AS SELECT * FROM _log_users_monedas_favoritas;`);
    await connection.query(`
    CREATE VIEW api_LTvaloraciones_users AS select * from _log_valoraciones_users;`);

    await connection.query(`
    CREATE VIEW api_monedas_activas AS select * from monedas where activada=-1 order by moneda;`);

    await connection.query(`
    CREATE VIEW api_users_x_grupo AS
    SELECT 
    g.id AS id_group,
    g.name AS group_name,
    u.id AS id_user,
    u.email AS email
    FROM
    ((users u JOIN users_groups ug ON ((u.id = ug.id_user)))
    JOIN grupos g ON ((ug.id_group = g.id)))
    ORDER BY g.id,u.id;`);

    await connection.query(`
    CREATE VIEW api_grupo_x_users AS
    SELECT 
    g.id,
    g.name,
    g.comentario,
    g.activado AS grupo_activado,
    g.personal,
    g.createdAt AS createdAt,
    ug.*
    FROM
    users_groups ug JOIN grupos g ON ug.id_group = g.id
    ORDER BY ug.id_user , ug.id_group;`);

    await connection.query(`
    CREATE VIEW api_monedas_favoritas_x_users AS
    SELECT 
    users.id AS id_user,
    users.email AS email,
    users.alias AS alias,
    monedas.*
    FROM
    users JOIN users_monedas_favoritas ON (users.id = users_monedas_favoritas.id_user)
    JOIN monedas on (users_monedas_favoritas.moneda = monedas.moneda)
    order by id_user,moneda;`);

    await connection.query(`
    CREATE VIEW api_monedas_mas_favoritas_x_users_test AS
    SELECT * FROM api_monedas_favoritas_x_users where id_user=4
    union distinct select 0,"","",monedas.* from monedas where moneda not in 
    (select moneda FROM api_monedas_favoritas_x_users where id_user=4)
    and activada=-1;`);

    await connection.query(`
    CREATE VIEW api_operaciones_abiertas_x_grupo AS
    SELECT
    g.id AS id_group,
    g.name AS group_name,
    oa.moneda as moneda,
    oa.numero_compra,
    oa.precio_compra,
    m.ultimo_precio,
    oa.comision_compra,
    oa.fecha_compra
    FROM grupos g join operaciones_abiertas oa on g.id = oa.id_group
    join monedas m on oa.moneda=m.moneda;`);

    await connection.query(`
    CREATE VIEW api_operaciones_cerradas_x_grupo AS
    SELECT
    g.id AS id_group,
    g.name AS group_name,
    oc.moneda as moneda,
    oc.numero_compra,
    oc.precio_compra,
    oc.comision_compra,
    oc.fecha_compra,
	  oc.numero_venta,
    oc.precio_venta,
    oc.comision_venta,
    oc.fecha_venta
    FROM grupos g join operaciones_cerradas oc on g.id = oc.id_group;`);

    await connection.query(`
    CREATE VIEW api_logresumen AS
    SELECT  fecha_accion as Fecha, concat ( ' User ', email, ' (',id,') -> ',accion, ' -> realizado por userid ',id_ejecutor) as Accion FROM _log_users
    union
    SELECT  fecha_accion as Fecha, concat ( ' Grupo ', name, ' (',id,') -> ',accion, ' -> realizado por userid ',id_ejecutor) as Accion FROM _log_grupos
    union
    SELECT  fecha_accion as Fecha, concat ( ' User ', alias, ' (',id_user,') <--> grupo (',id_group,') -> ',accion, ' -> realizado por userid ',id_ejecutor) as Accion FROM _log_users_groups
    union
    SELECT  fecha_accion as Fecha, concat ( ' User (', id_user, ') -- Moneda Favorita (',moneda,') -> ',accion, ' -> realizado por userid ',id_ejecutor) as Accion FROM _log_users_monedas_favoritas
    union
    SELECT  fecha_accion as Fecha, concat ( ' Operacion Abierta (',id,') <--> grupo (',id_group,') <--> Moneda (',moneda,') -> ',accion, ' -> realizado por userid ',id_ejecutor) as Accion FROM _log_operaciones_abiertas
    union
    SELECT  fecha_accion as Fecha, concat ( ' Operacion Cerrada (',id,') <--> grupo (',id_group,') <--> Moneda (',moneda,') -> ',accion, ' -> realizado por userid ',id_ejecutor) as Accion FROM _log_operaciones_cerradas
    union
    SELECT  fecha_accion as Fecha, concat ( ' Valoracion User ', ' (',id_user,') <--> moneda (',moneda,') -> ',accion, ' -> realizado por userid ',id_ejecutor) as Accion FROM _log_valoraciones_users
   order by Fecha DESC;`);

    await connection.query(`
   CREATE VIEW api_rendimiento_monedas_grupos AS
   SELECT 
       id_group AS id_group,
       moneda AS moneda,
       SUM(numero_compra) AS numero_compra,
       AVG(precio_compra) AS precio_compra,
       precio_actual AS precio_actual,
       SUM(precio_total) AS precio_total,
       ((precio_actual * SUM(numero_compra)) - SUM(precio_total)) AS rendimiento,
       VARMONEDA((SUM(numero_compra) * AVG(precio_compra)),(SUM(numero_compra) * precio_actual)) AS Var_rendimiento
   FROM
       api_rendimiento_operaciones
   GROUP BY id_group , moneda;
   `);

    await connection.query(`
   CREATE VIEW api_rendimiento_operaciones AS
   SELECT 
       oa.id AS id,
       oa.id_group AS id_group,
       oa.moneda AS moneda,
       oa.fecha_compra AS fecha_compra,
       oa.numero_compra AS numero_compra,
       oa.precio_compra AS precio_compra,
       oa.comision_compra AS comision_compra,
       m.ultimo_precio AS precio_actual,
       (oa.numero_compra * oa.precio_compra) AS precio_total,
       (oa.numero_compra * m.ultimo_precio) AS precio_total_actual,
       ((oa.numero_compra * m.ultimo_precio) - (oa.numero_compra * oa.precio_compra)) AS rendimiento,
       VARMONEDA((oa.numero_compra * oa.precio_compra),(oa.numero_compra * m.ultimo_precio)) AS Var_rendimiento
   FROM
       (operaciones_abiertas oa
       JOIN monedas m ON ((oa.moneda = m.moneda)));
   `);

    await connection.query(`
   CREATE VIEW api_rendimiento_operaciones_cerradas AS
    SELECT 
        oc.id AS id,
        oc.id_group AS id_group,
        oc.moneda AS moneda,
        oc.fecha_compra AS fecha_compra,
        oc.numero_compra AS numero_compra,
        oc.precio_compra AS precio_compra,
        oc.comision_compra AS comision_compra,
        oc.total_compra AS total_compra,
        oc.fecha_venta AS fecha_venta,
        oc.numero_venta AS numero_venta,
        oc.precio_venta AS precio_venta,
        oc.comision_venta AS comision_venta,
        oc.total_venta AS total_venta,
        oc.porcentaje_bruto AS porcentaje_bruto,
        oc.rendimiento_bruto AS rendimiento_bruto,
        oc.porcentaje_neto AS porcentaje_neto,
        oc.rendimiento_neto AS rendimiento_neto,
        oc.comentario AS comentario
    FROM
        (operaciones_cerradas oc
        JOIN monedas m ON ((oc.moneda = m.moneda)))
   `);

    await connection.query(`
    CREATE VIEW api_rendimiento_grupos AS
    SELECT 
        id_group AS id_group,
        SUM(precio_total) AS precio_total,
        SUM(precio_total_actual) AS precio_total_actual,
        (SUM(precio_total_actual) - SUM(precio_total)) AS rendimiento,
        VARMONEDA(SUM(precio_total),SUM(precio_total_actual)) AS Var_rendimiento
    FROM
        api_rendimiento_operaciones
    GROUP BY id_group;
   `);

    await connection.query(`
   CREATE VIEW api_rendimiento_grupos_cerradas AS
   SELECT 
       id_group AS id_group,
       SUM(total_compra) AS total_compra,
       SUM(total_venta) AS total_venta,
       (SUM(total_venta) - SUM(total_compra)) AS rendimiento,
       VARMONEDA(SUM(total_compra),SUM(total_venta)) AS Var_rendimiento
   FROM
       api_rendimiento_operaciones_cerradas
   GROUP BY id_group
  `);

    console.log('Vistas creadas');

    await connection.query(`
    CREATE PROCEDURE crearSimulacion (IN userid int, userid_ejecutor int, nombre_grupo VARCHAR(45)  )
    BEGIN
    DECLARE grupo INT DEFAULT 0;
    DECLARE varalias varchar(40);

    #Creamos grupo para simulacion
    insert into grupos (name,comentario,activado,personal) values (nombre_grupo,concat('Simulacionuserid',userid),-1,0);
    select id into grupo from grupos where name=nombre_grupo and (comentario=concat('Simulacionuserid',userid));
    select alias into varalias from users where id=userid;

    #Relacionamos grupo con usuario
    insert into users_groups (id_user,id_group,alias,role,inversion_grupo,activado) values (userid,grupo,varalias,'admin',100,-1);
    update grupos set comentario='Cartera de Simulacion' where id=grupo;

    #Añadimos operaciones
    insert into operaciones_abiertas (id_group,moneda,precio_compra,numero_compra,comision_compra,fecha_compra,comentario)
    values (grupo,'BTC-EUR',(select ultimo_precio from monedas where moneda='BTC-EUR') ,1,0,now(),'Operacion Simulacion');
    insert into operaciones_abiertas (id_group,moneda,precio_compra,numero_compra,comision_compra,fecha_compra,comentario)
    values (grupo,'ADA-EUR',(select ultimo_precio from monedas where moneda='ADA-EUR') ,10000,0,now(),'Operacion Simulacion');
    insert into operaciones_abiertas (id_group,moneda,precio_compra,numero_compra,comision_compra,fecha_compra,comentario)
    values (grupo,'ETH-EUR',(select ultimo_precio from monedas where moneda='ETH-EUR') ,3,0,now(),'Operacion Simulacion');
    insert into operaciones_abiertas (id_group,moneda,precio_compra,numero_compra,comision_compra,fecha_compra,comentario)
    values (grupo,'DOT-EUR',(select ultimo_precio from monedas where moneda='DOT-EUR') ,200,0,now(),'Operacion Simulacion');
    insert into operaciones_abiertas (id_group,moneda,precio_compra,numero_compra,comision_compra,fecha_compra,comentario)
    values (grupo,'MATIC-EUR',(select ultimo_precio from monedas where moneda='MATIC-EUR') ,10000,0,now(),'Operacion Simulacion');
    insert into operaciones_abiertas (id_group,moneda,precio_compra,numero_compra,comision_compra,fecha_compra,comentario)
    values (grupo,'DOGE-EUR',(select ultimo_precio from monedas where moneda='DOGE-EUR') ,10000,0,now(),'Operacion Simulacion');

    #Enviamos a Log
    call datos2log('grupos',concat('id=',grupo),userid_ejecutor,'add');
    call datos2log('users_groups',concat('id_user=',userid,' AND id_group=',grupo),userid_ejecutor,'add');
    call datos2log('operaciones_abiertas',concat('moneda="BTC-EUR" AND id_group=',grupo),userid_ejecutor,'add');
  	call datos2log('operaciones_abiertas',concat('moneda="ADA-EUR" AND id_group=',grupo),userid_ejecutor,'add');
    call datos2log('operaciones_abiertas',concat('moneda="ETH-EUR" AND id_group=',grupo),userid_ejecutor,'add');
    call datos2log('operaciones_abiertas',concat('moneda="DOT-EUR" AND id_group=',grupo),userid_ejecutor,'add');
    call datos2log('operaciones_abiertas',concat('moneda="MATIC-EUR" AND id_group=',grupo),userid_ejecutor,'add');
    call datos2log('operaciones_abiertas',concat('moneda="DOGE-EUR" AND id_group=',grupo),userid_ejecutor,'add');

    SELECT 'ok' as status,'Grupo Simulacion Creado' as message; 
    END
    `);

    await connection.query(`
    CREATE PROCEDURE datos2log (IN tabla varchar(50), que varchar(100), quien int, accion varchar(20))
    BEGIN
    set @sql_text = concat('insert into _log_',tabla,' select *,',quien,',"',accion,'",now() from ',tabla, ' where ',que);

    PREPARE salida FROM @sql_text; 
    EXECUTE salida; 
    DEALLOCATE PREPARE salida;

    END
    `);

    console.log('Procedimientos creados');

    // Insertamos un usuario administrador y dos normales
    await connection.query(`
        INSERT INTO users (createdAt, email, password, name, active, role,alias,avatar)
        VALUES
            ("${formatDate(new Date())}",
            "david.martinez@hackaboss.com",
            SHA2("${process.env.ADMIN_PASSWORD}", 512),
            "David Martinez",
            true,
            "admin",
            "david.martinez",
            "defaultAvatar.png"),
            ("${formatDate(new Date())}",
            "pepe.perez@hackaboss.com",
            SHA2("${process.env.ADMIN_PASSWORD}", 512),
            "Pepe Perez",
            true,
            "user",
            "pepe.perez",
            "defaultAvatar.png"),
            ("${formatDate(new Date())}",
            "manolo.garcia@hackaboss.com",
            SHA2("${process.env.ADMIN_PASSWORD}", 512),
            "Manolo Gracia",
            true,
            "user",
            "manolo.garcia",
            "defaultAvatar.png"),
            ("${formatDate(new Date())}",
            "jsanzb@gmail.com",
            SHA2("${process.env.ADMIN_PASSWORD}", 512),
            "",
            true,
            "admin",
            "JJ",
            "defaultAvatar.png")
            
        ;
    `);

    // Insertamos un grupo para cada usuario y uno para los tres.
    await connection.query(`
            INSERT INTO grupos (name,comentario,personal)
            VALUES 
            ("Personal","Cartera Personal",-1),
            ("Personal","Cartera Personal",-1),
            ("Personal","Cartera Personal",-1),
            ("Los Locos","Cartera de Grupo",0),
            ("Personal","Cartera Personal",-1);
`);
    // Añadimos los usuarios a los grupos, uno personal para cada uno y uno compartido para los tres
    await connection.query(`
            INSERT INTO users_groups (id_user,id_group,alias,role,inversion_grupo,activado)
            VALUES 
            (1,1,"david.martinez","admin",100,-1),
            (2,2,"pepe.perez","admin",100,-1),
            (3,3,"manolo.garcia","admin",100,-1),
            (1,4,"david.martinez","admin",50,-1),
            (2,4,"pepe.perez","user",25,-1),
            (3,4,"manolo.garcia","user",25,-1),
            (4,5,"jsanzb","admin",100,-1)
            ;
`);

    // Añadimos los datos de monedas
    await connection.query(`
            INSERT INTO monedas (moneda, activada, nombre, fecha_ultimo_precio, ultimo_precio, bid, ask, volumen, comentario, up1, up2, up3, up4, up5, up6, up7, up8, up9, up10)
            VALUES 
            ('1INCH-EUR', '-1', '1inch', '2021-06-19 13:39:44', NULL, '2.605', '2.609', '147828', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('AAVE-EUR', '-1', 'Aave', '2021-06-19 13:43:10', NULL, '223.769', '223.972', '5100.97', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('ADA-EUR', '-1', 'Cardano', '2021-06-19 13:43:10', NULL, '1.2026', '1.2033', '3123530',NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('ALGO-EUR', '-1', 'Algorand', '2021-06-19 13:42:55', NULL, '0.8138', '0.8143', '1622060', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('ANKR-EUR', '-1', 'Ankr', '2021-06-19 13:37:56', NULL, '0.06196', '0.06203', '11372100', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('BAND-EUR', '-1', 'Band Protocol', '2021-06-19 13:19:42', NULL, '5.3385', '5.3455', '49407.9', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('BAT-EUR', '-1', 'Basic Attention Token', '2021-06-19 13:33:14', NULL, '0.52', '0.522', '133989', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('BCH-EUR', '-1', 'Bitcoin Cash', '2021-06-19 13:37:51', NULL, '483.22', '483.63', '1905.24', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('BNT-EUR', '-1', 'Bancor Network Token', '2021-06-19 13:12:52', NULL, '2.9953', '2.9993', '84805.9', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('BTC-EUR', '-1', 'Bitcoin', '2021-06-19 13:43:15', NULL, '30295.8', '30302.1', '1515.31', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('CGLD-EUR', '-1', 'Celo', '2021-06-19 13:40:54', NULL, '2.0018', '2.0039', '307746', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('COMP-EUR', '0', 'Compound', NULL, NULL, NULL, NULL, NULL, 'No sale en la API de Coinbase PRO con el par EUR, la ignoro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            ('CRV-EUR', '-1', 'Curve Dao Token', '2021-06-19 13:36:35', NULL, '1.6149', '1.6166', '720412',NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('DOGE-EUR', '-1', 'DogeCoin', '2021-06-19 13:43:12',NULL, '0.2462', '0.2464', '5073700', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('DOT-EUR', '-1', 'Polkadot', '2021-06-19 13:39:38', NULL, '17.793', '17.826', '47302.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('EOS-EUR', '-1', 'EOS', '2021-06-19 13:43:14',NULL, '3.904', '3.907', '246445', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('ETC-EUR', '-1', 'Ethereum Clasic', '2021-06-19 13:39:07', NULL, '44.217', '44.27', '15125.8', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('ETH-EUR', '-1', 'Ethereum', '2021-06-19 13:43:15', NULL, '1890.6', '1890.61', '15434.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('EUR', '0', 'Euros', NULL, NULL, NULL, NULL, NULL, 'Para integridad referencial con operaciones', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            ('FIL-EUR', '-1', 'Filecoin', '2021-06-19 13:40:36', NULL, '55.1584', '55.2104', '5000.98', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('FORTH-EUR', '-1', 'Ampleforth Governance Token', '2021-06-19 13:43:06', NULL, '12.851', '12.882', '15752.9', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('GRT-EUR', '-1', 'The Graph', '2021-06-19 13:42:58',NULL, '0.5192', '0.5196', '2564420', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('ICP-EUR', '-1', 'Internet Computer', '2021-06-19 13:43:17', NULL, '43.523', '43.55', '70003.6', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('LINK-EUR', '-1', 'ChainLink', '2021-06-19 13:43:05',NULL, '18.0608', '18.0729', '109799', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('LTC-EUR', '-1', 'Litecoin', '2021-06-19 13:43:14',NULL, '133.97', '134.04', '15791', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL),
            ('MANA-EUR', '-1', 'Decentraland', '2021-06-19 13:39:59', NULL, '0.546', '0.548', '167306', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('MATIC-EUR', '-1', 'Polygon', '2021-06-19 13:43:18', NULL, '1.1694', '1.1707', '4592390', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('MIR-EUR', '-1', 'Mirror Protocol', '2021-06-19 13:40:09', NULL, '4.036', '4.04', '439109', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('NMR-EUR', '-1', 'Numeraire', '2021-06-19 13:37:18',NULL, '31.2631', '31.3133', '22926.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('NU-EUR', '-1', 'NuCypher', '2021-06-19 13:41:20', NULL, '0.2419', '0.2423', '4864420',NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('OMG-EUR', '-1', 'OMG Network', '2021-06-19 13:42:37',NULL, '3.9487', '3.9523', '113347', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('SKL-EUR', '-1', 'SKALE Network', '2021-06-19 13:43:14',NULL, '0.2358', '0.2362', '4257140', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('SNX-EUR', '-1', 'Synthetix Network Token', '2021-06-19 13:39:54', NULL, '7.0594', '7.0674', '33541.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('SOL-EUR', '-1', 'Solana', '2001-01-01 02:00:00', NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            ('SUSHI-EUR', '-1', 'SushiSwap', '2021-06-19 13:36:14', NULL, '6.571', '6.578', '92254.3',NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('UMA-EUR', '-1', 'UMA', '2021-06-19 13:41:20', NULL, '9.194', '9.206', '19653.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('USDC-EUR', '-1', 'USD Coin', '2021-06-19 13:43:18', NULL, '0.845', '0.846', '8622140', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('USDT-EUR', '-1', 'Tether', '2021-06-19 13:43:03',NULL, '0.8451', '0.8453', '5234100',NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('XLM-EUR', '-1', 'Stellar Lumens', '2021-06-19 13:42:38', NULL, '0.255712', '0.256017', '4345370', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('XTZ-EUR', '-1', 'Tezos', '2021-06-19 13:41:45', NULL, '2.65625', '2.65878', '391913', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL),
            ('ZRX-EUR', '-1', '0x', '2021-06-19 13:41:25',NULL, '0.684744', '0.685164', '756854', NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,NULL, NULL, NULL)
            
            ;
`);

    // Añadimos registros consultas APIS
    await connection.query(`
            INSERT INTO api
            VALUES 
            ('API','http://localhost:4000/api', 'select * from api order by name', '-1'),
            ('Tabla Grupos','http://localhost:4000/api/Tgrupos', 'select * from api_Tgrupos', '-1'),
            ('Tabla Monedas','http://localhost:4000/api/Tmonedas', 'select * from api_Tmonedas', '-1'),
            ('Tabla Operaciones Abiertas','http://localhost:4000/api/Toperaciones_abiertas', 'select * from api_Toperaciones_abiertas', '-1'),
            ('Tabla Operaciones Cerradas','http://localhost:4000/api/Toperaciones_cerradas', 'select * from api_Toperaciones_cerradas', '-1'),
            ('Tabla Users','http://localhost:4000/api/Tusers', 'select * from api_Tusers', '-1'),
            ('Tabla Users-Groups','http://localhost:4000/api/Tusers_groups', 'select * from api_Tusers_groups', '-1'),
            ('Tabla Monedas Favoritas','http://localhost:4000/api/Tusers_monedas_favorita', 'select * from api_Tusers_monedas_favoritas', '-1'),
            ('Tabla Valoraciones Usuarios','http://localhost:4000/api/Tvaloraciones_users', 'select * from api_Tvaloraciones_users', '-1'),
            ('Consulta Valoraciones Monedas Promedio','http://localhost:4000/api/valoraciones_users_promedio', 'select * from api_valoraciones_users_promedio', '-1'),
            ('Consulta Usuarios por Grupo','http://localhost:4000/api/users_x_grupo', 'select id_group,group_name,id_user,email from api_users_x_grupo order by id_group,id_user', '-1'),
            ('Consulta Grupos por Usuarios','http://localhost:4000/api/grupo_x_users', 'select * FROM api_grupo_x_users', '-1'),
            ('Consulta Monedas Activas','http://localhost:4000/api/monedas_activas', 'select * from api_monedas_activas', '-1'),
            ('Consulta Monedas Favoritas por Usuarios','http://localhost:4000/api/monedas_favoritas_x_users', 'select * from api_monedas_favoritas_x_users', '-1'),
            ('Consulta Monedas mas las favoritas por Usuarios_test','http://localhost:4000/api/monedas_mas_favoritas_x_users_test', 'select * from api_monedas_mas_favoritas_x_users_test', '-1'),
            ('Consulta Operaciones Abiertas por Grupo','http://localhost:4000/api/operaciones_abiertas_x_grupo', 'select * from api_operaciones_abiertas_x_grupo', '-1'),
            ('Consulta Operaciones Cerradas por Grupo','http://localhost:4000/api/operaciones_cerradas_x_grupo', 'select * from api_operaciones_cerradas_x_grupo', '-1'),
            ('Consulta Rendimientos por Operacion','http://localhost:4000/api/rendientos_x_operacion', 'select * FROM api_rendimiento_operaciones', '-1'),
            ('Consulta Rendimientos por Monedas y Grupos','http://localhost:4000/api/rendientos_x_monedas_grupos', 'select * FROM api_rendimiento_monedas_grupos', '-1'),
            ('Consulta Rendimientos por Grupos','http://localhost:4000/api/rendientos_x_grupos', 'select * FROM api_rendimiento_grupos', '-1'),
            ('Log_Tabla Grupos','http://localhost:4000/api/LTgrupos', 'select * from api_LTgrupos', '-1'),
            ('Log_Tabla Operaciones Abiertas','http://localhost:4000/api/LToperaciones_abiertas', 'select * from api_LToperaciones_abiertas', '-1'),
            ('Log_Tabla Operaciones Cerradas','http://localhost:4000/api/LToperaciones_cerradas', 'select * from api_LToperaciones_cerradas', '-1'),
            ('Log_Tabla Users','http://localhost:4000/api/LTusers', 'select * from api_LTusers', '-1'),
            ('Log_Tabla Users-Groups','http://localhost:4000/api/LTusers_groups', 'select * from api_LTusers_groups', '-1'),
            ('Log_Tabla Monedas Favoritas','http://localhost:4000/api/LTusers_monedas_favorita', 'select * from api_LTusers_monedas_favoritas', '-1'),
            ('Log_Resumen','http://localhost:4000/api/Log_resumen', 'select * from api_logresumen', '-1'),
            ('Log_Tabla Valoraciones Usuarios','http://localhost:4000/api/LTvaloraciones_users', 'select * from api_LTvaloraciones_users', '-1')
            ;
`);

    // Añadimos datos Monedas favoritas
    await connection.query(`
            INSERT INTO users_monedas_favoritas
            VALUES 
            ('1', 'BTC-EUR'),
            ('1', 'DOGE-EUR'),
            ('1', 'ICP-EUR'),
            ('1', 'MATIC-EUR'),
            ('2', 'BTC-EUR'),
            ('2', 'ADA-EUR'),
            ('3', 'BTC-EUR'),
            ('3', 'ETH-EUR'),
            ('3', 'BAT-EUR'),
            ('3', 'EOS-EUR'),
            ('4', 'BTC-EUR'),
            ('4', 'DOGE-EUR'),
            ('4', 'ETH-EUR'),
            ('4', 'ICP-EUR'),
            ('4', 'MATIC-EUR')
            ;
`);

    // Añadimos datos Operaciones Abiertas
    await connection.query(`
            INSERT INTO operaciones_abiertas (id_group, moneda, numero_compra, precio_compra, comision_compra, fecha_compra, comentario)
            VALUES 
            ('1', 'BTC-EUR', '0.5', '30600', '2', '2021-05-27 15:00:27', NULL),
            ('2', 'BTC-EUR', '1', '30700', '2.5', '2021-05-27 15:00:27', NULL),
            ('3', 'BTC-EUR', '1.5', '30800', '3', '2021-05-27 15:00:27', NULL),
            ('4', 'BTC-EUR', '1', '30700', '2.5', '2021-05-27 15:00:27', NULL),
            ('4', 'ETH-EUR', '10', '1900', '2', '2021-05-27 15:00:27', NULL),
            ('4', 'ADA-EUR', '1000', '1.3', '3', '2021-05-27 15:00:27', NULL),
            ('4', 'DOT-EUR', '1000', '1', '2', '2021-05-27 15:00:27', NULL),
            ('5', 'BTC-EUR', '0.5', '30600', '2', '2021-05-27 15:00:27', NULL),
            ('5', 'ETH-EUR', '10', '1900', '2', '2021-05-27 15:00:27', NULL)
            ;
`);

    // Añadimos datos Operaciones Cerradas
    await connection.query(`
            INSERT INTO operaciones_cerradas (id_group, moneda, numero_compra, precio_compra, comision_compra, total_compra, fecha_compra, numero_venta, precio_venta, comision_venta, total_venta, fecha_venta, porcentaje_bruto, rendimiento_bruto, porcentaje_neto, rendimiento_neto, comentario)
            VALUES 
            ('1', 'ETC-EUR', '1', '43.5', '1', '43.5', '2021-05-20 00:00:00', '1', '44.5', '1', '44.5', '2021-05-22 00:00:00', '1', '2', '3', '4', NULL),
            ('2', 'ETC-EUR', '3', '43.5', '3', '43.5', '2021-05-20 00:00:00', '3', '42.5', '3', '127.5', '2021-05-22 00:00:00', '1', '2', '3', '4', NULL),
            ('3', 'ETC-EUR', '2', '43.5', '2', '43.5', '2021-05-20 00:00:00', '2', '43.5', '2', '87', '2021-05-22 00:00:00', '1', '2', '3', '4', NULL),
            ('4', 'ETC-EUR', '5', '43.5', '5', '43.5', '2021-05-20 00:00:00', '10', '45.5', '10', '455.5', '2021-05-22 00:00:00', '1', '2', '3', '4', NULL)
            ;
`);

    // Añadimos datos Valoraciones Usuarios
    await connection.query(`
            INSERT INTO valoraciones_users
            VALUES 
            ('1', 'BTC-EUR','5','Bitcoin is the best'),
            ('2', 'BTC-EUR','1','No invirtais!!'),
            ('3', 'ETH-EUR','4','Arriba Ethereum!!')
            ;
`);

    console.log('Entradas insertadas');
  } catch (error) {
    console.error(error);
  } finally {
    // Siempre debemos cerrar la conexión.
    if (connection) connection.release();
    process.exit(0);
  }
};

main();
