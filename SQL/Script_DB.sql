

-- -----------------------------------------------------
-- Schema gic
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gic` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `gic` ;

-- -----------------------------------------------------
-- Table `gic`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`groups` ;

CREATE TABLE IF NOT EXISTS `gic`.`groups` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `comentario` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`monedas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`monedas` ;

CREATE TABLE IF NOT EXISTS `gic`.`monedas` (
  `moneda` VARCHAR(10) NOT NULL,
  `activada` TINYINT NOT NULL,
  `nombre` VARCHAR(40) NULL DEFAULT NULL,
  `fecha_ultimo_precio` DATETIME NULL DEFAULT NULL,
  `ultimo_precio` FLOAT NULL DEFAULT NULL,
  `bid` FLOAT NULL DEFAULT NULL,
  `ask` FLOAT NULL DEFAULT NULL,
  `volumen` FLOAT NULL DEFAULT NULL,
  `comentario` VARCHAR(200) NULL DEFAULT NULL,
  `up1` FLOAT NULL DEFAULT NULL,
  `up2` FLOAT NULL DEFAULT NULL,
  `up3` FLOAT NULL DEFAULT NULL,
  `up4` FLOAT NULL DEFAULT NULL,
  `up5` FLOAT NULL DEFAULT NULL,
  `up6` FLOAT NULL DEFAULT NULL,
  `up7` FLOAT NULL DEFAULT NULL,
  `up8` FLOAT NULL DEFAULT NULL,
  `up9` FLOAT NULL DEFAULT NULL,
  `up10` FLOAT NULL DEFAULT NULL,
  `vup1` FLOAT NULL DEFAULT NULL,
  `vup2` FLOAT NULL DEFAULT NULL,
  `vup3` FLOAT NULL DEFAULT NULL,
  `vup4` FLOAT NULL DEFAULT NULL,
  `vup5` FLOAT NULL DEFAULT NULL,
  `vup6` FLOAT NULL DEFAULT NULL,
  `vup7` FLOAT NULL DEFAULT NULL,
  `vup8` FLOAT NULL DEFAULT NULL,
  `vup9` FLOAT NULL DEFAULT NULL,
  `vup10` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`moneda`),
  UNIQUE INDEX `moneda_UNIQUE` (`moneda` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`historico_tickers_21600`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`historico_tickers_21600` ;

CREATE TABLE IF NOT EXISTS `gic`.`historico_tickers_21600` (
  `moneda` VARCHAR(10) NOT NULL,
  `time` INT NOT NULL,
  `low` FLOAT NOT NULL,
  `high` FLOAT NOT NULL,
  `open` FLOAT NOT NULL,
  `close` FLOAT NOT NULL,
  `volume` FLOAT NOT NULL,
  INDEX `historico21600` (`moneda` ASC, `time` ASC),
  CONSTRAINT `historico21600_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`historico_tickers_300`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`historico_tickers_300` ;

CREATE TABLE IF NOT EXISTS `gic`.`historico_tickers_300` (
  `moneda` VARCHAR(10) NOT NULL,
  `time` INT NOT NULL,
  `low` FLOAT NOT NULL,
  `high` FLOAT NOT NULL,
  `open` FLOAT NOT NULL,
  `close` FLOAT NOT NULL,
  `volume` FLOAT NOT NULL,
  INDEX `historico300` (`moneda` ASC, `time` ASC),
  CONSTRAINT `historico300_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`historico_tickers_3600`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`historico_tickers_3600` ;

CREATE TABLE IF NOT EXISTS `gic`.`historico_tickers_3600` (
  `moneda` VARCHAR(10) NOT NULL,
  `time` INT NOT NULL,
  `low` FLOAT NOT NULL,
  `high` FLOAT NOT NULL,
  `open` FLOAT NOT NULL,
  `close` FLOAT NOT NULL,
  `volume` FLOAT NOT NULL,
  INDEX `historico3600` (`moneda` ASC, `time` ASC),
  CONSTRAINT `historico3600_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`historico_tickers_60`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`historico_tickers_60` ;

CREATE TABLE IF NOT EXISTS `gic`.`historico_tickers_60` (
  `moneda` VARCHAR(10) NOT NULL,
  `time` INT NOT NULL,
  `low` FLOAT NOT NULL,
  `high` FLOAT NOT NULL,
  `open` FLOAT NOT NULL,
  `close` FLOAT NOT NULL,
  `volume` FLOAT NOT NULL,
  INDEX `historico60` (`moneda` ASC, `time` ASC),
  CONSTRAINT `historico60_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`historico_tickers_86400`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`historico_tickers_86400` ;

CREATE TABLE IF NOT EXISTS `gic`.`historico_tickers_86400` (
  `moneda` VARCHAR(10) NOT NULL,
  `time` INT NOT NULL,
  `low` FLOAT NOT NULL,
  `high` FLOAT NOT NULL,
  `open` FLOAT NOT NULL,
  `close` FLOAT NOT NULL,
  `volume` FLOAT NOT NULL,
  INDEX `historico86400` (`moneda` ASC, `time` ASC),
  CONSTRAINT `historico86400_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`historico_tickers_900`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`historico_tickers_900` ;

CREATE TABLE IF NOT EXISTS `gic`.`historico_tickers_900` (
  `moneda` VARCHAR(10) NOT NULL,
  `time` INT NOT NULL,
  `low` FLOAT NOT NULL,
  `high` FLOAT NOT NULL,
  `open` FLOAT NOT NULL,
  `close` FLOAT NOT NULL,
  `volume` FLOAT NOT NULL,
  INDEX `historico900` (`moneda` ASC, `time` ASC),
  CONSTRAINT `historico900_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`operaciones_abiertas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`operaciones_abiertas` ;

CREATE TABLE IF NOT EXISTS `gic`.`operaciones_abiertas` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_group` INT UNSIGNED NOT NULL,
  `moneda` VARCHAR(10) NOT NULL,
  `numero_compra` FLOAT NOT NULL,
  `precio_compra` FLOAT NOT NULL,
  `comision_compra` FLOAT NOT NULL,
  `fecha_compra` DATETIME NOT NULL,
  `comentario` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `operaciones_grupos_idx` (`id_group` ASC),
  INDEX `operaciones_monedas_idx` (`moneda` ASC),
  CONSTRAINT `operaciones_abiertas_groups`
    FOREIGN KEY (`id_group`)
    REFERENCES `gic`.`groups` (`id`),
  CONSTRAINT `operaciones_abiertas_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`operaciones_cerradas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`operaciones_cerradas` ;

CREATE TABLE IF NOT EXISTS `gic`.`operaciones_cerradas` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_group` INT UNSIGNED NOT NULL,
  `moneda` VARCHAR(10) NOT NULL,
  `numero_compra` FLOAT NOT NULL,
  `precio_compra` FLOAT NOT NULL,
  `comision_compra` FLOAT NOT NULL,
  `total_compra` FLOAT NOT NULL,
  `fecha_compra` DATETIME NOT NULL,
  `numero_venta` FLOAT NOT NULL,
  `precio_venta` FLOAT NOT NULL,
  `comision_venta` FLOAT NOT NULL,
  `total_venta` FLOAT NOT NULL,
  `porcentaje_bruto` FLOAT NOT NULL,
  `rendimiento_bruto` FLOAT NOT NULL,
  `porcentaje_neto` FLOAT NOT NULL,
  `rendimiento_neto` FLOAT NOT NULL,
  `comentario` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `operaciones_cerradas_groups` (`id_group` ASC),
  INDEX `operaciones_cerradas_monedas` (`moneda` ASC),
  CONSTRAINT `operaciones_cerradas_groups`
    FOREIGN KEY (`id_group`)
    REFERENCES `gic`.`groups` (`id`),
  CONSTRAINT `operaciones_cerradas_monedas`
    FOREIGN KEY (`moneda`)
    REFERENCES `gic`.`monedas` (`moneda`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`users` ;

CREATE TABLE IF NOT EXISTS `gic`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `last_login` DATETIME NULL DEFAULT NULL,
  `password` VARCHAR(45) NOT NULL,
  `activado` TINYINT NULL DEFAULT '0',
  `borrado` TINYINT NULL DEFAULT '0',
  `roleapp` VARCHAR(15) NULL DEFAULT 'user',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `gic`.`users_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gic`.`users_groups` ;

CREATE TABLE IF NOT EXISTS `gic`.`users_groups` (
  `id_user` INT UNSIGNED NOT NULL,
  `id_group` INT UNSIGNED NOT NULL,
  `alias` VARCHAR(40) NULL DEFAULT NULL,
  `role` VARCHAR(40) NOT NULL,
  `inversion_grupo` FLOAT UNSIGNED NOT NULL,
  PRIMARY KEY (`id_user`, `id_group`),
  INDEX `users_groups_groups` (`id_group` ASC),
  CONSTRAINT `users_groups_groups`
    FOREIGN KEY (`id_group`)
    REFERENCES `gic`.`groups` (`id`),
  CONSTRAINT `users_groups_users`
    FOREIGN KEY (`id_user`)
    REFERENCES `gic`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

USE `gic` ;


CREATE VIEW `users_x_groups` AS
    SELECT 
        `u`.`id` AS `id_user`,
        `u`.`email` AS `email`,
        `u`.`name` AS `name`,
        `u`.`lastname` AS `lastname`,
        `u`.`last_login` AS `last_login`,
        `u`.`password` AS `password`,
        `u`.`activado` AS `activado`,
        `u`.`borrado` AS `borrado`,
        `u`.`roleapp` AS `roleapp`,
        `ug`.`alias` AS `alias_group`,
        `g`.`id` AS `id_group`,
        `g`.`name` AS `group_name`,
        `ug`.`role` AS `role_group`
    FROM
        ((`users` `u`
        JOIN `users_groups` `ug` ON ((`u`.`id` = `ug`.`id_user`)))
        JOIN `groups` `g` ON ((`ug`.`id_group` = `g`.`id`)))